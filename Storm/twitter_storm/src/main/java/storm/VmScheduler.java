package storm;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import backtype.storm.generated.Bolt;
import backtype.storm.generated.SpoutSpec;
import backtype.storm.generated.StormTopology;
import backtype.storm.scheduler.Cluster;
import backtype.storm.scheduler.EvenScheduler;
import backtype.storm.scheduler.ExecutorDetails;
import backtype.storm.scheduler.IScheduler;
import backtype.storm.scheduler.SupervisorDetails;
import backtype.storm.scheduler.Topologies;
import backtype.storm.scheduler.TopologyDetails;
import backtype.storm.scheduler.WorkerSlot;
import backtype.storm.utils.Utils;

public class VmScheduler implements IScheduler{

	private static final String VM_NAME = "vm_name";

	public void prepare(Map conf) {
		// TODO Auto-generated method stub
	}

	public void schedule(Topologies topologies, Cluster cluster) {

		Collection<TopologyDetails> topologyDetails = topologies.getTopologies();
		// TopologyDetails topologyDetails = topologies.getByName("ΤweetTopology"); // get our topology 
		Collection<SupervisorDetails> supervisorDetails = cluster.getSupervisors().values(); // get all supervisors
		Map<String, SupervisorDetails> supervisors = new HashMap<String, SupervisorDetails>();
		JSONParser parser = new JSONParser();

		// index supervisors that have a meta vm_name
		for(SupervisorDetails s : supervisorDetails){

			Map<String, String> schedulerMeta = (Map<String, String>)s.getSchedulerMeta();
			Map<String, String> metadata = schedulerMeta;
			if(metadata.get(VM_NAME) != null){
				supervisors.put(metadata.get(VM_NAME), s);
			}
		}

		for(TopologyDetails t: topologyDetails){

			if(t==null){				
				System.out.println("========> topologyDetails==null :( ");
			}
			else{
				System.out.println("========> GOT topologyDetails :) ");
				System.out.println(t.getName());

			}


			// if topology is squeezed or there are unassigned workers then we can't do anything now
			if(!cluster.needsScheduling(t)){
				System.out.println("========> NO NEED FOR SCHEDULING! :( ");
			}
			else{
				System.out.println("========> WE DO NEED SCHEDULING! :) ");
				StormTopology topology = t.getTopology();
				Map<String, Bolt> bolts = topology.get_bolts();
				Map<String, SpoutSpec> spouts = topology.get_spouts(); // one spout in our case

				try{
					for(String name : bolts.keySet()){ // iterate bolts and 

						Bolt bolt = bolts.get(name);

						// get the configuration of bolts
						JSONObject conf = (JSONObject)parser.parse(bolt.get_common().get_json_conf());
						String vm_name = "";

						//if the bolt has a vm_name in configuration and supervisors that have the same vm_name not null
						if(conf.get(VM_NAME) != null && supervisors.get(conf.get(VM_NAME)) != null){
							vm_name = (String)conf.get(VM_NAME);  // get vm_name value
							System.out.println("========> got vm_name: " + vm_name);
						}

						if(vm_name !=""){
							SupervisorDetails supervisor = supervisors.get(vm_name);// changed VM_NAME to vm_name
							if (supervisor!=null){
								List<WorkerSlot> workerSlots = cluster.getAvailableSlots(supervisor);
								System.out.println(workerSlots.size());

								List<WorkerSlot> assignablelots = cluster.getAssignableSlots(supervisor);
								System.out.println(assignablelots.size());

								List<ExecutorDetails> executors = cluster.getNeedsSchedulingComponentToExecutors(t).get(name);
								System.out.println("========> supervisor!=null executors == null?");

								System.out.println(executors==null);
								System.out.println("========> supervisor!=null executors name: " + name );

								if(!workerSlots.isEmpty() && executors != null){
									cluster.assign(workerSlots.get(0), t.getId(), executors);
									System.out.println("========> supervisor!=null workerSlots.get(0): " + workerSlots.get(0).toString());
								}
							}
							else{
								System.out.println("========> supervisor==null!");
							}
						}
						else{
							System.out.println("========> vm_name equals to empty string");
						}

					}
					for(String name : spouts.keySet()){

						String vm_name = "";
						SpoutSpec spout = spouts.get(name);
						JSONObject conf = (JSONObject)parser.parse(spout.get_common().get_json_conf());
						if(conf.get(VM_NAME) != null && supervisors.get(conf.get(VM_NAME)) != null){
							vm_name = (String)conf.get(VM_NAME);
							System.out.println("========> spouts vm_name: " + vm_name);
						}
						if(vm_name !=""){
							SupervisorDetails supervisor = supervisors.get(vm_name); // changed VM_NAME to vm_name

							if (supervisor!=null){
								List<WorkerSlot> availableSlots = cluster.getAvailableSlots(supervisor);
								System.out.println("========> supervisor!=null availableSlots !!!!: ");
								System.out.println(availableSlots.size());

								List<WorkerSlot> assignablelots = cluster.getAssignableSlots(supervisor);
								System.out.println(assignablelots.size());
								List<ExecutorDetails> executors = cluster.getNeedsSchedulingComponentToExecutors(t).get(name);
								System.out.println("========> supervisor!=null executors !!!!: name:" + name);
								System.out.println(executors==null);
								if(!availableSlots.isEmpty() && executors != null){
									cluster.assign(availableSlots.get(0), t.getId(), executors);
									System.out.println(availableSlots.get(0).toString());
								}
							}else{
								System.out.println("========>spouts supervisor==null");
							}
						}
						else{
							System.out.println("========> spouts vm_name equals to empty string");
						}
					}
				}catch(ParseException pe){
					pe.printStackTrace();
				}

			}

		}

		//	// let system's even scheduler handle the rest scheduling work
		//	// you can also use your own other scheduler here, this is what
		//	// makes storm's scheduler composable.
		//	// for(TopologyDetails t : topologyDetails){
		//	if(cluster.needsScheduling(topologyDetails)) {
		//	System.out.println("========> cluster.needsScheduling(topologyDetails)");
		//		new EvenScheduler().schedule(topologies, cluster);
		//
		//	}
		// }
	}	
}