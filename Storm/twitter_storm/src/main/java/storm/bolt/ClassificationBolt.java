package storm.bolt;

import java.util.Map;

import backtype.storm.task.ShellBolt;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;


public class ClassificationBolt extends ShellBolt implements IRichBolt {

    /**
     * This bolt is implemented in Python. See classification.py in multilang folder
     */
    private static final long serialVersionUID = 1L;

    public ClassificationBolt() {
        super("python", "classification.py");
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }

}
