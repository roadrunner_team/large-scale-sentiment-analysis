package storm.bolt;


import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.UpdateOptions;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.ascending;
import static java.util.Arrays.asList;

import java.sql.Date;
import java.util.Calendar;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;


public class CalculationBolt extends BaseRichBolt {

    /**
     * This bolt calculates statistics like: how many positive - negative - neutral so far for each model
     */
    private static final long serialVersionUID = 1L;


    private OutputCollector collector;
    private DB mongoDB;

    private final String mongoHost;
    private final int mongoPort; // 27017
    private final String mongoDbName;
    private String collectionName = "Calculation";
    private int sumPos;
    private int sumNeg;
    private int sumNeu;
    //private String id = "55fa7e2351fe8b05743d6456";
    BasicDBObject query = new BasicDBObject();
    DBObject currentDaysStatisticsObject = null;
    private Calendar today;
    private String todayStr = "";

    public CalculationBolt(String mongoHost, int mongoPort, String mongoDbName) {
        this.mongoHost = mongoHost;
        this.mongoPort = mongoPort;
        this.mongoDbName = mongoDbName;
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context, OutputCollector collector) {

        this.collector = collector;
        today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0); // same for minutes and seconds
        today.set(Calendar.MINUTE, 0); // same for minutes and seconds
        today.set(Calendar.SECOND, 0); // same for minutes and seconds
        todayStr = DateFormatUtils.format(today, "yyyy-MM-dd");

        try {
            this.mongoDB = new MongoClient(mongoHost, mongoPort).getDB(mongoDbName);
            findTodaysTotal();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void findTodaysTotal() {
        BasicDBObject whereQuery = new BasicDBObject();
        whereQuery.put("dateUpdated", todayStr);
        DBCursor cursor = this.mongoDB.getCollection(this.collectionName).find(whereQuery);

        if (!cursor.hasNext()) { // if there is no record for today, create one
            this.mongoDB.getCollection(this.collectionName).insert(
                    new BasicDBObject().append("all_features_110K_positive", 0)
                            .append("all_features_110K_negative", 0)
                            .append("all_features_110K_neutral", 0)
                            .append("no_emoticons_110K_positive", 0)
                            .append("no_emoticons_110K_negative", 0)
                            .append("no_emoticons_110K_neutral", 0)
                            .append("no_links_110K_positive", 0)
                            .append("no_links_110K_negative", 0)
                            .append("no_links_110K_neutral", 0)
                            .append("no_1_6_m_positive", 0)
                            .append("no_1_6_m_negative", 0)
                            .append("no_1_6_m_neutral", 0)
                            .append("total", 0)
                            .append("dateUpdated", todayStr)
            );
        }
    }

    @Override
    public void execute(Tuple input) {

        if (today.before(Calendar.getInstance())) {
            today = Calendar.getInstance();     // Move today to actual today
            today.set(Calendar.HOUR_OF_DAY, 0); // set hour
            today.set(Calendar.MINUTE, 0);      // min
            today.set(Calendar.SECOND, 0);      // and seconds
            todayStr = DateFormatUtils.format(today, "yyyy-MM-dd");
        }

        String tweet = (String) input.getValueByField("tweet");
        try {
            JSONObject jsonObj = new JSONObject(tweet);
            String predicted_score = jsonObj.getString("predicted_score");
            String no_emoticons = jsonObj.getString("predicted_score_no_emoticons");
            String no_links = jsonObj.getString("predicted_score_no_links");
            String no_1_6_m = jsonObj.getString("predicted_score_all_features_no_1_6m");
            insertDoc(Double.parseDouble(predicted_score),
                      Double.parseDouble(no_emoticons),
                      Double.parseDouble(no_links),
                      Double.parseDouble(no_1_6_m)
            );

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void insertDoc(Double predicted_score, Double no_emoticons, Double no_links, Double no_1_6_m) {
        // Use Mongo's increment so that the update is done once and we do not need to know how many so far
        // object with todays' date
        BasicDBObject insertDocument = new BasicDBObject();
        BasicDBObject innerDocument = new BasicDBObject();
        BasicDBObject searchQuery = new BasicDBObject().append("dateUpdated", todayStr);
        insertDocument.append("$inc", innerDocument.append("total", 1));

        // ===== ALL FEATURES ===== //
        if (predicted_score < 0.0) {

            insertDocument.append("$inc", innerDocument.append("all_features_110K_negative", 1));

        } else if (predicted_score > 0.0) {
            insertDocument.append("$inc", innerDocument.append("all_features_110K_positive", 1));

        } else {
            insertDocument.append("$inc", innerDocument.append("all_features_110K_neutral", 1));
        }

        // ===== NO EMOTICONS ===== //
        if (no_emoticons < 0.0) {

            insertDocument.append("$inc", innerDocument.append("no_emoticons_110K_negative", 1));

        } else if (no_emoticons > 0.0) {
            insertDocument.append("$inc", innerDocument.append("no_emoticons_110K_positive", 1));

        } else {
            insertDocument.append("$inc", innerDocument.append("no_emoticons_110K_neutral", 1));
        }

        // ===== NO LINKS ===== //
        if (no_links < 0.0) {

            insertDocument.append("$inc", innerDocument.append("no_links_110K_negative", 1));

        } else if (no_links > 0.0) {
            insertDocument.append("$inc", innerDocument.append("no_links_110K_positive", 1));

        } else {
            insertDocument.append("$inc", innerDocument.append("no_links_110K_neutral", 1));
        }
        // ===== NO 1.6M ===== //
        if (no_1_6_m < 0.0) {

            insertDocument.append("$inc", innerDocument.append("no_1_6_m_negative", 1));

        } else if (no_1_6_m > 0.0) {
            insertDocument.append("$inc", innerDocument.append("no_1_6_m_positive", 1));

        } else {
            insertDocument.append("$inc", innerDocument.append("no_1_6_m_neutral", 1));

        }

        // update once
        this.mongoDB.getCollection(this.collectionName).update(searchQuery, insertDocument, true, false);

    }

    private void findTotal() {

        BasicDBObject queryTotal = new BasicDBObject();
        BasicDBObject field = new BasicDBObject();
        field.append("negative", 1).append("positive", 1).append("neutral", 1);
        DBCursor cursor = mongoDB.getCollection(collectionName).find(queryTotal, field);
        while (cursor.hasNext()) {
            BasicDBObject obj = (BasicDBObject) cursor.next();
            sumNeg = Integer.parseInt(obj.getString("negative").toString());
            sumPos = Integer.parseInt(obj.getString("positive").toString());
            sumNeu = Integer.parseInt(obj.getString("neutral").toString());
        }
    }


    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        // TODO Auto-generated method stub

    }

}