package storm.bolt;

import java.util.Map;

import backtype.storm.task.ShellBolt;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;


public class FeatureSelectionBolt extends ShellBolt implements IRichBolt {

    /**
     * This bolt is implemented in Python. See feature_dict_cleaner.py in multilang folder
     */
    private static final long serialVersionUID = 1L;

    public FeatureSelectionBolt() {
        super("python", "feature_dict_cleaner.py");
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }

}
