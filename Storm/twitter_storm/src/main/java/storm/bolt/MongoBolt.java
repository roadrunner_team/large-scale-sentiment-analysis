package storm.bolt;

import java.util.Map;

import twitter4j.internal.org.json.JSONObject;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteConcern;
import com.mongodb.util.JSON;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class MongoBolt extends BaseRichBolt {

    /**
     * This bolt saves the input Tweet to MongoDB and emits the same tweet unchanged to the Calculation bolt.
     */
    private static final long serialVersionUID = 1L;


    private OutputCollector collector;
    private DB mongoDB;

    private final String mongoHost;
    private final int mongoPort;    // 27017
    private final String mongoDbName;


    public MongoBolt(String mongoHost, int mongoPort, String mongoDbName) {
        this.mongoHost = mongoHost;
        this.mongoPort = mongoPort;
        this.mongoDbName = mongoDbName;
    }


    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context, OutputCollector collector) {

        this.collector = collector;
        try {
            this.mongoDB = new MongoClient(mongoHost, mongoPort).getDB(mongoDbName);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute(Tuple input) {

        String collectionName = "Tweet";
        JSONObject obj;
        obj = new JSONObject(input);
        for (String field : input.getFields()) {
            Object value = input.getValueByField(field);
            DBObject newdbObject = (DBObject) JSON.parse(value.toString());
            if (newdbObject != null) {
                try {
                    mongoDB.getCollection(collectionName).save(newdbObject, new WriteConcern(1));
                    collector.emit(new Values(newdbObject.toString()));
                } catch (MongoException me) {
                    collector.fail(input);
                }

            }
        }
    }


    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }

}
