package storm;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Objects;
import java.util.Properties;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;	/* for running in cluster mode */
import backtype.storm.topology.TopologyBuilder;
import storm.bolt.CalculationBolt;
import storm.bolt.ClassificationBolt;
import storm.bolt.FeatureSelectionBolt;
import storm.bolt.MongoBolt;
import storm.bolt.PreprocessingBolt;
import storm.spout.RandomSentenceSpout;  /* for testing purposes only */
import storm.spout.TweetSpout;


public class Topology {

    public static void main(String[] args) throws Exception {
    	// Firstly load the configuration file
    	Properties props = new Properties();
        String fileName = "config.properties";
        ClassLoader classLoader = Topology.class.getClassLoader();
        Boolean isInClusterMode = Boolean.parseBoolean(props.getProperty("clusterMode"));
        // Make sure that it exists
        URL res = Objects.requireNonNull(classLoader.getResource(fileName), "Can't find configuration file:" + fileName);
        InputStream is = new FileInputStream(res.getFile());
        props.load(is);
        
        TopologyBuilder builder = new TopologyBuilder();
        Config conf = new Config();
        
        // set this to false when on a cluster mode and everything is set up correctly
        conf.setDebug(!isInClusterMode);

        // create the tweet spout with the credentials
		TweetSpout tweetSpout = new TweetSpout (props.getProperty("custkey"),
												props.getProperty("custsecret"), 
												props.getProperty("accesstoken"), 
												props.getProperty("accesssecret") );
		
	    /* The cluster topology configuration */
        //// this is for performance testing:
        //// builder.setSpout("tspout", new RandomSentenceSpout(),1).addConfiguration("vm_name", "preprocessingVM");
        if(isInClusterMode){
			builder.setSpout("tspout", tweetSpout, 1).addConfiguration("vm_name", "preprocessingVM");
	        builder.setBolt("preprocessing", new PreprocessingBolt(),4).shuffleGrouping("tspout").addConfiguration("vm_name", "preprocessingVM");
	        builder.setBolt("feature_selection", new FeatureSelectionBolt(),6).shuffleGrouping("preprocessing").addConfiguration("vm_name", "featureSelVM");
	        builder.setBolt("classification", new ClassificationBolt(),6).shuffleGrouping("feature_selection").addConfiguration("vm_name", "classificationVM");
	        builder.setBolt("MongoBolt", new MongoBolt("localhost", 27017 , "SentiStorm"),6).shuffleGrouping("classification").addConfiguration("vm_name", "mongoVM");
	        builder.setBolt("CalculationBolt", new CalculationBolt("localhost", 27017 , "SentiStorm"),6).shuffleGrouping("MongoBolt").addConfiguration("vm_name", "mongoVM");
	        //conf.setNumWorkers(16);
	        //conf.setMessageTimeoutSecs(120);
	        // configuration for the cluster - can be set up to yaml
	        conf.put(Config.TOPOLOGY_RECEIVER_BUFFER_SIZE, 8);
	        conf.put(Config.TOPOLOGY_TRANSFER_BUFFER_SIZE, 32);
	        conf.put(Config.TOPOLOGY_EXECUTOR_RECEIVE_BUFFER_SIZE, 16384);
	        conf.put(Config.TOPOLOGY_EXECUTOR_SEND_BUFFER_SIZE, 16384);
	        
	        // use the following line to submit the topology to a cluster
	        StormSubmitter.submitTopology("TweetTopology", conf, builder.createTopology());
        }
        else{
        	/* The local topology configuration */
            builder.setSpout("tspout", tweetSpout, 1);
            builder.setBolt("preprocessing", new PreprocessingBolt(), 1).shuffleGrouping("tspout");
            builder.setBolt("feature_selection", new FeatureSelectionBolt(), 1).shuffleGrouping("preprocessing");
            builder.setBolt("classification", new ClassificationBolt(), 1).shuffleGrouping("feature_selection");
            builder.setBolt("MongoBolt",new MongoBolt(props.getProperty("mongoHost"), 
    													Integer.parseInt(props.getProperty("mongoPort")), 
    													props.getProperty("mongoCollection")))
            	   .shuffleGrouping("classification");
            builder.setBolt("CalculationBolt", new CalculationBolt(props.getProperty("mongoHost"),
            														Integer.parseInt(props.getProperty("mongoPort")), 
        															props.getProperty("mongoCollection")), 1)
            		.shuffleGrouping("MongoBolt");
            
            // run it locally!
            final LocalCluster cluster = new LocalCluster();
            cluster.submitTopology("TweetTopology", conf, builder.createTopology());

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    cluster.killTopology("TweetTopology");
                    cluster.shutdown();
                }
            });

        } 

    }

}
