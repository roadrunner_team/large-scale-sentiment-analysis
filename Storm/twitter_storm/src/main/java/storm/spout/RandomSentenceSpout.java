/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package storm.spout;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.json.simple.JSONObject;

public class RandomSentenceSpout extends BaseRichSpout {
    /**
     * This spout emits one tweet at a time, randomly chosen from the sentences list.
     * This is used to see how the cluster behaves in increasing traffic - using different sleep times to adjust incoming
     * volume.
     */
    private static final long serialVersionUID = 1L;
    private SpoutOutputCollector _collector;
    private Random _rand;
    private Integer times = 0;
    private String[] sentences = new String[]{
            "Explaining the optimum tactical route to navigate a parliamentary debate designed as entrapment won't help me pay 6 months water rates",
            "@jvsshow why don't you go to h/Regis with your team and some recording equipment and confront these people I don't think you will?",
            "@LyraPro JUST SPECULATION that really isnt gonna happen",
            "\"It is time for the industry to pay back its workers, realise the workers are human beings\" - #AMCU's #JosephMathunjwa @joburgindaba",
            "@SkyBet For the both players to score market Sund'land v Newcastle could you add Jermaine Defoe &amp; Georginio Wijnaldum please  #RequestABet",
            "@AshleyJay_xx You remind me now of the TV advert sweetheart THE LADY LOVES MILK TRAY You know what i am on about my angel xxx",
            "@demidlovato5 You are so talented gal....singing, acting abilities and damn ...ur so NATURAL  and loveable....I can see a good human in you!",
            "Might casually buy a BMW this week. It's actually in my price range of $3-$4k and it's a god damn BMW. #6amnosleephopesanddreams",
            "Landing of random framed things. One day I'll have room to hang them! #london #archway https://t.co/2pu049D6tW",
            "Renting your wedding gown is Ok Gives you the option of wearing a stunning gown without draining your wedding budget https://t.co/ZiwgsaUyc4"
    };

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        _collector = collector;
        _rand = new Random();
    }

    @Override
    public void nextTuple() {

        if (times % 10 == 0) {
            Utils.sleep(100);
        }
        JSONObject fakeTweet = new JSONObject();
        fakeTweet.put("id", "1");
        fakeTweet.put("text", sentences[_rand.nextInt(sentences.length)]);

        //String sentence = sentences[_rand.nextInt(sentences.length)];
        times += 1;
        _collector.emit(new Values(fakeTweet.toString()));
    }

    @Override
    public void ack(Object id) {
    }

    @Override
    public void fail(Object id) {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }

}