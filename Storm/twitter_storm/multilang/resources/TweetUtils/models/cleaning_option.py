from TweetUtils.helpers.globals import Globals

__author__ = 'maria'


class CleaningOptions(object):
    """

    """

    name_to_value = {

    }

    def __init__(self, name, value, regex, keep):
        self.name = self.get_name(name)
        self.value = self.get_value(value)
        self.regex = self.get_regex(regex)
        self.keep = keep

    def get_name(self, name):
        """
        Check if name is a valid option, if yes return it else throw exception
        :param name: the name of the feature, e.g. 'postags'
        :type name: enum
        :return:
        :rtype:
        """
        return name

    def get_value(self, value):
        """
        Value should indicate what type of value the feature should have.
        Check if value is a valid option, if yes return it else throw exception
        :param value: the value of the feature, e.g. 'BOOLEAN'
        :type value: enum
        :return:
        :rtype:
        """

        return value

    def get_regex(self, regex):
        """
        Check if regex is valid? if yes assign to feature regex.
        :param regex:Regex to get feature from text
        :type regex: string
        :return: valid regex
        :rtype: string
        """
        return regex