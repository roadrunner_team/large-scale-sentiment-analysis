from TweetUtils.helpers.globals import Globals

__author__ = 'maria'


class Feature(object):
    """

    """
    feat_type = Globals.enum(FIGURATIVE=0,
                             TEXT_SIMILARITY=1,
                             MORPHOLOGICAL=2,
                             PRIOR_POLARITY=3,
                             LINGUISTIC=4)

    FEATURES = Globals.enum(CAPITAL=0,
                            HT=1,
                            HT_POS=2,
                            HT_NEG=3,
                            LINK=4,
                            POS_SMILEY=5,
                            NEG_SMILEY=6,
                            NEGATION=7,
                            REFERENCE=8,
                            RT=9,
                            LAUGH=10,
                            LOVE=11,
                            OH_SO=12,
                            DONT_YOU=13,
                            AS_GROUND_AS_VEHICLE=14)
    name_to_value = {

    }

    name_to_type = {

    }

    _link_pattern = r'(?P<url>https?://[^\s]+)'
    _pos_smiley_pattern = r'(:-\)|:-D|:-\)\)|:\)\)|\(:|:\)|:D|=\)|;-\)|XD|=D|=]|;D|:]|:o\))'
    _neg_smiley_pattern = r'(:-\\|:-\(|:-\(\(|:\(|(:o|:O|D:|=/|=\(!:\'\(|:\'-\(|:\\|:/|:S)'
    _negation_pattern = r'\b#{0,1}(no|not|isnt|isn\'t|isn;t|don\'t|won\'t|couldn\'t|can\'t|didn\'t|not|didnt|didn;t' \
                        r'|dont|don;t|don t|won t|can;t|cant|can t|doesn\'t|doesnt|doesn t|doesn;t|cannot|did not|not)\b'
    _reference_pattern = r'(@\w+|@\D\w+|@\d\w+)'
    _ht_pattern = r'#\w+'
    _rt_pattern = r'\b(^RT:{0,1}|rt:{0,1})\b'
    _laugh_pattern = r'\b(a*ha+h[ha]*|o?l+o+l+[ol]*)\b'
    _love_pattern = r'(<3|&lt;3)'
    _capital_pattern = r'\b[A-Z]{3,}\b'
    _oh_so_pattern = r'(O|o)h,?\s?(so)?\s?(you)?\w+'
    _dont_you_pattern = r'(D|d)on\'?;?\s?t\s(you|u)'
    _as_ground_as_vehicle_pattern = r'(A|a)s\s\w+\sas\s\w+\s?'
    _patterns_ = {
        FEATURES.CAPITAL: _capital_pattern,
        FEATURES.HT: _ht_pattern,
        FEATURES.LINK: _link_pattern,
        FEATURES.POS_SMILEY: _pos_smiley_pattern,
        FEATURES.NEG_SMILEY: _neg_smiley_pattern,
        FEATURES.NEGATION: _negation_pattern,
        FEATURES.REFERENCE: _reference_pattern,
        FEATURES.RT: _rt_pattern,
        FEATURES.LAUGH: _laugh_pattern,
        FEATURES.LOVE: _love_pattern,
        FEATURES.OH_SO: _oh_so_pattern,
        FEATURES.DONT_YOU: _dont_you_pattern,
        FEATURES.AS_GROUND_AS_VEHICLE: _as_ground_as_vehicle_pattern,
    }
    _tags = {
        FEATURES.reverse_mapping[FEATURES.CAPITAL]: "False",
        FEATURES.reverse_mapping[FEATURES.HT]: "False",
        FEATURES.reverse_mapping[FEATURES.HT_POS]: "False",
        FEATURES.reverse_mapping[FEATURES.HT_NEG]: "False",
        FEATURES.reverse_mapping[FEATURES.LINK]: "False",
        FEATURES.reverse_mapping[FEATURES.POS_SMILEY]: "False",
        FEATURES.reverse_mapping[FEATURES.NEG_SMILEY]: "False",
        FEATURES.reverse_mapping[FEATURES.NEGATION]: "False",
        FEATURES.reverse_mapping[FEATURES.REFERENCE]: "False",
        FEATURES.reverse_mapping[FEATURES.RT]: "False",
        FEATURES.reverse_mapping[FEATURES.LAUGH]: "False",
        FEATURES.reverse_mapping[FEATURES.LOVE]: "False",
        FEATURES.reverse_mapping[FEATURES.OH_SO]: "False",
        FEATURES.reverse_mapping[FEATURES.DONT_YOU]: "False",
        FEATURES.reverse_mapping[FEATURES.AS_GROUND_AS_VEHICLE]: "False"
    }

    def __init__(self, name, value, type_, discretization, regex):
        self.name = self._get_name(name)
        self.value = self._get_value(value)
        self.type_ = self._get_type(type_)
        self.discretization = self._get_discretization(discretization)
        self.regex = self._get_regex(regex)

    def _get_name(self, name):
        """
        Check if name is a valid option, if yes return it else throw exception
        :param name: the name of the feature, e.g. 'postags'
        :type name: enum
        :return:
        :rtype:
        """

        if name in self.FEATURES:
            return name
        else:
            raise Exception("Feature '{0}' Not Found!!".format(name))

    def _get_value(self, value):
        """
        Value should indicate what type of value the feature should have.
        Check if value is a valid option, if yes return it else throw exception
        :param value: the value of the feature, e.g. 'BOOLEAN'
        :type value: enum
        :return:
        :rtype:
        """

        return value

    def _get_regex(self, regex):
        """
        Check if regex is valid? if yes assign to feature regex.
        :param regex:Regex to get feature from text
        :type regex: string
        :return: valid regex
        :rtype: string
        """
        return regex

    def _get_discretization(self, discretization):
        pass

    def _get_type(self, type_):
        pass