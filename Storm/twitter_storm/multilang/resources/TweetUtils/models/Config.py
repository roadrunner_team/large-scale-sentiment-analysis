__author__ = 'maria'

from TweetUtils.models.options import CleaningOptions, FeatureOptions
from TweetUtils.helpers.no_mysql_globals import g


class Config(object):
    def __init__(self):
        pass

    def set_defaults(self):
        pass


class UtilsConfig(Config):
    """
    Configuration for cleaning and feature extraction.
    Loaded with defaults for both.
    Options can be modified for both cleaning_options and feature_options afterwards
    """

    def __init__(self, clean=True, get_features=True):
        super(UtilsConfig, self).__init__()
        if not clean and not get_features:
            raise Exception("Configuration is not valid!")
        if clean:
            self.cleaning_options = CleaningOptions()
        else:
            self.cleaning_options = None
        if get_features:
            self.feature_options = FeatureOptions()
        else:
            self.feature_options = None


class TweetConfig(object):

    def __init__(self,
                 text_tagger,
                 text_cleaner,
                 pos_tagger,
                 semantic_analyzer,
                 metaphor_cls
                 ):
        self.text_tagger = text_tagger
        self.text_cleaner = text_cleaner
        self.pos_tagger = pos_tagger
        self.semantic_analyzer = semantic_analyzer
        self.metaphor_cls = metaphor_cls



class TrialConfig(Config):
    def __init__(self, process_config, selected_features, table, discretization=1):
        super(TrialConfig, self).__init__()
        self.selected_features = selected_features
        self.table = table
        self.discretization = discretization
        self.process_config = process_config

    def set_defaults(self):
        self.selected_features = [
                                    '__OH_SO__',                  # * // <<   +
                                    '__DONT_YOU__',               # * // <<   +
                                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                                    '__CAPITAL__',                # * // <<   +
                                    '__HT__',                     # * <<      +
                                    '__HT_POS__',               # * // <<   +
                                    '__HT_NEG__',               # * // <<   +
                                    '__LINK__',               # //
                                    '__POS_SMILEY__',           # * // << +
                                    '__NEG_SMILEY__',           # * // << +
                                    '__NEGATION__',             # * // << +
                                    '__REFERENCE__',            # * // << +
                                    '__questionmark__',         # * // << +
                                    '__exclamation__',          # * // << +
                                    '__fullstop__',
                                    # '__RT__',                 # //
                                    # '__LAUGH__',              # //
                                    #  '__LOVE__',               # //
                                    'postags',              # * // << +
                                    # 'words',              #
                                    # '__swn_score__',
                                    's_word',               # * // << +
                                    # 't_similarity',
                                    # '__res__',                  # * <<
                                    # # '__lin__',
                                    # # '__wup__',
                                    # # '__path__',               # //
                                    # # # '__contains__'
                                    # '__punctuation_percentage__',
                                    # '__hashtag_lexicon_sum__',
                                    # # '__is_metaphor__',
                                    # # '__synset_length__',
                                    # '__multiple_chars_in_a_row__',
                                    # "__lemma_word__"
                                ]
        self.table = "Tweet_Total"
        self.preproccess = False
        self.discretization = g.DISCRETIZATION.ONE


class ProcessConfig(Config):
    def __init__(self, cls_config, preprocess, selected_features, dataset_handler):
        super(ProcessConfig, self).__init__()
        self.preprocess = preprocess
        self.table = "Tweet_Total"
        self.cls_config = cls_config
        self.selected_features = selected_features
        self.dataset_handler = dataset_handler

    def set_defaults(self):
        self.preprocess = False
        self.cls_config.set_defaults()


class ClsConfig(Config):
    def __init__(self,
                 cls_type=g.CLASSIFIER_TYPE.SVMStandalone,
                 vec_type=g.VECTORIZER.Dict,
                 use_tf=True,
                 use_k_best=0,
                 use_cosine=False,
                 k_fold=0,
                 max_vote=False,
                 select_percent=0,
                 use_idf=False,
                 use_hashing=False,
                 variance_threshold=0):
        super(ClsConfig, self).__init__()
        self.cls_type = cls_type
        self.vec_type = vec_type
        self.use_tf = use_tf
        self.use_k_best = use_k_best
        self.use_cosine = use_cosine
        self.k_fold = k_fold
        self.max_vote = max_vote
        self.select_percent = select_percent
        self.use_idf = use_idf
        self.use_hashing = use_hashing
        self.variance_threshold = variance_threshold

    def __str__(self):
        str_self = {
        'cls_type': self.cls_type,
        'vec_type': self.vec_type,
        'use_tf': self.use_tf,
        'use_k_best': self.use_k_best,
        'use_cosine': self.use_cosine,
        'k_fold': self.k_fold,
        'max_vote': self.max_vote,
        'select_percent': self.select_percent,
        'use_idf': self.use_idf,
        'use_hashing': self.use_hashing,
        'variance_threshold': self.variance_threshold
        }
        return json.dumps(str_self)

    def set_defaults(self):
        # todo : remove - not neccessary
        self.cls_type = g.CLASSIFIER_TYPE.SVMStandalone
        self.vec_type = g.VECTORIZER.Dict
        self.use_tf = True
        self.use_k_best = 0
        self.use_cosine = False
        self.k_fold = 0

class DatasetConfig(Config):
    def __init__(self, task11=0, task2b=0, task2bdev=0, one_point_six_m=0, manual=0, manual_1k=0, manual_neutral=0, dict_or_text="dict"):
        super(DatasetConfig, self).__init__()
        self.task11 = task11
        self.task2b = task2b
        self.task2bdev = task2bdev
        self.one_point_six_m = one_point_six_m
        self.manual_1k = manual_1k
        self.manual_neutral = manual_neutral
        self.manual = manual
        self.dict_or_text = dict_or_text

    def __str__(self):
        return "FROM TASK11 :{0}\nFROM TASK2B:{1}\nFROM TASK2BDEV:{2}\nFROM 1.6M POSITIVE:{3}\nFROM 1.6M NEGATIVE:{3}\n" \
               "FROM MANUAL:{4}\nFROM MANUAL 1K:{5}\nFROM MANUAL NEUTRAL:{6}\n".format(self.task11, self.task2b,
                                                                                       self.task2bdev,
                                                                                       self.one_point_six_m,
                                                                                       self.manual, self.manual_1k,
                                                                                       self.manual_neutral)

    def set_defaults(self):
        self.task11 = 15000
        self.task2b = 10000
        self.task2bdev = 10000
        self.one_point_six_m = 20000
        self.manual_1k = 1000
        self.manual_neutral = 10000
        self.manual = 1000

    def set_task11(self):
        self.task11 = 15000
        self.task2b = 0
        self.task2bdev = 0
        self.one_point_six_m = 0
        self.manual_1k = 0
        self.manual_neutral = 0
        self.manual = 0

    def set_task2b(self):
        self.task11 = 0
        self.task2b = 15000
        self.task2bdev = 15000
        self.one_point_six_m = 0
        self.manual_1k = 0
        self.manual_neutral = 0
        self.manual = 0

    def set_one_point_six(self):
        self.task11 = 0
        self.task2b = 0
        self.task2bdev = 0
        self.one_point_six_m = 30000
        self.manual_1k = 0
        self.manual_neutral = 0
        self.manual = 0

    def set_50K(self):
        self.task11 = 15000
        self.task2b = 10000
        self.task2bdev = 10000
        self.one_point_six_m = 10000
        self.manual_1k = 1000
        self.manual_neutral = 10000
        self.manual = 1000

    def set_70K(self):
        self.task11 = 15000
        self.task2b = 10000
        self.task2bdev = 10000
        self.one_point_six_m = 20000
        self.manual_1k = 1000
        self.manual_neutral = 10000
        self.manual = 1000

    def set_90K(self):
        self.task11 = 15000
        self.task2b = 10000
        self.task2bdev = 10000
        self.one_point_six_m = 30000
        self.manual_1k = 1000
        self.manual_neutral = 10000
        self.manual = 1000

    def set_110K(self):
        self.task11 = 15000
        self.task2b = 10000
        self.task2bdev = 10000
        self.one_point_six_m = 30000
        self.manual_1k = 1000
        self.manual_neutral = 30000
        self.manual = 1000