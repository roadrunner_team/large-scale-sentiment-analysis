import ast
from random import shuffle
import traceback

__author__ = 'mariakaranasou'
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from sklearn import cross_validation


class DatasetHandler(object):

    def __init__(self, ds_config):
        if ds_config is None:
            raise Exception("Missing Dataset Configuration.")
        self.ds_config = ds_config
        self.data = []
        self.total_data = []
        self.data_figurative = []
        self.data_task2b = []
        self.data_task2bdev = []
        self.data_manual = []
        self.data_positive = []
        self.data_negative = []
        self.data_neutral = []
        self.data_manual10K = []
        self.total_scores = []
        self.test_set_data = []
        self.test_set_scores = []
        self.trial_set_data = []
        self.trial_set_scores = []
        self._sources = ["Task11", "Task2b", "1.6M", "Manual", "Task2bDev", "manual_test1K", "neutral10K"]
        self._get_data_q = """
                              SELECT id, text, initial_score, feature_dict, train, source
                              FROM SentiFeed.Tweet_Total
                              where source = "{0}" {1} order by id limit {2};
                          """

    def get_data_set(self):

        # get Semeval Task 11 ~ 12000
        if self.ds_config.task11 > 0:
            self.data_figurative = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[0],"", self.ds_config.task11)))

        # get Semeval Task 2 b ~ 9000
        if self.ds_config.task2b > 0:
            self.data_task2b = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[1],"", self.ds_config.task2b)))

        if self.ds_config.task2bdev > 0:
            self.data_task2bdev = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[4],"", self.ds_config.task2bdev)))

        # get Manual data ~ 500
        if self.ds_config.manual > 0:
            self.data_manual = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[3],"", self.ds_config.manual)))

        # get Manual data 1000
        if self.ds_config.manual_1k > 0:
            self.data_manual10K = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[5],"", self.ds_config.manual_1k)))

        # get Manual Neutral data ~ 26000 total
        if self.ds_config.manual_neutral > 0:
            self.data_neutral = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[6],"", self.ds_config.manual_neutral)))

        # get 1.6M ~ 10000 <= x <=20000 from each category
        if self.ds_config.one_point_six_m > 0:
            self.data_positive = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[2], " and initial_score>0 and feature_dict!='' ", self.ds_config.one_point_six_m)))
            self.data_negative = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[2], " and initial_score<0 and feature_dict!='' ", self.ds_config.one_point_six_m)))

        # self.data = list(data_figurative) + list(data_task2b) + list(data_task2bdev) + list(data_manual) + list(data_positive) + list(data_negative)
        self.data = self.data_figurative
        print len(self.data)
        print len(set(self.data))
        shuffle(self.data)
        shuffle(self.data)

    def get_feature_dicts_and_scores(self):
        # test = []
        # text = []
        for row in self.data:
            try:
                # test.append(row[3])
                # text.append(row[1])
                self.total_data.append(ast.literal_eval(row[3]))
                # self.total_data.append(row[3])
                # self.total_data.append(row[1])
                self.total_scores.append(row[2])
            except:
                g.logger.error(traceback.format_exc())
                g.logger.error(str(row))
        # print len(set(test))
        # print len(set(text))
        # import collections
        # print [item for item, count in collections.Counter(test).items() if count > 1]

    def split_data_scores(self):
        i = 0
        for id, text, initial_score, feature_dict, train in self.total_data:
            while i <= (len(self.total_data)*0.8):
                self.test_set_data.append(ast.literal_eval(feature_dict))
                self.test_set_scores.append(initial_score)
                i += 1
            while (len(self.total_data)*0.8) < i <= len(self.total_data):
                self.trial_set_data.append(ast.literal_eval(feature_dict))
                self.trial_set_scores.append(initial_score)
                i += 1

    def get_test_and_trial_set(self):
        self.test_set_data, \
        self.trial_set_data, \
        self.test_set_scores, \
        self.trial_set_scores = cross_validation.train_test_split(self.total_data,
                                                                  self.total_scores,
                                                                  test_size=0.4,
                                                                  random_state=5)

        print "test_set_scores"
        print "---------------"
        pos_test = self.test_set_scores.count(1)
        neu_test = self.test_set_scores.count(0)
        neg_test = self.test_set_scores.count(-1)
        tot_test = pos_test + neg_test + neu_test
        print tot_test
        print "POSITIVE", pos_test, round((float(pos_test)/ float(tot_test)) * 100, 1)
        print "NEUTRAL",  neu_test, round((float(neu_test)/ float(tot_test)) * 100, 1)
        print "NEGATIVE",  neg_test, round((float(neg_test)/ float(tot_test)) * 100, 1)
        print "trial_set_scores"
        print "---------------"
        pos_trial = self.trial_set_scores.count(1)
        neu_trial = self.trial_set_scores.count(0)
        neg_trial = self.trial_set_scores.count(-1)
        tot_trial = pos_trial + neg_trial + neu_trial
        print tot_trial
        print "POSITIVE", pos_trial, round((float(pos_trial)/ float(tot_trial)) * 100, 1)
        print "NEUTRAL", neu_trial, round((float(neu_trial)/ float(tot_trial)) * 100, 1)
        print "NEGATIVE", neg_trial, round((float(neg_trial)/ float(tot_trial)) * 100, 1)
        fig = 0
        manual = 0
        onepointsixM = 0
        task2b = 0

        for each in self.trial_set_data:
            source = self.data[self.total_data.index(each)][5]
            if source == "Task11":
                fig += 1
            elif source == "Manual":
                manual += 1
            elif source == "Task2b":
                task2b += 1
            elif source == "1.6M":
                onepointsixM += 1

        print "FROM 1.6M:", onepointsixM
        print "FROM MANUAL:", manual
        print "FROM FIGURATIVE:", fig
        print "FROM task2b:", task2b




class Dataset(object):
    def __init__(self, data):
        self.data = data
        self.positive = 0
        self.negative = 0
        self.neutral = 0

    def get_percentages(self):
        self.positive = self.data.count(1)
        self.negative = self.data.count(-1)
        self.neutral = self.data.count(0)
