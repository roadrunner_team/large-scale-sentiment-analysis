import ast
from random import shuffle
import traceback
from TweetUtils.helpers.globals import g
from sklearn import cross_validation

__author__ = 'mariakaranasou'


class DatasetHandler(object):
    """
    Used to handle the different datasets and configurations - e.g. get a dataset
    that consists from the tweets in task2 and the tweets in task 11 (figurative tweets)
    """

    def __init__(self, ds_config):
        if ds_config is None:
            raise Exception("Missing Dataset Configuration.")
        self.ds_config = ds_config
        self.data = []
        self.total_data = []
        self.data_figurative = []
        self.data_task2b = []
        self.data_task2bdev = []
        self.data_manual = []
        self.data_positive = []
        self.data_negative = []
        self.data_neutral = []
        self.data_manual1K = []
        self.data_feedback = []
        self.total_scores = []
        self.test_set_data = []
        self.test_set_scores = []
        self.trial_set_data = []
        self.trial_set_scores = []
        self.back_up_test_data = []
        self.back_up_trial_data = []
        self.train_pos = 0
        self.train_neg = 0
        self.train_neu = 0
        self.test_pos = 0
        self.test_neg = 0
        self.test_neu = 0
        self.split = 0.0
        self.random_state = 0.0
        self._sources = ["Task11", "Task2b", "1.6M", "Manual", "Task2bDev", "manual_test1K", "neutral10K", "Feedback"]
        self._get_data_q = """
                              SELECT id, text, initial_score, feature_dict, train, source
                              FROM SentiFeed.Tweet_Total
                              where source = "{0}" {1} order by id limit {2};
                          """

    def get_data_set(self):

        # Feedback
        if self.ds_config.feedback > 0:
            self.data_figurative = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[7],
                                                                                           "",
                                                                                           self.ds_config.feedback)))

        # get Semeval Task 11 ~ 12000
        if self.ds_config.task11 > 0:
            self.data_figurative = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[0],
                                                                                           "",
                                                                                           self.ds_config.task11)))

        # get Semeval Task 2 b ~ 9000
        if self.ds_config.task2b > 0:
            self.data_task2b = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[1],
                                                                                       "",
                                                                                       self.ds_config.task2b)))

        if self.ds_config.task2bdev > 0:
            self.data_task2bdev = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[4],
                                                                                          "",
                                                                                          self.ds_config.task2bdev)))
        # get Manual data ~ 500
        if self.ds_config.manual > 0:
            self.data_manual = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[3],
                                                                                       " ",
                                                                                       self.ds_config.manual)))

        # get Manual data 1000
        if self.ds_config.manual_1k > 0:
            self.data_manual1K = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[5],
                                                                                          "",
                                                                                          self.ds_config.manual_1k)))

        # get Manual Neutral data ~ 26000 total
        if self.ds_config.manual_neutral > 0:
            self.data_neutral = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[6],
                                                                                        "",
                                                                                        self.ds_config.manual_neutral)))

        # get 1.6M ~ 10000 <= x <=30000 from each category
        if self.ds_config.one_point_six_m > 0:
            self.data_positive = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[2],
                                                                 " and initial_score>0 and feature_dict!='' ",
                                                                 self.ds_config.one_point_six_m)))
            self.data_negative = list(g.mysql_conn.execute_query(self._get_data_q.format(self._sources[2],
                                                                 " and initial_score<0 and feature_dict!='' ",
                                                                 self.ds_config.one_point_six_m)))

        self.data = list(self.data_figurative) + \
                    list(self.data_positive) + \
                    list(self.data_negative) + \
                    list(self.data_task2b) + \
                    list(self.data_task2bdev) + \
                    list(self.data_manual) + \
                    list(self.data_neutral) + \
                    list(self.data_manual1K) + \
                    list(self.data_feedback)

        # self._filter(self.data)

        print len(self.data)
        print len(set(self.data))
        # shuffle(self.data)

    def _filter(self, ds):
        tmp = []
        duplicates = []
        remove = []
        for each in ds:
            if each[1][:-2] not in duplicates:
                tmp.append(each)
            else:
                remove.append(each)
            duplicates.append(each[1][:-2])
        print "len(ds)", len(ds)

        for each in remove:  # todo
            ds.remove(each)

        remove = []
        tmp = []
        duplicates = []

        print "len(ds) after", len(ds)

    def get_feature_dicts_and_scores(self):
        test = []
        # text = []
        for row in self.data:
            try:
                if self.ds_config.dict_or_text == "dict":
                    test.append(row[3])
                    self.total_data.append([row[0], ast.literal_eval(row[3])])
                else:
                    test.append(row[1])
                    self.total_data.append([row[0], row[1]])
                self.total_scores.append(row[2])
            except:
                g.logger.error(traceback.format_exc())
                g.logger.error(str(row))
        print len(test), len(set(test))
        test = []
        # print len(set(text))
        # import collections
        # print [item for item, count in collections.Counter(test).items() if count > 1]

    def split_data_scores(self):
        i = 0
        for id, text, initial_score, feature_dict, train in self.total_data:
            while i <= (len(self.total_data)*0.8):
                self.test_set_data.append(ast.literal_eval(feature_dict))
                self.test_set_scores.append(initial_score)
                i += 1
            while (len(self.total_data)*0.8) < i <= len(self.total_data):
                self.trial_set_data.append(ast.literal_eval(feature_dict))
                self.trial_set_scores.append(initial_score)
                i += 1

    def get_test_and_trial_set(self, split=0.2, random_state=5):
        # self.test_set_data = self.total_data
        # self.test_set_scores = self.total_scores
        # self.trial_set_data = [ast.literal_eval(x[3]) for x in self.data_manual]
        # self.trial_set_scores = [x[2] for x in self.data_manual]

        self.split = split
        self.random_state = random_state
        self.test_set_data, \
        self.trial_set_data, \
        self.test_set_scores, \
        self.trial_set_scores = cross_validation.train_test_split(self.total_data,
                                                                  self.total_scores,
                                                                  test_size=self.split,
                                                                  random_state=5)
        self.get_back_up()
        self.get_percentages()
        self.get_clean_data_list()

    def get_back_up(self):

        for each in self.test_set_data:
            self.back_up_test_data.append(each[0])

        for each in self.trial_set_data:
            self.back_up_trial_data.append(each[0])

    def get_clean_total_data_list(self):
        self.total_data = [x[1] for x in self.total_data]

    def get_clean_data_list(self):
        self.test_set_data = [x[1] for x in self.test_set_data]
        self.trial_set_data = [x[1] for x in self.trial_set_data]

    def get_percentages(self):
        print "test_set_scores"
        print "---------------"
        self.test_pos = self.test_set_scores.count(1)
        self.test_neu = self.test_set_scores.count(0)
        self.test_neg = self.test_set_scores.count(-1)
        tot_test = self.test_pos + self.test_neg + self.test_neu

        print tot_test
        print "POSITIVE", self.test_pos, round((float(self.test_pos) / float(tot_test)) * 100, 1)
        print "NEUTRAL",  self.test_neu, round((float(self.test_neu) / float(tot_test)) * 100, 1)
        print "NEGATIVE",  self.test_neg , round((float(self.test_neg) / float(tot_test)) * 100, 1)
        print "trial_set_scores"
        print "---------------"
        self.train_pos = self.trial_set_scores.count(1)
        self.train_neu = self.trial_set_scores.count(0)
        self.train_neg = self.trial_set_scores.count(-1)
        tot_trial = self.train_pos + self.train_neg + self.train_neu

        print tot_trial
        print "POSITIVE", self.train_pos, round((float(self.train_pos) / float(tot_trial)) * 100, 1)
        print "NEUTRAL", self.train_neu, round((float(self.train_neu) / float(tot_trial)) * 100, 1)
        print "NEGATIVE", self.train_neg , round((float(self.train_neg) / float(tot_trial)) * 100, 1)
        fig = 0
        manual = 0
        onepointsixM = 0
        task2b = 0
        manual_1k = 0
        manual_30k = 0

        for each in self.trial_set_data:
            source = self.data[self.total_data.index(each)][5]
            if source == "Task11":
                fig += 1
            elif source == "Manual":
                manual += 1
            elif source == "Task2b" or source == "Task2bDev":
                task2b += 1
            elif source == "1.6M":
                onepointsixM += 1
            elif source == "manual_test1K":
                manual_1k += 1
            elif source == "neutral10K":
                manual_30k += 1

        print "FROM 1.6M:", onepointsixM
        g.logger.info("FROM 1.6M:{0}".format(onepointsixM))
        print "FROM MANUAL:", manual
        g.logger.info("FROM MANUAL:{0}".format(manual))
        print "FROM FIGURATIVE:", fig
        g.logger.info("FROM FIGURATIVE:{0}".format(fig))
        print "FROM task2b:", task2b
        g.logger.info("FROM task2b:{0}".format(task2b))
        print "FROM manual_test1K:", manual_1k
        g.logger.info("FROM manual_test1k:{0}".format(manual_1k))
        print "FROM manual_30k:", manual_30k
        g.logger.info("FROM manual_30k:{0}".format(manual_30k))

    def save(self, trial_id, predictions):
        try:
            ids = []

            # save dataset settings first
            g.mysql_conn.execute_query(self.insert_dataset_settings_q().format(
                                        trial_id,
                                        len(self.data_figurative),
                                        len(self.data_task2b) + len(self.data_task2bdev),
                                        len(self.data_positive),
                                        len(self.data_negative),
                                        len(self.data_manual),
                                        len(self.data_manual1K),
                                        len(self.data_neutral),
                                        self.split,
                                        self.random_state,
                                        len(self.test_set_scores),
                                        len(self.trial_set_scores),
                                        self.train_pos,
                                        self.train_neg,
                                        self.train_neu,
                                        self.test_pos,
                                        self.test_neg,
                                        self.test_neu
            ))

            # then for each prediction save
            for i in xrange(0, len(self.trial_set_data)):
                id = self.back_up_trial_data[i]
                score = self.trial_set_scores[i]
                if id not in ids:
                    self.insert_score(trial_id, id, score, predictions[i])
                    ids.append(id)
                else:
                    print "DUPLICATE ID INDEX FOR ", id
        except:
            traceback.print_exc()
            print "ERROR in DATASET HANDLER WHILE SAVING"
            pass

    @staticmethod
    def insert_dataset_settings_q():
        return ''' INSERT INTO SentiFeed.DatasetSettings
                    (
                    trials_id,
                    task11,
                    task2b,
                    one_point_six_m_positive,
                    one_point_six_m_negative,
                    manual,
                    manual_1k,
                    manual_neutral,
                    split,
                    random_state,
                    train_set,
                    test_set,
                    train_pos,
                    train_neg,
                    train_neu,
                    test_pos,
                    test_neg,
                    test_neu
                    )
                    VALUES
                    (
                    {0},
                    {1},
                    {2},
                    {3},
                    {4},
                    {5},
                    {6},
                    {7},
                    {8},
                    {9},
                    {10},
                    {11},
                    {12},
                    {13},
                    {14},
                    {15},
                    {16},
                    {17}
                    );
                    '''

    @staticmethod
    def insert_scores_q():
        return '''
                INSERT INTO SentiFeed.Scores
                (
                trials_id,
                tweet_id,
                expected,
                predicted
                )
                VALUES
                (
                {0},
                {1},
                {2},
                {3}
                );
                '''

    def insert_score(self, trial_id, id, initial_score, predicted):
        g.mysql_conn.execute_query(self.insert_scores_q().format(trial_id, id, initial_score, predicted))
