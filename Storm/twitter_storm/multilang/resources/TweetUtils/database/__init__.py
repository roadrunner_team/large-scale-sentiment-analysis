# coding: utf-8
import json
import MySQLdb
import traceback

__author__ = 'maria'


class MySQLDatabaseConnector(object):
    def __init__(self):
        def __init__(self, cfg_path="../data/config.json"):
            with open(cfg_path) as cfg:
                config = json.loads(cfg)
                print config
            try:
                # use this if you use MySQLdb (usually in windows)
                self.conn = MySQLdb.connect(host=config["MySQL"]["host"],
                                            user=config["MySQL"]["user"],
                                            passwd=config["MySQL"]["passwd"],
                                            db=config["MySQL"]["db"])

                # use this if you use mysql.connector
                # self.conn = mysql.connector.connect(user=config["MySQL"]["user"],
                #                                     password=config["MySQL"]["passwd"],
                #                                     host=config["MySQL"]["host"],
                #                                     database=config["MySQL"]["db"])

                self.query = ''
                # self.conn.autocommit(True)
                self.cur = self.conn.cursor()
            except:
                traceback.print_exc()
                raise Exception("Could not connect to MySQL, check if server is running!")

    def lastrowid(self):
        return self.cur._lastrowid

    def connect(self):
        self.conn = MySQLdb.connect(host="127.0.0.1",
                                        user="root",
                                        passwd="",
                                        db="SentiFeed")

    def execute_query(self, query):
        # self.cur.close()
        cursor = None
        try:
            cursor = self.conn.cursor()
            cursor.execute(query)
        except (AttributeError, MySQLdb.OperationalError):
            self.connect()
            cursor = self.conn.cursor()
            cursor.execute(query)

        data = cursor.fetchall()
        # self.conn.ping(True)
        # cur = self.conn.cursor()
        # cur.execute(query)
        # rows = cur.fetchall()
        cursor.close()

        return data

    def update(self, query):
        # self.conn.ping(True)
        try:
            cursor = self.conn.cursor()
            cursor.execute(query)
        except (AttributeError, MySQLdb.OperationalError):
            print "CONN CLOSED"
            self.connect()
            cursor = self.conn.cursor()
            cursor.execute(query)
        self.conn.commit()
        last_row_id = cursor.lastrowid
        cursor.close()
        return last_row_id

    def close_conn(self):
        self.conn.close()