from multiprocessing import Pool
import traceback
import datetime
from TweetUtils.models.classifier import Classifier
from TweetUtils.cleaning.text_cleaner import TextCleaner
from TweetUtils.feature_extraction.pos_tagger import POSTagger
from TweetUtils.feature_extraction.semantic_analyser import SemanticAnalyzer
from TweetUtils.feature_extraction.text_tagger import TextTagger
from TweetUtils.helpers.globals import g
from TweetUtils.models.config import TweetConfig, ClsConfig, UtilsConfig, DatasetConfig
from TweetUtils.models.dataset_handler import DatasetHandler
from TweetUtils.models.tweet import Tweet

__author__ = 'maria'


class TweetUtils(object):
    """

    """
    def __init__(self, configuration, metaphor_cls):
        if configuration is None:
            raise Exception("No configuration provided!")
        if type(configuration) is not UtilsConfig:
            raise Exception("Wrong type of Configuration provided!")
        self.configuration = configuration
        self.tweet_list = None
        self.tweet_text = None
        self.results = None
        self.text_tagger = None
        self.text_cleaner = None
        self.pos_tagger = None
        self.semantic_analyzer = None
        self.metaphor_cls = metaphor_cls
        self.tweet_config = None
        self._init_processors()

    def process(self, tweet_list_or_text, input_file_path=None, output_file_path=None, delimiter=";", include_original=True):
        """
        Returns a list of processed Tweet objects.
        In case input_file_path and output_file_path are specified, tweet_list_or_text is ignored and the result is
        stored in output_file_path file in the form of:
        tweet text {delimiter} cleaned tweet text {delimiter} feature dictionary
        :param tweet_list_or_text: a list of tweet text of a single tweet text
        :param input_file_path: optional: file to read tweet text from - one per line
        :param output_file_path: optional: file to store processed tweet text
        :param delimiter: optional: default is comma ; . It is used when input_file_path and output_file_path are set
        :param include_original: boolean to indicate if the original tweet text will be included in the output_file_path
        :return: results: list of Tweet objects
        """
        self.results = []

        if input_file_path is not None and input_file_path is not None:
            with open(input_file_path, 'rb') as in_file:
                for line in in_file.readlines():
                    self.results.append(self._process(line.strip('\n').strip('\r')))

            with open(output_file_path, 'w') as out_file:
                for each in self.results:
                    if include_original:
                        out_file.write(each.text.strip("\n").strip("\r")
                                       + delimiter
                                       + " ".join(each.words)
                                       + delimiter
                                       + str(each.feature_dict) + "\n")
                    else:
                        out_file.write(str(each.feature_dict) + "\n")
        else:

            if type(tweet_list_or_text) is list:
                self.tweet_list = tweet_list_or_text
                for tweet_text in self.tweet_list:
                    self.results.append(self._process(tweet_text))
            elif type(tweet_list_or_text) is str:
                self.results = self._process(tweet_list_or_text)

            else:
                raise Exception("Wrong parameter types for function")

        return self.results

    def _process(self, tweet_text):
        """
            Creates a tweet object using tweet_text and processes it depending on configuration.
            :param tweet_text:
            :type tweet_text: str
            :return: Tweet object processed
            :rtype: Tweet
        """
        if type(tweet_text) is str:
            tweet = Tweet(None, tweet_text, self.tweet_config)
            # First process morphological features
            if self.configuration.feature_options is not None:
                tweet.tag()
            # Then proceed to cleaning
            if self.configuration.cleaning_options is not None:
                tweet.clean()

            if self.configuration.feature_options is not None:
                # postags must be the first to be processed because the following features require pos-tagged text
                if self.configuration.feature_options.get_feature_by_name("postags") is not None:
                    tweet.pos_tag()
                # calculate sentiwordnet score for each word and the whole tweet
                if self.configuration.feature_options.get_feature_by_name("s_word") is not None:
                    tweet.get_swn_score()

                tweet.contains_metaphor()
                tweet.get_synonym_lengthing()
                tweet.get_multiple_char_words()
                tweet.get_words_to_swn_score_dict()
                # finally gather all features
                tweet.gather_dicts()

                # calculate all four types of text similarity
                if self.configuration.feature_options.get_feature_by_name("res") is not None:
                    tweet.feature_dict['res'] = tweet.get_semantic_similarity('res')
                # if self.configuration.feature_options is not None and \
                #    self.configuration.feature_options.get_feature_by_name("lin") is not None:
                #     tweet.feature_dict['lin'] = tweet.get_semantic_similarity('lin')
                if self.configuration.feature_options.get_feature_by_name("path") is not None:
                    tweet.feature_dict['path'] = tweet.get_semantic_similarity('path')
                if self.configuration.feature_options.get_feature_by_name("wup") is not None:
                    tweet.feature_dict['wup'] = tweet.get_semantic_similarity('wup')

        else:
            raise Exception("Not suitable data type {0}".format(type(tweet_text)))
        return tweet

    def _init_processors(self):
        if self.configuration.feature_options is not None:
            self.text_tagger = TextTagger(self.configuration.feature_options)     # initialize Tagger w/ configuration
            self.pos_tagger = POSTagger()
            self.semantic_analyzer = SemanticAnalyzer()

        if self.configuration.cleaning_options is not None:
            self.text_cleaner = TextCleaner(self.configuration.cleaning_options)  # initialize Cleaner w/ configuration

        self.tweet_config = TweetConfig(self.text_tagger,
                                        self.text_cleaner,
                                        self.pos_tagger,
                                        self.semantic_analyzer,
                                        self.metaphor_cls)

    def _get_metaphor_classifier(self):
        self.metaphor_cls = Classifier(g.CLASSIFIER_TYPE.SVMStandalone, g.VECTORIZER.Count, use_k_best=False)
        q_metaphor = "SELECT tweet_text FROM SentiFeed.Metaphors;"
        q_others_positive = "SELECT text FROM SentiFeed.tweet_total where source='1.6M' and initial_score > 0  limit 3225;"
        q_others_negative = "SELECT text FROM SentiFeed.tweet_total where source='1.6M' and initial_score < 0  limit 3225;"

        met_data = [x[0] for x in g.mysql_conn.execute_query(q_metaphor)]
        oth_data_positive = [x[0] for x in g.mysql_conn.execute_query(q_others_positive)]
        oth_data_negative = [x[0] for x in g.mysql_conn.execute_query(q_others_negative)]

        t_met_data = [[x, True] for x in met_data]
        t_oth_pos_data = [[x, False] for x in oth_data_positive]
        t_oth_neg_data = [[x, False] for x in oth_data_negative]

        final_data = t_met_data + t_oth_neg_data + t_oth_pos_data

        self.metaphor_cls .set_y_train([x[1] for x in final_data])
        self.metaphor_cls .set_x_train([x[0] for x in final_data])
        self.metaphor_cls .train()

# examples
# utils = TweetUtils(Config(True, True))
# tweet = utils.process(["This is lovely!!!#NOT :( :) http://dsgrg.vom/vfda", "I hate Monday mornings... :) :( !!!"])[0]
# print tweet.clean_text
# print tweet.feature_dict
# # for tweet in tweets:
# #     print str(tweet), tweet.feature_dict
# #
# # other_tweet = utils.process("This is lovely!!!#NOT")
# # print str(other_tweet), other_tweet.feature_dict
#
# tweets_from_file = utils.process(None, "../TweetUtils/data/tweets.txt", "../TweetUtils/data/output_file_test.txt")
#
if __name__ == "__main__":
    completed = 0
    ds_config = DatasetConfig()
    ds_config.feedback = 6000
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count, use_k_best=False)

    metaphor_cls = Classifier(cls_config)

    try:
        metaphor_cls.load_model("Metaphor")
    except:
        q_metaphor = "SELECT tweet_text FROM SentiFeed.Metaphors;"
        q_others_positive = "SELECT text FROM SentiFeed.tweet_total where source='1.6M' and initial_score > 0  limit 3225 OFFSET 100000;"
        q_others_negative = "SELECT text FROM SentiFeed.tweet_total where source='1.6M' and initial_score < 0  limit 3225 OFFSET 100000;"

        met_data = [x[0] for x in g.mysql_conn.execute_query(q_metaphor)]
        oth_data_positive = [x[0] for x in g.mysql_conn.execute_query(q_others_positive)]
        oth_data_negative = [x[0] for x in g.mysql_conn.execute_query(q_others_negative)]

        t_met_data = [[x, True] for x in met_data]
        t_oth_pos_data = [[x, False] for x in oth_data_positive]
        t_oth_neg_data = [[x, False] for x in oth_data_negative]

        final_data = t_met_data + t_oth_neg_data + t_oth_pos_data

        metaphor_cls.set_y_train([x[1] for x in final_data])
        metaphor_cls.set_x_train([x[0] for x in final_data])
        metaphor_cls.train()
        # metaphor_cls.save_model("Metaphor")
        # pass


    # utils = TweetUtils(UtilsConfig(), metaphor_cls)
    # tweet = utils.process("@Someone this is #perfection!!!")
    # print tweet.feature_dict

    dataset_handler = DatasetHandler(ds_config)
    dataset_handler.get_data_set()
    # dataset_handler.get_clean_total_data_list()
    print "Data lenght:", len(dataset_handler.data)

    def process(tweet_tuple):
        global completed
        try:
            utils = TweetUtils(UtilsConfig(), metaphor_cls)
            text = tweet_tuple[1].replace("\n", "").replace("'", "\'").replace("\"", "\\\"").replace("\r", "")
            tweet = utils.process(text)
            tweet.id = tweet_tuple[0]
            # tweet.update()
        except:
            print traceback.format_exc(), tweet_tuple
            g.logger.error(tweet_tuple)
        completed += 1
        if completed % 100 == 0:
            print completed, datetime.datetime.now()

    print "start", datetime.datetime.now()
    num_of_threads = 1
    skip = 0
    pool = Pool(processes=num_of_threads)
    pool.map(process, dataset_handler.data)
    print completed
    print "finished", datetime.datetime.now()
