import copy
from TweetUtils.helpers.no_mysql_globals import g
__author__ = 'mariakaranasou'


class FeatureSelector(object):
    """
    
    """
    def __init__(self, selected_features, use=0):
        self.use = use
        self.tweet = None
        self.selected_features = selected_features
        self.sentiment_discretization_values = ["positive", "negative", "somewhat_positive", "somewhat_negative", "neutral"]
        self.semantic_similarity = ['__path__', '__lin__', '__wup__', '__res__']
        self.tags = ['__OH_SO__',                  # * // <<   +
                     '__DONT_YOU__',               # * // <<   +
                     '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                     '__CAPITAL__',                # * // <<   +
                     '__HT__',                     # * <<      +
                     '__HT_POS__',                 # * // <<   +
                     '__HT_NEG__',                 # * // <<   +
                     '__LINK__',                   # //
                     '__POS_SMILEY__',             # * // << +
                     '__NEG_SMILEY__',             # * // << +
                     '__NEGATION__',               # * // << +
                     '__REFERENCE__',              # * // << +
                     '__questionmark__',           # * // << +
                     '__exclamation__',            # * // << +
                     '__fullstop__']

    def update_feature_dict(self, tweet):
            try:
                tweet["feature_dict_original"] = copy.deepcopy(tweet["feature_dict"])  # keep the original just in case
                self._get_clean_feature_dict(tweet["feature_dict"])                    # clean feature_dict

            except Exception:
                g.logger.error("PROBLEM WITH:\t%s" % tweet["id"], exc_info=True)

            return tweet

    def excluded(self, k, dicta_k_value):
        if 's_word' in k:                                   # if k is a sentiwordnet feature
            if 's_word' not in self.selected_features:      # if swn is not wanted
                return True                                 # remove
            return False
        if '__contains__' in k:                                # if k is a sentiwordnet feature
            if '__contains__' not in self.selected_features:   # if swn is not wanted
                return True                                 # remove
            return False
        if '__synset_length__' in k:                                # if k is a sentiwordnet feature
            if '__synset_length__' not in self.selected_features:   # if swn is not wanted
                return True                                     # remove
            return False
        if '__swn_score__' == k:                                # if k is sentiwordnet total score
            if '__swn_score__' not in self.selected_features:   # if swn is not wanted
                return True                                 # remove
            return False
        if 'pos_position_' in k:
            if 'pos_position_' not in self.selected_features:
                return True
            return False

        if dicta_k_value in g.VERBS or \
            dicta_k_value in g.ADJECTIVES or \
            dicta_k_value in g.ADVERBS or \
            dicta_k_value in g.NOUNS or \
            dicta_k_value in g.OTHERS:
            if 'postags' not in self.selected_features:
                return True
            return False
        if 'word-' in dicta_k_value:
            if 'words' not in self.selected_features or 'word' not in self.selected_features:
                return True
            return False
        if 't-similarity' in k:
            if 't_similarity' not in self.selected_features:
                return True
            return False
        if '__res__' == k:
            if '__res__' not in self.selected_features:
                return True
            return False
        if '__lin__' == k:
            if '__lin__' not in self.selected_features:
                return True
            return False
        if '__path__' == k:
            if '__path__' not in self.selected_features:
                return True
            return False
        if '__wup__' == k:
            if '__wup__' not in self.selected_features:
                return True
            return False
        if dicta_k_value in self.sentiment_discretization_values:
            if "__lemma_word__" not in self.selected_features:
                return True
            return False
        if k not in self.selected_features:
            return True
        return False


    def _get_clean_feature_dict(self, dicta):

        for k in dicta.keys():
            if not self.excluded(k, str(dicta[k])):
                if 's_word-' in k or 'swn_score' in k:
                        # dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral" \
                        #             if 0.95 <= dicta[k] <= 1.05\
                        #             else "somewhat_positive" if 1.2 >= dicta[k] > 1.05 \
                        #             else "somewhat_negative"

                    if dicta[k] > 1.2:
                        dicta[k] = "positive"
                    elif dicta[k] < 0.2:
                        dicta[k] = "negative"
                    elif 0.95 <= dicta[k] <= 1.05:
                        dicta[k] = "neutral"
                    elif 0.95 > dicta[k] > 0.2:
                        dicta[k] = "somewhat_negative"
                    elif 1.05 < dicta[k] < 1.2:
                        dicta[k] = "somewhat_positive"

                    if self.use > 0:
                        if dicta[k] in g.VERBS:
                            dicta[k] = "VB"
                        elif dicta[k] in g.NOUNS:
                            dicta[k] = "NN"
                        elif dicta[k] in g.ADJECTIVES:
                            dicta[k] = "ADJ"
                        elif dicta[k] in g.ADVERBS:
                            dicta[k] = "RB"

                    if k == "__punctuation_percentage__":
                        dicta[k] = 1 if dicta[k] > 5 else 0
                    # if k == "__multiple_chars_in_a_row__":
                    #     dicta[k] = 0 if dicta[k] == False else 1
                    #
                    # if k in self.tags:
                    #     dicta[k] = 0 if dicta[k] == "False" else 1
                    # if self.use > 0:
                    # if "__lemma_word__" in k:
                    #     dicta[k] = "True"

            else:
                del dicta[k]