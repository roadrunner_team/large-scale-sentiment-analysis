import string
from nltk import WordNetLemmatizer
from TweetUtils.helpers.pyenchant_spell_checker import EnchantSpellChecker
from TweetUtils.helpers.globals import g
import re

__author__ = 'm.karanasou'

# Globals
wnl = WordNetLemmatizer()


class NameHelper(object):
    """
    http://public.oed.com/how-to-use-the-oed/abbreviations/
    """
    def __init__(self, deploy_abbrev=True, pos_tag=True):
        self.name = ""
        self.split_name = None
        self.has_been_split = False
        self.pos_tagger = None
        self.spell_checker = EnchantSpellChecker()
        self.proper_names = []
        self.deploy_abbrev = deploy_abbrev
        self.pos_tag = pos_tag

    def singularize(self, word_plural):
        """
        http://stackoverflow.com/questions/18911589/how-to-test-whether-a-word-is-in-singular-form-or-not-in-python
        :param word_plural:
        :return:
        """
        # todo: check this out https://pypi.python.org/pypi/inflect

        plurar_result = self.is_plural(word_plural)

        if plurar_result[0]:
            return plurar_result[1]
        else:
            return word_plural

    def plurar_to_singular(self):
        for nameIndex in range(0, len(self.proper_names)):
            self.proper_names[nameIndex] = self.singularize(self.proper_names[nameIndex])
        return self.proper_names

    def deploy_abbreviations(self):
        for each in self.split_name:
            if each in g.abbreviations:
                # print "abbrev ", each, g.abbreviations[each]
                self.split_name[self.split_name.index(each)] = g.abbreviations[each]

    def split_all_lower(self, word):
        # from math import log
        # Build a cost dictionary, assuming Zipf's law and cost = -math.log(probability).
        # words = open("../data/words-by-frequency.txt").read().split()
        # g.words
        # g.wordcost = dict((k, log((i+1)*log(len(words)))) for i, k in enumerate(words))
        # g.maxword = max(len(x) for x in g.words)

        """Uses dynamic programming to infer the location of spaces in a string
        without spaces."""

        # Find the best match for the i first characters, assuming cost has
        # been built for the i-1 first characters.
        # Returns a pair (match_cost, match_length).
        def best_match(i):
            candidates = enumerate(reversed(cost[max(0, i-g.max_word):i]))
            return min((c + g.word_cost.get(word[i-k-1:i], 9e999), k+1) for k, c in candidates)

        # Build the cost array.
        cost = [0]
        for i in range(1, len(word)+1):
            c, k = best_match(i)
            cost.append(c)

        # Backtrack to recover the minimal-cost string.
        out = []
        i = len(word)
        while i > 0:
            c, k = best_match(i)
            assert c == cost[i]
            out.append(word[i-k:i])
            i -= k

        return reversed(out)

    def remove_special_chars(self):
        """
            Will store and remove any characters like '?' '!!!' '...'
            :return:
        """

        for each in self.split_name:
            self.split_name[self.split_name.index(each)] = re.sub(r'\W|\d', '', each)

    @staticmethod
    def is_plural(word):
        lemma = wnl.lemmatize(word, 'n')
        plural = True if word is not lemma else False
        return plural, lemma

    def _can_be_split(self):
        # a name can be like TollControl_AFM or sourcetype
        has_been_split = False
        pascal_or_camel_case = False
        all_lower = False
        temp = []

        # Remove digits
        self.name = re.sub(r'\d', '', self.name)

        # check if word in capitals exist
        # capitalized = re.findall(r'\b[A-Z]{3,}\b', self.name)
        # print("capitalized", capitalized)

        # Check snake_case
        if "_" in self.name:
            self.split_name = self.name.split("_")
            has_been_split = True

        # this means we have a possible TollControl_AFM-like attribute
        if has_been_split:
            # try further split for each in split words
            for split_word in self.split_name:
                if split_word != '':
                    word = re.sub(r'((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))', r' \1', split_word)
                    split_temp = word.split(' ')

                    # we split 'Toll Control' to 'Toll' 'Control'
                    if len(split_temp) > 1:
                        for each in split_temp:
                            temp.append(each)
                        pascal_or_camel_case = True
                    else:
                        temp.append(split_temp[0])
        else:
            temp_word = re.sub(r'((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))', r' \1', self.name)
            temp = temp_word.split(' ')

        if len(temp) == 1 and not temp[0] is "":
            if not self.spell_checker.is_correct(temp[0]):
                all_lower = True
                temp = [x for x in self.split_all_lower(temp[0].lower())]  # tollcontrol

        if pascal_or_camel_case or len(temp) >= 1 or all_lower:
            self.split_name = [a.lower() for a in temp]
            has_been_split = True

        return has_been_split

    def identify_lang(self):
        """
        https://github.com/saffsd/langid.py
        :return:
        """

    def translate(self):
        pass

    def _get_proper_name(self):
        self.has_been_split = self._can_be_split()
        if self.has_been_split:
            if self.deploy_abbrev:
                self.deploy_abbreviations()
            self.remove_special_chars()
            if self.pos_tag:
                pass
                # self.proper_names = self.pos_tagger.pos_tag(self.split_name)
            else:
                self.proper_names = self.split_name
        else:
            self.proper_names.append(self.name)

        # singularize
        self.plurar_to_singular()
        g.logger.debug("PROPER NAMES:\t{0}\t{1}".format(self.name, self.proper_names))
        return self.proper_names

    def remove_non_ascii_chars(self, txt):
        """
        to solve problem with extra / weird characters when getting data from database
        :return:
        """
        return str(filter(lambda x: x in string.printable, txt))

    def get_name(self, name):
        self.name = self.remove_non_ascii_chars(name)
        return self._get_proper_name()
