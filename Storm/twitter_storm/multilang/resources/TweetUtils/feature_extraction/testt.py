from nltk.corpus import wordnet as wn

tokens = ["dog"]

for token in tokens:
    syn_sets = wn.synsets(token)
    for syn_set in syn_sets:
       # print(syn_set, syn_set.lemma_names())
        print(syn_set.definition())