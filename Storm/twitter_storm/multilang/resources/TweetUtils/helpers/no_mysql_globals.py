import ast
import re
import datetime
from TweetUtils.helpers.static_paths import DATA_FOLDER_PATH, LOG_FOLDER_PATH
from nltk.corpus.reader.wordnet import NOUN, VERB, ADJ, ADV
import logging
import inspect
import os
import platform

__author__ = 'maria'


class Globals(object):
    """
    General purpose class to hold global variables, such as db connection and logger instance.
    """
    def __init__(self):
        self.train_model = {}
        self.abbreviations = {}
        self.words = None
        self.ckey = 'UmHSnVjiOnI8uUOQQtLQqQ'
        self.csecret = 'CJxZ7790yM3bJgyymgLFzXyimugjY6b820xCMW8xS0U'
        self.atoken = '85281300-55pVbdrlhWTPSNq5dLj2z8oR0zRcX4kdmRVOfkEei'
        self.asecret = 'j9SPfNbCOdLQpZzcBjpJHMgFGTibb9AD7rRf4gCgV1cHF'
        self.logger = None
        self.handler = None
        self.formatter = None
        self.collection_name = 'tweet_test_collection'
        self.redis_conn = None
        self.mongo = None
        self.mysql_conn = None
        self.THREAD_LIST = []
        self.NOUNS = ['N', 'NP', 'NN', 'NNS', 'NNP', 'NNPS']
        self.VERBS = ['V', 'VD', 'VG', 'VN', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
        self.ADJECTIVES = ['ADJ', 'JJ', 'JJR', 'JJS']
        self.ADVERBS = ['RB', 'RBR', 'RBS', 'WRB']
        self.OTHERS = ['IN', 'PRP$', 'CC', "''", "USR", "UH", "TO" , ',', '.', 'MD', 'DT', ')', 'WRB', 'URL', 'HT', ':', "``", "CD", "RP", "RT", "EX", 'POS', 'WP', '(', 'WDT', 'FW', "$", "PDT", "SYM"]
        # self.LINK_PATTERN = r'(?P<url>https?://[^\s]+)'
        """
        ref: https://gist.github.com/uogbuji/705383
        """
        self.LINK_PATTERN = r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?\xab\xbb\u201c\u201d\u2018\u2019]))'
        self.HT_PATTERN = r'#\w+'
        self.RT_PATTERN = r'\b(^RT:{0,1}|rt:{0,1})\b'
        self.LAUGHING_PATTERN_haha = '\b(a?((h)*(a)*)*)\b'
        self.LAUGHING_PATTERN_lol= '(?ix)\b(l(o)*(l)*)\1*\b'
        self.LAUGHING_PATTERN_rotfl = '(?ix)\b(?=ro(t)*fl)'
        self.LAUGHING_PATTERN_omg = '(?ix)\b(?=o(m)*(g)*)'
        self.LAUGH_PATTERN = r'\b(a*ha+h[ha]*|o?l+o+l+[ol]*)\b'
        # self.POS_SMILEY_PATTERN = r'(:-\)|:-D|:-\)\)|:\)\)|\(:|:\)|:D|=\)|;-\)|XD|=D|=]|;D|:]|:o\)|<3|&lt;?3)'
        self.POS_SMILEY_PATTERN = r'(:-\){1,}|:-D{1,}|\({1,}:|:\){1,}|:D{1,}|=\){1,}|;-\){1,}|XD{1,}|=D{1,}|=]|;D|:]|:o\){1,}|<3{1,}|&lt;?3{1,}|\({1,}-?:)'  # todo: can be done more clearly and short
        # self.NEG_SMILEY_PATTERN = r'(:-\\|:-\(|:-\(\(|:\(|:o|:O|D:|=/|=\(|:\'\(|:\'-\(|:\\|:/|:S)'  # bug fix: ! --> |
        self.NEG_SMILEY_PATTERN = r'(:-\\{1,}|:-\({1,}|:\({1,}|:o{1,}|:O{1,}|D:|=/|=\({1,}|:\'\({1,}|:\'-\({1,}|:\\{1,}|:/{1,}|:S|o.(O|o)|<(/|\\)3|&lt;?(/|\\)3|\){1,}-?:)'   # todo: can be done more clearly and short
        self.SMILEY_FULL_PATTERN = r'(:-\)|:-\(|:-\(\(|:-D|:-\)\)|:\)\)|\(:|:\(|:\)|:D|=\)|;-\)|XD|=D|:o|:O|=]|D:|;D|:]|=\/|=\(!:\'\(|:\'-\(|:\\|:\/|:S|<3)'
        self.NEGATIONS_PATTERN = r'\b#{0,1}(no|not|isnt|isn\'t|isn;t|don\'t|won\'t|couldn\'t|can\'t|didn\'t|not|didnt|didn;t' \
                                 r'|dont|don;t|don t|won t|can;t|cant|can t|doesn\'t|doesnt|doesn t|doesn;t|cannot|did not|not|wasn(\'|;)?t)\b'
        self.CAPITALS_PATTERN = r'\b[A-Z]{2,}\b'
        self.REFERENCE_PATTERN = r'(@\w+|@\D\w+|@\d\w+)'
        self.LOVE_PATTERN = r'(<3|&lt;?3)'

        self.NEGATIONS = ['no', 'don\'t', 'won\'t', 'couldn\'t', 'can\'t', 'didn\'t', 'not', 'could not', 'cannot']

        self.OH_SO_PATTERN = r'(O|o)h,?\s?(so)?\s?(you)?\w+'
        self.DONT_YOU_PATTERN = r'(D|d)on\'?;?\s?t\s(you|u)'
        self.AS_GROUND_AS_VEHICLE_PATTERN = r'(A|a)s\s\w+\sas\s\w+\s?'
        self.MUST_KNOW_ABBREV = ['RT', 'MT', 'DM', 'FF', '@reply']
        self._init_enums()
        self.pos_literal = {'n': NOUN,
                            'v': VERB,
                            'a': ADJ,
                            'r': ADV}
        self.ROOT_PATH = os.path.dirname(os.path.realpath(__file__))
        self.PROJECT_ROOT_PATH = str(self.ROOT_PATH).replace('helpers','')
        self.TWITTIE_JAR_PATH = self.PROJECT_ROOT_PATH + '/data/twitie-tagger/twitie_tag.jar'
        self.TWITTIE_MODEL_PATH = self.PROJECT_ROOT_PATH + '/data/twitie-tagger/models/gate-EN-twitter.model'
        self.DATESTAMP = str(datetime.datetime.today()).replace(' ', '_').replace(':', '_')
        self.log_separator = "#################################{0}####################################################"
        self.LOG_PATH = LOG_FOLDER_PATH  # self.PROJECT_ROOT_PATH + "/logs/"
        self.LOG_FILE_PATH = self.LOG_PATH + "tweet_utils{0}.logs".format(self.DATESTAMP)
        self._init_logger()


    def parse_abbreviations(self):
        # todo: store it in db? tbd
        # if len(self.abbreviations) == 0:
        #     q = """SELECT abbreviation, expansion FROM sentifeed.abbreviations;"""
        #     data = self.mysql_conn.execute_query(q)
        #     for each in data:
        #         self.abbreviations[each[0]] = each[1]
        pass

    def _prepare_gate_pos_tags_model(self):
        # stored processed model as a dict in file so to minimize load time
        with open(self.PROJECT_ROOT_PATH+'data/model_dict.txt', 'r') as f:
            self.train_model = ast.literal_eval(f.read())
# ================================================ LOGGER  ============================================================#

    def _init_logger(self):
        self.logger = logging.getLogger('baulogger')
        if not os.path.exists(self.LOG_PATH):
            os.makedirs(self.LOG_PATH)
        self.handler = logging.FileHandler(self.LOG_FILE_PATH)
        self.formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)
        self.logger.setLevel(logging.ERROR)

    def autolog(self, message):
        """
        Automatically logs the current function details.
        source:
        http://stackoverflow.com/questions/10973362/python-logging-function-name-file-name-line-number-using-a-single-file
        """
        # Get the previous frame in the stack, otherwise it would
        # be this function!!!
        func = inspect.currentframe().f_back.f_code
        # Dump the message + the name of this function to the logs.
        self.logger.debug("%s: %s in %s:%i" % (
            message,
            func.co_name,
            func.co_filename,
            func.co_firstlineno
        ))
    # <== LOGGER #

    def _init_enums(self):
        self.DB_TYPE = Globals.enum(MYSQL=0, MSSQL=1, MONGODB=2)
        self.DATA_TYPE = Globals.enum(TWITTER_CORPUS=0, SWN=1, STOP_WORDS=2, EMOTICONS=3)
        self.TABLE = Globals.enum(TwitterCorpus=0, TwitterCorpusV2=1, ManualTwitterCorpus=2, TwitterCorpusNoEmoticons=3,
                                  FigurativeTrainingData=4)
        self.SOURCE = Globals.enum(Corpus=0, Text=1, Stream=2)
        self.CLASSIFIER_TYPE = Globals.enum(NBayes=0,
                                            SVM=1,
                                            DecisionTree=2,
                                            SVMStandalone=3,
                                            SVR=4,
                                            NuSVR=5,
                                            RandomForestRegressor=6,
                                            SGD=7,
                                            SVC=8,
                                            Perceptron=9,
                                            Ada=10,
                                            Bagging=11,
                                            SVMRbf=12)
        self.VECTORIZER = Globals.enum(Dict=0, Count=1, Idf=2, Tf=3)
        self.TAGS = Globals.enum(__CAPITAL__=0,
                                 __HT__=1,
                                 __HT_POS__=2,
                                 __HT_NEG__=3,
                                 __LINK__=4,
                                 __POS_SMILEY__=5,
                                 __NEG_SMILEY__=6,
                                 __NEGATION__=7,
                                 __REFERENCE__=8,
                                 __RT__=9,
                                 __LAUGH__=10,
                                 __LOVE__=11,
                                 __OH_SO__=12,
                                 __DONT_YOU__=13,
                                 __AS_GROUND_AS_VEHICLE__=14,
                                 __questionmark__=15,
                                 __fullstop__=16,
                                 __exclamation__=17,
                                 __punctuation_percentage__=18,
                                 __hashtag_lexicon_sum__=19)
        # Globals for Evaluation of results #
        self.METHOD_TYPE = Globals.enum(GENERAL=0, ASPECT_BASED=1, GEO_BASED=2)
        # Globals for Statistics #
        self.STATS_TYPE = Globals.enum(TEXT_AND_SCORE=0, METHOD_AND_ACCURACY=1 )
        self.CHART_TYPE = Globals.enum(BAR=0, PIE=1)
        # Globals for Aspect Analysis #
        self.WORD_TYPE = Globals.enum(NOUN=0, VERB=1, ADJECTIVE=2, ADVERB=3)
        # Globals for Threading Jobs #
        self.JOB = Globals.enum(STOP=0, START=1)
        self.DISCRETIZATION = Globals.enum(ONE=1.0, HALF=0.5, ZERO_POINT_TWO=0.2)

    def _init_db_handlers(self):
        # try:
        #     self.mysql_conn = MySQLDatabaseConnector()
        # except:
        #     self.logger.error("Could not instantiate mysql")
        #     raise Exception("Check if MySql is running!")
        #     pass
        pass

    def load_word_costs(self):
        if self.words is None:
            from math import log
            self.words = open("/Users/mariakaranasou/Projects/large-scale-sentiment-analysis/SentimentAnalysis/TweetUtils/data/words-by-frequency.txt").read().split()
            self.word_cost = dict((k, log((i+1)*log(len(self.words)))) for i, k in enumerate(self.words))
            self.max_word = max(len(x) for x in self.words)

    def stream_thread_is_running(self):
        for thread in self.THREAD_LIST:
            try:
                if thread.is_alive() and thread.name == 'StreamThread':
                    # print "Thread is alive!!!"
                    return True
            except:
                self.logger.debug("Thread {0} is not alive".format(self.THREAD_LIST.index(thread)))
                return False



################################################ ENUMS ################################################################


    @staticmethod
    def enum(*sequential, **named):
        """
        REFERENCE = http://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python
        """
        enums = dict(zip(sequential, range(len(sequential))), **named)
        reverse = dict((value, key) for key, value in enums.iteritems())
        enums['name'] = reverse
        return type('Enum', (), enums)


# Singleton-like
g = Globals()