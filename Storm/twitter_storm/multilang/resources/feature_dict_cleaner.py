import json
from TweetUtils.feature_extraction.feature_selection import FeatureSelector
import storm

__author__ = 'mariakaranasou'

# todo: store them in database when train/ classify and get them from there
# * : final combination of features
selected_features =[
                    'postags',              # * // << +
                    '__swn_score__',
                    's_word',               # * // << +
                    "pos_position_",
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                ]

selector = FeatureSelector(selected_features)


class FeatureCleanerBolt(storm.BasicBolt):
    """
    The feature cleaner bolt's job is to remove/ transform the incoming Tweet's features to what the Classification bolt
    will need.
    """

    def process(self, tup):
        """
        tup.values[0] is the serialized Tweet from the Preprocessing bolt.
        Keep the features necessary for the Classification bolt, transform them and emit the serialized Tweet
        :param tup: tuple with one Tweet
        :return: 
        """
        tweet = json.loads(tup.values[0])
        selector.update_feature_dict(tweet)
        storm.emit([json.dumps(tweet)])

        # try:
        #     storm.emit([json.dumps(tweet)])
        #     storm.ack(tup)
        # except:
        #     storm.fail(tup)


FeatureCleanerBolt().run()
