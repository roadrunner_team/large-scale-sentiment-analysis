# coding: utf-8
import json

import MySQLdb
import traceback

__author__ = 'maria'

# Establish connection to DB
# conn = pypyodbc.connect('DRIVER={MySQL ODBC 5.1 Driver};SERVER=localhost;DATABASE=SentiFeed;UID=root;PWD=291119851$Mk', ansi=True)


class MySQLDatabaseConnector(object):
    def __init__(self, cfg_path="../data/config.json"):
        with open(cfg_path) as cfg:
            config = json.loads(cfg)
            print config
        try:
            # use this if you use MySQLdb (usually in windows)
            self.conn = MySQLdb.connect(host=config["MySQL"]["host"],
                                        user=config["MySQL"]["user"],
                                        passwd=config["MySQL"]["passwd"],
                                        db=config["MySQL"]["db"])

            # use this if you use mysql.connector
            # self.conn = mysql.connector.connect(user='root',
            #                                     password='291119851$Mk',  # 291119851$Mk
            #                                     host='127.0.0.1',
            #                                     database='SentiFeed')

            self.query = ''
            self.cur = self.conn.cursor()
        except:
            traceback.print_exc()
            raise Exception("Could not connect to MySQL, check if server is running!")

    def lastrowid(self):
        return self.cur._lastrowid

    def execute_query(self, query):
        self.query = query
        self.cur.execute(query)
        rows = self.cur.fetchall()
        return rows

    def update(self, query):
        self.query = query
        self.cur.execute(self.query)
        last_row_id = self.cur.lastrowid
        self.conn.commit()
        return last_row_id

    def close_conn(self):
        self.conn.close()
