import ast
import re
import datetime
from TweetUtils.database import MySQLDatabaseConnector
from TweetUtils.helpers.static_paths import DATA_FOLDER_PATH, LOG_FOLDER_PATH
from nltk.corpus.reader.wordnet import NOUN, VERB, ADJ, ADV
import logging
import inspect
import os

__author__ = 'maria'
# pattern source 'http://stackoverflow.com/questions/3895874/regex-to-match-lol-to-lolllll-and-omg-to-omggg-etc'
# '''LAUGHING_PATTERN_lol = """(?ix)\b    # assert position at a word boundary
#     (?=l(o)*(l)*)        # assert that "lol" can be matched here
#     \S*            # match any number of characters except whitespace
#     (\S+)          # match at least one character (to be repeated later)
#     (?<=\blo)     # until we have reached exactly the position after the 1st "lol"
#     \1*            # then repeat the preceding character(s) any number of times
#     \b             # and ensure that we end up at another word boundary"""'''


class Globals(object):
    """
    General purpose class to hold global variables, such as db connection and logger instance.
    """
    def __init__(self):
        self.train_model = {}
        self.logger = None
        self.handler = None
        self.formatter = None
        self.mysql_conn = None
        self.words = None
        self.abbreviations = {}
        self.NOUNS = ['N', 'NP', 'NN', 'NNS', 'NNP', 'NNPS']
        self.VERBS = ['V', 'VD', 'VG', 'VN', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
        self.ADJECTIVES = ['ADJ', 'JJ', 'JJR', 'JJS']
        self.ADVERBS = ['RB', 'RBR', 'RBS', 'WRB']
        self.OTHERS = ['IN', 'PRP$', 'CC', "''", "USR", "UH", "TO", ',', '.', 'MD', 'DT', ')', 'WRB', 'URL', 'HT', ':',
                       "``", "CD", "RP", "RT", "EX", 'POS', 'WP', '(', 'WDT', 'FW', "$", "PDT", "SYM"]

        # self.LINK_PATTERN = r'(?P<url>https?://[^\s]+)'
        """
        ref: https://gist.github.com/uogbuji/705383
        """
        self.LINK_PATTERN = r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>' \
                            r']+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,' \
                            r'<>?\xab\xbb\u201c\u201d\u2018\u2019]))'
        self.HT_PATTERN = r'#\w+'
        self.RT_PATTERN = r'\b(^RT:{0,1}|rt:{0,1})\b'
        self.LAUGHING_PATTERN_haha = '\b(a?((h)*(a)*)*)\b'
        self.LAUGHING_PATTERN_lol= '(?ix)\b(l(o)*(l)*)\1*\b'
        self.LAUGHING_PATTERN_rotfl = '(?ix)\b(?=ro(t)*fl)'
        self.LAUGHING_PATTERN_omg = '(?ix)\b(?=o(m)*(g)*)'
        self.LAUGH_PATTERN = r'\b(a*ha+h[ha]*|o?l+o+l+[ol]*)\b'
        self.POS_SMILEY_PATTERN = r'(:-\){1,}|:-D{1,}|\(:|:\){1,}|:D|=\){1,}|;-\){1,}|XD{1,}|=D{1,}|=]|;D|:]|:o\)' \
                                  r'{1,}|<3{1,}|&lt;?3{1,})'  # todo: can be done more clearly and short
        self.NEG_SMILEY_PATTERN = r'(:-\\|:-\({1,}|:\(|:o|:O|D:|=/|=\(|:\'\(|:\'-\(|:\\|:/|:S|o.(O|o))'
        self.SMILEY_FULL_PATTERN = r'(:-\)|:-\(|:-\(\(|:-D|:-\)\)|:\)\)|\(:|:\(|:\)|:D|=\)|;-\)|XD|=D|:o|:O' \
                                   r'|=]|D:|;D|:]|=\/|=\(!:\'\(|:\'-\(|:\\|:\/|:S|<3)'
        self.NEGATIONS_PATTERN = r'\b#{0,1}(no|not|isnt|isn\'t|isn;t|don\'t|won\'t|couldn\'t|can\'t|didn\'t|' \
                                 r'not|didnt|didn;t' \
                                 r'|dont|don;t|don t|won t|can;t|cant|can t|doesn\'t|doesnt|doesn t|doesn;t|' \
                                 r'cannot|did not|not|wasn(\'|;)?t)\b'
        self.CAPITALS_PATTERN = r'\b[A-Z]{2,}\b'
        self.REFERENCE_PATTERN = r'(@\w+|@\D\w+|@\d\w+)'
        self.LOVE_PATTERN = r'(<3|&lt;?3)'

        self.NEGATIONS = ['no', 'don\'t', 'won\'t', 'couldn\'t', 'can\'t', 'didn\'t', 'not', 'could not', 'cannot']
        self._init_enums()
        self.OH_SO_PATTERN = r'(O|o)h,?\s?(so)?\s?(you)?\w+'
        self.DONT_YOU_PATTERN = r'(D|d)on\'?;?\s?t\s(you|u)'
        self.AS_GROUND_AS_VEHICLE_PATTERN = r'(A|a)s\s\w+\sas\s\w+\s?'
        self.MUST_KNOW_ABBREV = ['RT', 'MT', 'DM', 'FF', '@reply']
        self.pos_literal = {'n': NOUN,
                            'v': VERB,
                            'a': ADJ,
                            'r': ADV}
        self.ROOT_PATH = os.path.dirname(os.path.realpath(__file__))
        self.PROJECT_ROOT_PATH = str(self.ROOT_PATH).replace('helpers', '')
        self.TWITTIE_JAR_PATH = self.PROJECT_ROOT_PATH + '/data/twitie-tagger/twitie_tag.jar'
        self.TWITTIE_MODEL_PATH = self.PROJECT_ROOT_PATH + '/data/twitie-tagger/models/gate-EN-twitter.model'
        self.DATESTAMP = str(datetime.datetime.today()).replace(' ', '_').replace(':', '_')
        self.log_separator = "#################################{0}####################################################"
        self.LOG_PATH = LOG_FOLDER_PATH  # self.PROJECT_ROOT_PATH + "/logs/"
        self.LOG_FILE_PATH = self.LOG_PATH + "tweet_utils{0}.logs".format(self.DATESTAMP)
        self._init_logger()
        self._init_db_handlers()
        self._prepare_gate_pos_tags_model()
        self.load_word_costs()
        self.parse_abbreviations()

    def _prepare_gate_pos_tags_model(self):
        # stored processed model as a dict in file so to minimize load time self.PROJECT_ROOT_PATH + 
        with open(DATA_FOLDER_PATH + 'model_dict.txt', 'r') as f:
            self.train_model = ast.literal_eval(f.read())

    def parse_abbreviations(self):
        if len(self.abbreviations) == 0:
            try:
                q = """SELECT abbreviation, expansion FROM SentiFeed.Abbreviations;"""
                data = self.mysql_conn.execute_query(q)
                for each in data:
                    self.abbreviations[each[0]] = each[1]
            except:
                pass

    def load_word_costs(self):
        if self.words is None:
            from math import log
            self.words = open(DATA_FOLDER_PATH + 'words-by-frequency.txt').read().split()
            self.word_cost = dict((k, log((i+1)*log(len(self.words)))) for i, k in enumerate(self.words))
            self.max_word = max(len(x) for x in self.words)

# ================================================ LOGGER  ============================================================#
    def _init_logger(self):
        self.logger = logging.getLogger('baulogger')
        if not os.path.exists(self.LOG_PATH):
            os.makedirs(self.LOG_PATH)
        self.handler = logging.FileHandler(self.LOG_FILE_PATH)
        self.formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)
        self.logger.setLevel(logging.ERROR)

    def autolog(self, message):
        """
        Automatically logs the current function details.
        source:
        http://stackoverflow.com/questions/10973362/python-logging-function-name-file-name-line-number-using-a-single-file
        """
        # Get the previous frame in the stack, otherwise it would
        # be this function!!!
        func = inspect.currentframe().f_back.f_code
        # Dump the message + the name of this function to the logs.
        self.logger.debug("%s: %s in %s:%i" % (
            message,
            func.co_name,
            func.co_filename,
            func.co_firstlineno
        ))

    def _init_db_handlers(self):
        try:
            self.mysql_conn = MySQLDatabaseConnector()
        except:
            # raise Exception("Could not instantiate mysql") # do not raise since g is used from many class
            self.logger.error("Could not instantiate mysql")

# ================================================ ENUMS  =============================================================#

    @staticmethod
    def enum(*sequential, **named):
        """
        REFERENCE = http://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python
        """
        enums = dict(zip(sequential, range(len(sequential))), **named)
        reverse = dict((value, key) for key, value in enums.iteritems())
        enums['name'] = reverse
        return type('Enum', (), enums)

    def _init_enums(self):
        self.DB_TYPE = Globals.enum(MYSQL=0, MSSQL=1, MONGODB=2)
        self.DATA_TYPE = Globals.enum(TWITTER_CORPUS=0, SWN=1, STOP_WORDS=2, EMOTICONS=3)
        self.TABLE = Globals.enum(TwitterCorpus=0, TwitterCorpusV2=1, ManualTwitterCorpus=2, TwitterCorpusNoEmoticons=3,
                                  FigurativeTrainingData=4)
        self.SOURCE = Globals.enum(Corpus=0, Text=1, Stream=2)
        self.CLASSIFIER_TYPE = Globals.enum(NBayes=0,
                                            SVM=1,
                                            DecisionTree=2,
                                            SVMStandalone=3,
                                            SVR=4,
                                            NuSVR=5,
                                            RandomForestRegressor=6,
                                            SGD=7,
                                            SVC=8,
                                            Perceptron=9,
                                            Ada=10,
                                            Bagging=11)
        self.VECTORIZER = Globals.enum(Dict=0, Count=1, Idf=2, Tf=3)
        self.TAGS = Globals.enum(__CAPITAL__=0,
                                 __HT__=1,
                                 __HT_POS__=2,
                                 __HT_NEG__=3,
                                 __LINK__=4,
                                 __POS_SMILEY__=5,
                                 __NEG_SMILEY__=6,
                                 __NEGATION__=7,
                                 __REFERENCE__=8,
                                 __RT__=9,
                                 __LAUGH__=10,
                                 __LOVE__=11,
                                 __OH_SO__=12,
                                 __DONT_YOU__=13,
                                 __AS_GROUND_AS_VEHICLE__=14,
                                 __questionmark__=15,
                                 __fullstop__=16,
                                 __exclamation__=17,
                                 __punctuation_percentage__=18,
                                 __hashtag_lexicon_sum__=19)

        self.VALUE_TYPES = Globals.enum(Boolean=0, Text_boolean=1, Text=2, Count=3)
        # Globals for Evaluation of results #
        self.METHOD_TYPE = Globals.enum(GENERAL=0, ASPECT_BASED=1, GEO_BASED=2)
        # Globals for Statistics #
        self.STATS_TYPE = Globals.enum(TEXT_AND_SCORE=0, METHOD_AND_ACCURACY=1)
        self.CHART_TYPE = Globals.enum(BAR=0, PIE=1)
        # Globals for Aspect Analysis #
        self.WORD_TYPE = Globals.enum(NOUN=0, VERB=1, ADJECTIVE=2, ADVERB=3)
        # Globals for Threading Jobs #
        self.JOB = Globals.enum(STOP=0, START=1)
        self.DISCRETIZATION = Globals.enum(ONE=1.0, HALF=0.5, ZERO_POINT_TWO=0.2)

# ================================================ SQL  ===============================================================#

    @staticmethod
    def select_from_stop_words():
        return "SELECT StopWord FROM SentiFeed.StopWordsSample"

    @staticmethod
    def sum_query_figurative_scale_equals():
        return '''SELECT
                    CASE
                        WHEN count(SentimentAssesment) > 0
                        THEN sum(SentimentAssesment) / count(SentimentAssesment)
                        ELSE 10
                    END AS SWN
                FROM
                    SentiFeed.sentiwordnet
                where
                    SysTerms = '{0}' {1};'''

    @staticmethod
    def sum_query_figurative_scale_equals_rank_test():
        return ''' SELECT sum(SentimentAssesment),sum(rank)
                   FROM SentiFeed.sentiwordnet
                   where SysTerms = '{0}' {1}
                   GROUP BY sentiwordnet.SentiWordNetTempId;'''

    @staticmethod
    def sum_query_figurative_scale_like_rank_test():
        return ''' SELECT sum(SentimentAssesment),sum(rank)
                   FROM SentiFeed.sentiwordnet
                   where SysTerms LIKE '{0}%' {1}
                   GROUP BY sentiwordnet.SentiWordNetTempId;'''

  

    @staticmethod
    def sum_query_figurative_scale_like():
        return '''SELECT
                    CASE
                        WHEN count(SentimentAssesment) > 0
                        THEN sum(SentimentAssesment) / count(SentimentAssesment)
                        ELSE 10
                    END AS SWN
                FROM
                    SentiFeed.sentiwordnet
                where
                    SysTerms like '{0}%' {1};'''

    @staticmethod
    def sum_query_figurative_scale2_equals():
        return '''SELECT CASE
                            WHEN count(ConvertedScale)>0
                            THEN sum(ConvertedScale)/count(ConvertedScale)
                            ELSE 10
                            END AS SWN
                    FROM SentiFeed.sentiwordnet
                    where SysTerms = "{0}" {1}'''

    @staticmethod
    def sum_query_figurative_scale2_like():
        return '''SELECT CASE
                            WHEN count(ConvertedScale)>0
                            THEN sum(ConvertedScale)/count(ConvertedScale)
                            ELSE 10
                            END AS SWN
                    FROM SentiFeed.sentiwordnet
                    where SysTerms like "{0}%" {1}'''

    # For Updating Corpus #
    @staticmethod
    def insert_in_tweet_bo():
        return '''
                INSERT INTO SentiFeed.TweetBO4000Test
                (id, text, initial_score, clean_text, tagged_text, pos_tagged_text, tags,sentences, words, links, corrected_words,
                smileys_per_sentence, uppercase_words_per_sentence, reference, hash_list, non_word_chars_removed, negations,
                swn_score_dict, swn_score, feature_dict, score, train)
                VALUES
                ("{0}","{1}","{2}","{3}","{4}","{5}","{6}","{7}","{8}","{9}","{10}", "{11}","{12}",
                "{13}","{14}","{15}","{16}", "{17}","{18}","{19}","{20}", "{21}");
              '''

    @staticmethod
    def update_tweet_bo():
        return '''
                UPDATE SentiFeed.{23}
                SET
                    clean_text = "{0}",
                    tagged_text = "{1}",
                    pos_tagged_text = "{2}",
                    tags = "{3}",
                    initial_score = "{4}",
                    sentences = "{5}",
                    words = "{6}",
                    links = "{7}",
                    corrected_words = "{8}",
                    smileys_per_sentence = "{9}",
                    uppercase_words_per_sentence = "{10}",
                    reference = "{11}",
                    hash_list = "{12}",
                    non_word_chars_removed = "{13}",
                    negations = "{14}",
                    swn_score_dict = "{15}",
                    swn_score = "{16}",
                    feature_dict = "{17}",
                    score = "{18}",
                    train = {19},
                    modified = "{20}",
                    words_to_swn_score_dict ="{21}"
                WHERE id = "{22}";
                '''

    # For Abbreviations   #
    @staticmethod
    def insert_into_slang_translation():
        return '''
                INSERT INTO SentiFeed.SlangTranslation
                (SlangTerm, SlangTranslation, SlangMeaning, SlangScore, Source)
                VALUES
                ("{SlangTerm}", "{SlangTranslation}", "{SlangMeaning}", "{SlangScore}", "{Source}");
                '''

    @staticmethod
    def check_if_term_exists():
        return '''SELECT * FROM SentiFeed.SlangTranslation where SlangTerm = "{0}";'''

# Singleton-likes
g = Globals()

