import json
import traceback
from TweetUtils.models.config import UtilsConfig
from TweetUtils.models.config import ClsConfig
from TweetUtils.models.classifier import Classifier
from TweetUtils.tweet_utils import TweetUtils
from TweetUtils.helpers.globals import g
import storm

__author__ = 'mariakaranasou'


def get_cls_metaphor():
    """
    Loads the pre-trained classifier model - if there is none, it gathers the data, trains the cls, saves the model
    and returns a Classifier instance
    :return: 
    """
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                               max_vote=False,
                               select_percent=0,
                               use_idf=False,
                               use_hashing=False)
    metaphor_cls = Classifier(cls_config)

    try:
        metaphor_cls.load_model("Metaphor")
    except:
        q_metaphor = "SELECT tweet_text FROM SentiFeed.Metaphors;"
        q_others_positive = "SELECT text FROM SentiFeed.tweet_total where source='1.6M' and initial_score > 0  limit 3225 OFFSET 30000;"
        q_others_negative = "SELECT text FROM SentiFeed.tweet_total where source='1.6M' and initial_score < 0  limit 3225 OFFSET 30000;"

        met_data = [x[0] for x in g.mysql_conn.execute_query(q_metaphor)]
        oth_data_positive = [x[0] for x in g.mysql_conn.execute_query(q_others_positive)]
        oth_data_negative = [x[0] for x in g.mysql_conn.execute_query(q_others_negative)]

        t_met_data = [[x, True] for x in met_data]
        t_oth_pos_data = [[x, False] for x in oth_data_positive]
        t_oth_neg_data = [[x, False] for x in oth_data_negative]

        final_data = t_met_data + t_oth_neg_data + t_oth_pos_data

        metaphor_cls .set_y_train([x[1] for x in final_data])
        metaphor_cls .set_x_train([x[0] for x in final_data])
        metaphor_cls .train()
        metaphor_cls.save_model("Metaphor")
        pass
    return metaphor_cls


class PreprocessingBolt(storm.BasicBolt):
    """
    This bolt is responsible for the preprocessing of each incoming tweet.
    When the bolt first loads, it initializes the metaphor classifier - pretrained model
    and the TweetUtils library.
    On process, it cleans the tweet, serializes the resulting Tweet model and emits it
    """
    def __init__(self):
        self.metaphor_cls = get_cls_metaphor()
        self.utils = TweetUtils(UtilsConfig(), self.metaphor_cls)

    def process(self, tup):
        stream_tweet = json.loads(tup.values[0])
        tweet = self.utils.process(str(stream_tweet["text"].encode("utf-8")))
        tweet.original_tweet = stream_tweet
        tweet.id = tweet.original_tweet["id"]
        storm.emit([json.dumps(tweet.serialize())])
        # storm.emit([stream_tweet])

PreprocessingBolt().run()
