import storm
import json
import re

class SplitSentenceBolt(storm.BasicBolt):
    def process(self, tup):
        # TO DO: Add check for empty values
        from TweetUtils.models.config import Config
        from TweetUtils.models.tweet import Tweet
        from TweetUtils.tweet_utils import TweetUtils
        from TweetUtils.helpers.globals import g
        stream_tweet = tup.values[0]
        words = tup.values[0].split(" ")
        for word in words:
            storm.emit([word])
         
SplitSentenceBolt().run() 


