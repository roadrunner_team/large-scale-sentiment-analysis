import copy
import json
import traceback
from TweetUtils.models.config import ClsConfig
from TweetUtils.models.classifier import Classifier
from TweetUtils.helpers.globals import g

import storm

__author__ = 'mariakaranasou'


# Set up and load the pretrained models for each classifier

# All Features classifier
cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                       max_vote=False,
                       select_percent=60,
                       use_idf=False,
                       use_tf=True,
                       use_hashing=False)
cls = Classifier(cls_config)
cls.load_model("ALL_FEATURES")

# All features but emoticons classifier
cls_config_no_emoticons = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                       max_vote=False,
                       select_percent=60,
                       use_idf=False,
                       use_tf=True,
                       use_hashing=False)
cls_no_emoticons = Classifier(cls_config)
cls_no_emoticons.load_model("NO_EMOTICONS")

# All features but links / urls classifier
cls_config_no_links = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                       max_vote=False,
                       select_percent=60,
                       use_idf=False,
                       use_tf=True,
                       use_hashing=False)
cls_no_links = Classifier(cls_config)
cls_no_links.load_model("NO_LINKS")

# All features - but the classifier is trained only on the hand-labeled dataset,
# without the 1.6M emoticon-harvested dataset
cls_config_no_1_6_m = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                       max_vote=False,
                       select_percent=60,
                       use_idf=False,
                       use_tf=True,
                       use_hashing=False)
cls_no_1_6_m = Classifier(cls_config)
cls_no_1_6_m.load_model("NO_1_6M")

import redis
CHANNEL = 'live'

def send_message(message):
   client = redis.StrictRedis()
   message = message.encode('utf8')
   g.logger("sending msg" + message)
   return client.publish(CHANNEL, message)


# The following functions modify the feature dict to fit each classifier's model.

def remove_emoticons(feature_dict):
    copied_dict = copy.copy(feature_dict)
    del copied_dict["__POS_SMILEY__"]
    del copied_dict["__NEG_SMILEY__"]
    return copied_dict


def remove_links(feature_dict):
    copied_dict = copy.copy(feature_dict)
    g.logger.error(copied_dict)
    del copied_dict["__LINK__"]
    return copied_dict


class ClassificationBolt(storm.BasicBolt):
    """
        Loads the pre-trained classifier models on init and for each classifier, 
        a prediction is made and stored in the Tweet structure.
        The whole Tweet structure along with the predicted scores is emitted to the next bolt.
    """
    def process(self, tup):
        global cls
        try:
            # get tweet
            stream_tweet = json.loads(tup.values[0])

            # vectorize feature dict - modified accordingly for different cls models
            cls.set_x_trial([stream_tweet["feature_dict"]])
            cls_no_emoticons.set_x_trial([remove_emoticons(stream_tweet["feature_dict"])])
            cls_no_links.set_x_trial([remove_links(stream_tweet["feature_dict"])])
            cls_no_1_6_m.set_x_trial([stream_tweet["feature_dict"]])

            # save different predictions in different fields
            stream_tweet["predicted_score"] = cls.predict()[0]
            stream_tweet["predicted_score_no_emoticons"] = cls_no_emoticons.predict()[0]
            stream_tweet["predicted_score_no_links"] = cls_no_links.predict()[0]
            stream_tweet["predicted_score_all_features_no_1_6m"] = cls_no_1_6_m.predict()[0]
            pos_neg_neu = "positive" if stream_tweet["predicted_score"] > 0 \
                else "negative" if stream_tweet["predicted_score"] < 0 \
                else "neutral"
            send_message(json.dumps({
                "id": stream_tweet["id"],
                "text": stream_tweet["text"],
                "prediction": pos_neg_neu,
                "type": "tweet"
            }))
            storm.emit([stream_tweet])
        except:
            g.logger.error("ERROR!! " + traceback.format_exc())


ClassificationBolt().run()
