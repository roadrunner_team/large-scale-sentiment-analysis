/**
 * Created by mariakaranasou on 9/27/15.
 */

$(document).ready(function(){
    //$('.btn').button();
   listen();
});

var btn_correct = "<button class='btn btn-success' title='Prediction is correct' onclick='feedback(this)'><i class='fa fa-check'></i></button>";
var btn_error = "<button class='btn btn-danger' title='Prediction is not correct' onclick='feedback(this)'><i class='fa fa-close'></i></button>";

function listen(){
    $.ajax({url: "../dashboard/long_polling_endpoint/",
        success: function(result){
            try{
                 var jsonRes = JSON.parse(result);
                if(jsonRes.type==="tweet"){
                    var cls = "";
                    if(jsonRes.prediction == "positive"){
                        cls = 'btn-success';
                    }else if(jsonRes.prediction == "negative"){
                        cls = "btn-danger";
                    }else{
                        cls = "btn-info"
                    }
                    //$("<li class='"+ cls +"'>"+btn_correct + btn_error +"<span> "  +result+"</span></li>").hide().prependTo('#notifications').slideDown();
                    $("<li id='"+jsonRes.id+"' class='"+ cls +"'>"+btn_correct + btn_error +"<span> "  +jsonRes.text+"</span></li>").prependTo('#notifications');
                }
                else{
                    $("<li>"+btn_correct + btn_error +"<span> " +result+"</span></li>").prependTo('#notifications');
                }
            }
            catch(ex){
                $("<li class='btn-default'>"+btn_correct + btn_error +"<span> " +result+"</span></li>").prependTo('#notifications');
            }


            listen();
    },
    error: function(xhr,status,error){
       showalert(error, "danger");
        console.log("ERROR:", xhr.responseText)
    }
    });
}

function feedback(el){
    var $el = $(el);
    if($el.hasClass('btn-success')){
        $el.closest('li').slideUp();

    }else{

    }

}

function sendMessage(){
    var msg ={"message": $("#message").val()};
    var csrftoken = getCookie('csrftoken');

    $.ajax({url: "../dashboard/send_endpoint/",
        data: msg,
        method: "POST",
        "beforeSend": function(xhr, settings) {
            $.ajaxSettings.beforeSend(xhr, settings);
        },
        success: function(result){
            console.log(result.message);
    },
    error: function(xhr,status,error){
       showalert(error, "danger");
        console.log("ERROR:", xhr.responseText)
    }
    });
}