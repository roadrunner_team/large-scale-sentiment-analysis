/**
 * Created by maria on 4/18/15.
 * ajax POST calls
 * http://coreymaynard.com/blog/performing-ajax-post-requests-in-django/
 *
 */

$(document).ready(function(){
});

function listen(){
    $.ajax({url: "../dashboard/long_polling_endpoint/",
        success: function(result){

            $("<li><span class='fa fa-terminal fa-2x' style='color:cyan;'> " +result+"</span></li>").appendTo('#feedback_console');
            listen();
    },
    error: function(xhr,status,error){
       showalert(error, "danger");
        console.log("ERROR:", xhr.responseText)
    }
    });
}

function startTrial(btn){
    var options;
    var $btn = $(btn);
    var csrftoken = getCookie('csrftoken');
    var classifier = $( "#cls option:selected" ).text();
    var discretization = $( "#discr option:selected" ).text();
    var sel_fea = $( "#fea option:selected");
    var sel_morph = $( "#morph option:selected");
    var sel_fig = $( "#fig option:selected");
    var sel_sim = $( "#sim option:selected");
    var sel_corpus = $( "#corpus option:selected").text();
    var selected_features = [];
    var spinner = '<div class="spinner_container" id="spinner_box"><i class="fa fa-terminal fa-2x">Processing...</i>' +
                    '<i class="fa fa-refresh fa-2x fa-spin spinner"></i><ul id="feedback_console" class="feedback_console"></ul></div>';

    $('body').prepend(spinner);

    disableEnableMenu();

    listen();

    $.each(sel_fea, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_morph, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_fig, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_sim, function(k, v){
       selected_features.push($(v).text());
    });

    options = {
        "classifier": classifier,
        "discretization": discretization,
        "selected_features": selected_features,
        "corpus": sel_corpus
    };

    var data = JSON.stringify(options);

    $.ajax({url: "../dashboard/start_trial",
        method: "POST",
        async: true,
        data: options,
        "beforeSend": function(xhr, settings) {
            $.ajaxSettings.beforeSend(xhr, settings);
        },
        success: function(result){
            disableEnableMenu();
            $('#trial_results').html(result);
            $("#spinner_box").remove();
            //cummulativeLineChart(trial_results_agree, "chart_agree");
            //cummulativeLineChart(trial_results_disagree, "chart_disagree");
            //polarityBarChart(trial_results_agree.length, trial_results_disagree.length);
    },
    error: function(xhr,status,error){
        showalert(error, "danger");
        $("#spinner_box").remove();
        enableMenu();
    }
    });

}

function cummulativeLineChart(data, containerId){
    var y_axis = ['y', -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5];
    var data_len = ['x'];
    var data_agree_initial = ['initial'];
    var data_agree_predicted = ['predicted'];
    data_len[0] = "x";

    for(var i=1; i <= data.length; i++){
        data_len.push(i);
    }

    $.each(data, function(k,v){
        data_agree_initial.push(parseInt(v[1]));
        data_agree_predicted.push(parseInt(v[2]));
    });

    var chart_agree = c3.generate({
        bindto: "#"+containerId,
        data: {
            x: 'x',
    //        xFormat: '%Y%m%d', // 'xFormat' can be used as custom format of 'x'
            columns: [
                data_len,
                data_agree_initial,
                data_agree_predicted
            ]
        },
        type:'area',
        //types: {
        //    initial: 'spline',
        //    predicted: 'area'
        //},
        axis: {
            x: {
                //type: 'area',

                tick: {
                    fit:false
                    //format: '%Y-%m-%d'
                }
            },
            y: {
                max: 5,
                min: -5,
                label: { // ADD
                    text: 'Score',
                    position: 'outer-middle'
                }
            }
        }
    });

}

function polarityBarChart(len_agree, len_disagree){

    var chart = c3.generate({
    data: {
        bindto:'#chart',
        // iris data from R
        columns: [
            ['agree on polarity', len_agree],
            ['disagree on polarity', len_disagree],
        ],
        type : 'pie',
        onclick: function (d, i) { console.log("onclick", d, i); },
        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
    }
});
}