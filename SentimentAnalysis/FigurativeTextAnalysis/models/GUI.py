__author__ = 'mariakaranasou'



from Tkinter import *
from ttk import *

root = Tk()

root.title("Classification Trials")
root.geometry("500x500")
# app = Frame(root)
# app.grid()
# label = Label(app, text="Test Label")
# label.grid()
# btn = Button(app, text="Run")
# btn.grid()
# root.mainloop()

class Application(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.grid()
        self.create_widgets()

    def create_widgets(self):
        self.btn_run = Button(self, text="Run Trial")
        self.label_info = Label(self, text="Run Trial")

        self.btn_run["command"] = self.run_trial

        self.btn_run.grid()
        self.label_info.grid()

    def run_trial(self):
        self.label_info["text"] = "Trial was run!"

app = Application(root)
root.mainloop()