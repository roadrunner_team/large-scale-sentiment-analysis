from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from SentimentAnalysis.FigurativeTextAnalysis.models.Config import DatasetConfig, ClsConfig, ProcessConfig, TrialConfig
from SentimentAnalysis.FigurativeTextAnalysis.models.DatasetHandler import DatasetHandler
from SentimentAnalysis.FigurativeTextAnalysis.models.Trial_ import Trial

__author__ = 'mariakaranasou'

selected_features_all =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_no_emoticons =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    # '__POS_SMILEY__',           # * // << +
                    # '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_only_position_with_emoticons =[
                    # 'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_only_position_no_emoticons =[
                    # 'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    # '__POS_SMILEY__',           # * // << +
                    # '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]


def task_11_all():
    ds_config = DatasetConfig()
    ds_config.task11 = 15000
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all task_11 Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons task_11 Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons task_11")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons task_11 Dict")

    task11_max_vote(ds)
    task11_different_cls(ds)
    cross_validate("task_11", ds)
    ds = []


def task11_max_vote(ds):

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all max_vote=True task_11 Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=True task_11 Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons max_vote=True task_11 Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons max_vote=True task_11 Dict")


def task11_different_cls(ds):

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all NBayes task_11 Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.Perceptron, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons Perceptron task_11 Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons DecisionTree task_11 Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons SGD task_11 Dict")


def task2b_all():
    ds_config = DatasetConfig()
    ds_config.task2b = 15000
    ds_config.task2bdev = 15000
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all task2b Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons task2b Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons task2b Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons task2b Dict")

    task2b_max_vote(ds)
    task2b_different_cls(ds)
    cross_validate("task2b", ds)
    ds = []


def task2b_max_vote(ds):

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all max_vote=True task2b Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=True task2b Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons max_vote=True task2b Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons max_vote=True task2b Dict")


def task2b_different_cls(ds):

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all NBayes task2b Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.Perceptron, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons Perceptron task2b Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons DecisionTree task2b Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons SGD task2b Dict")


def one_point_six_all():
    ds_config = DatasetConfig()
    ds_config.one_point_six_m = 30000
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all one_point_six Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons one_point_six Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons one_point_six Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons one_point_six Dict")

    one_point_six_max_vote(ds)
    one_point_six_different_cls(ds)
    cross_validate("one_point_six", ds)
    ds = []


def one_point_six_max_vote(ds):

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all max_vote=True one_point_six Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=True one_point_six Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons max_vote=True one_point_six Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons max_vote=True one_point_six Dict")


def one_point_six_different_cls(ds):

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all NBayes one_point_six Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.Perceptron, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons Perceptron one_point_six Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons DecisionTree one_point_six Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons SGD one_point_six Dict")


def all_50k():
    ds_config = DatasetConfig()
    ds_config.set_50K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 50K Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 50K Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 50K Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 50K Dict")
    ds = []


def all_50k_different_cls_nbayes(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 50K NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 50K NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 50K NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 50K NBayes Dict")


def all_50k_different_cls_sgd(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 50K SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 50K SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 50K SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 50K SGD Dict")


def all_50k_different_cls_decision_tree(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 50K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 50K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 50K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 50K DecisionTree Dict")


def all_70k():
    ds_config = DatasetConfig()
    ds_config.set_70K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 70K Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 70K Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 70K Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 70K Dict")

    all_70k_different_cls_nbayes(ds)
    all_70k_different_cls_sgd(ds)
    all_70k_different_cls_decision_tree(ds)
    cross_validate("all_70k", ds)
    ds = []


def all_70k_different_cls_nbayes(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 50K NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 50K NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 50K NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 50K NBayes Dict")


def all_70k_different_cls_sgd(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 70K SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 70K SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 70K SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 70K SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 70K SGD Dict")
    #
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Count,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    # trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # trial.start()
    # trial.get_stats()
    # trial.save_results("selected_features_no_emoticons 50K SGD Count")
    #
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Count,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    # trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # trial.start()
    # trial.get_stats()
    # trial.save_results("selected_features_only_position_no_emoticons 50K SGD Count")
    #
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Count,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    # trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # trial.start()
    # trial.get_stats()
    # trial.save_results("selected_features_only_position_with_emoticons 50K SGD Count")


def all_70k_different_cls_decision_tree(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 70K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 70K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 70K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 70K DecisionTree Dict")


def all_90k():
    ds_config = DatasetConfig()
    ds_config.set_90K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 90K Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 90K Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 90K Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 90K Dict")

    all_90k_different_cls_nbayes(ds)
    all_90k_different_cls_sgd(ds)
    all_90k_different_cls_decision_tree(ds)
    cross_validate("all_90k", ds)
    ds = []


def all_90k_different_cls_nbayes(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 90K NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 90K NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 90K NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 90K NBayes Dict")


def all_90k_different_cls_sgd(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 90K SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 90K SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 90K SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 90K SGD Dict")


def all_90k_different_cls_decision_tree(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 90K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 90K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 90K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 90K DecisionTree Dict")


def all_110k():
    ds_config = DatasetConfig()
    ds_config.set_110K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 110k Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 110k Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 110k Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 110k Dict")


    all_110k_different_cls_nbayes(ds)
    all_110k_different_cls_sgd(ds)
    all_110k_different_cls_decision_tree(ds)
    cross_validate("all_110k", ds)
    ds = []


def all_110k_different_cls_nbayes(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 110k NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 110k NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 110k NBayes Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 110k NBayes Dict")


def all_110k_different_cls_sgd(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 110k SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 110k SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 110k SGD Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 110k SGD Dict")


def all_110k_different_cls_decision_tree(ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 110K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 110K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 110K DecisionTree Dict")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 110K DecisionTree Dict")


def cross_validate(title, ds):
    ds.get_clean_total_data_list()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(ds.total_data, ds.total_scores)
    trial.save_cross_validate(title + " SVMStandalone Dict", scores)
    print title + " SVMStandalone Dict", scores

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(ds.total_data, ds.total_scores)
    trial.save_cross_validate(title + " NBayes Dict", scores)
    print title + " NBayes Dict", scores

    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Count,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    # trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # scores = trial.tweet_processor.classifier.predict_cross_validate(ds.total_data, ds.total_scores)
    # trial.save_cross_validate(title + " NBayes Count", scores)
    # print title + " NBayes Count", scores
    #
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    # trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # scores = trial.tweet_processor.classifier.predict_cross_validate(ds.total_data, ds.total_scores)
    # trial.save_cross_validate(title + " DecisionTree Dict", scores)
    # print title + " DecisionTree Dict", scores

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(ds.total_data, ds.total_scores)
    trial.save_cross_validate(title + " SGD Dict", scores)
    print title + " SGD Dict", scores


# =========================== TEXT ================================== #

def cross_validate_text(title, ds):
    ds.get_clean_total_data_list()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(ds.total_data, ds.total_scores)
    trial.save_cross_validate(title + " SVMStandalone Count", scores)
    print title + " SVMStandalone Count", scores

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(ds.total_data, ds.total_scores)
    trial.save_cross_validate(title + " NBayes Count", scores)
    print title + " NBayes Count", scores

    #
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Count,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    # trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # scores = trial.tweet_processor.classifier.predict_cross_validate(ds.total_data, ds.total_scores)
    # trial.save_cross_validate(title + " DecisionTree Count", scores)
    # print title + " DecisionTree Dict", scores

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(ds.total_data, ds.total_scores)
    trial.save_cross_validate(title + " SGD Count", scores)
    print title + " SGD Count", scores


def all_task11_text():
    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_task11()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all task11 SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons task11 SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons task11 SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons task11 SVMStandalone Count")

    cross_validate_text("task11 text", ds)

    ds = []


def all_text_bayes():
    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_task11()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all task11 NBayes Count")
    cross_validate_text("task11 text", ds)

    ds = []
    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_task2b()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all task2b NBayes Count")

    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_50K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 50K NBayes Count")

    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_70K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 70k NBayes Count")

    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_90K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 90K NBayes Count")

    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_110K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 110K NBayes Count")



def all_task2b_text():
    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_task2b()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all task2b SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons task2b SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons task2b SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons task2b SVMStandalone Count")
    cross_validate_text("task2b text", ds)
    ds = []


def all_one_point_six_text():
    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_one_point_six()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all one_point_six SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons one_point_six SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons one_point_six SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons one_point_six SVMStandalone Count")

    cross_validate_text("all_one_point_six text", ds)
    ds = []


def all_50k_text():
    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_50K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 50K SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 50K SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 50K SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 50K SVMStandalone Count")

    cross_validate_text("all_50k text", ds)
    ds = []


def all_70k_text():
    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_70K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 70k SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 70k SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 70k SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 70k SVMStandalone Count")

    cross_validate_text("all_70k text", ds)
    ds = []


def all_90k_text():
    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_90K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 90K SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 90K SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 90K SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 90K SVMStandalone Count")

    cross_validate_text("all_90k text", ds)
    ds = []


def all_110k_text():
    ds_config = DatasetConfig(dict_or_text="text")
    ds_config.set_110K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all 110K SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons 110K SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 110K SVMStandalone Count")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons 110K SVMStandalone Count")

    cross_validate_text("all_110K text", ds)
    ds = []


def test_problematic():
    ds_config = DatasetConfig()
    ds_config.set_50K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons 50K SVMStandalone Count")


def test_manual():
    ds_config = DatasetConfig()
    ds_config.set_manual()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all Manual SVMStandalone Dict 2nd")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all Manual max_vote=True Dict")


    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_all Manual NBayes Dict")


if __name__ == '__main__':
    # task_11_all()
    # task2b_all()
    # one_point_six_all()
    # all_50k()
    # all_70k()
    # all_90k()
    # all_110k()

    # all_task11_text()
    # all_task2b_text()
    # all_one_point_six_text()

    # all_50k_text()
    # all_70k_text()
    # all_90k_text()
    # all_110k_text()
    # all_text_bayes()
    test_manual()