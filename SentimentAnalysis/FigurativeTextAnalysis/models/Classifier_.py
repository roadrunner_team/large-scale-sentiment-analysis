import copy
import json
import string
import traceback
import nltk
from numpy.ma import transpose
import sklearn
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer, HashingVectorizer
from sklearn.feature_selection import VarianceThreshold, chi2, f_classif
from sklearn.feature_selection import SelectKBest
from sklearn.linear_model import SGDClassifier, Perceptron
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.tree import DecisionTreeClassifier
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from sklearn.ensemble import RandomForestRegressor, AdaBoostClassifier, BaggingClassifier
# from sklearn.metrics import classification_report
from sklearn.externals import joblib
from sklearn import cross_validation
from nltk.probability import FreqDist
from sklearn.feature_selection import SelectPercentile

# pairwise cosine= http://stackoverflow.com/questions/12118720/python-tf-idf-cosine-to-find-document-similarity

__author__ = 'maria'


class Classifier(object):
    """
        Wrapper class for scikit-learn classifiers.
    """
    def __init__(self, cls_config):
        self.cls_config = cls_config
        if self.cls_config is None:
            raise Exception("No classification configuration found!")
        self.estimator = sklearn.svm.LinearSVC()
        self.cls_type = self.cls_config.cls_type
        self.cls = self._set_up_classifier(self.cls_type)
        self.vectorizer_type = self.cls_config.vec_type
        self.vec = self._set_up_vectorizer(self.vectorizer_type)
        self.use_pairwise_cosine = self.cls_config.use_cosine
        self.use_tf_transformer = self.cls_config.use_tf
        self.tfidf_transformer = self._set_up_additional_vectorizer()  # TfidfTransformer(use_idf=self.cls_config.use_idf)
        self.hash_vectorizer = self._set_up_additional_vectorizer()
        self.sklearn_precision_score = ""
        self.sel = self._set_up_additional_vectorizer()
        self.k_best = self._set_up_additional_vectorizer()
        self.use_k_best = self.cls_config.use_k_best
        self.min_max_scaler = MinMaxScaler()
        self.standard_scaler = StandardScaler()
        self.selector = self._set_up_additional_vectorizer()
        self.do_not_have_coefs = [g.CLASSIFIER_TYPE.DecisionTree, g.CLASSIFIER_TYPE.RandomForestRegressor,
                                  g.CLASSIFIER_TYPE.SVR, g.CLASSIFIER_TYPE.SVC, g.CLASSIFIER_TYPE.Ada,
                                  g.CLASSIFIER_TYPE.Bagging]
        self.important_features = []
        self.x_train = None
        self.y = None
        self.x_trial = None
        self.y_trial = None
        self.predictions = []
        self.x_train_copy = None
        self.last_vec = None

    def __str__(self):
        str_self = {
            "cls_config": str(self.cls_config),
            # "feature_names": str(self.vec.feature_names_),
            "important_features": str(self.important_features).replace("'", "\\\"")
        }

        return str(json.dumps(str_self))

    def _set_up_classifier(self, cls_type):
        if cls_type == g.CLASSIFIER_TYPE.NBayes:
            return MultinomialNB()
        elif cls_type == g.CLASSIFIER_TYPE.SVM:
            return OneVsRestClassifier(estimator=sklearn.svm.LinearSVC())
        elif cls_type == g.CLASSIFIER_TYPE.DecisionTree:
            return DecisionTreeClassifier()
        elif cls_type == g.CLASSIFIER_TYPE.SVMStandalone:
            return sklearn.svm.LinearSVC(C=1)  # , penalty="l1", dual=False
        elif cls_type == g.CLASSIFIER_TYPE.SVC:
            # Implementation of Support Vector Machine classifier using libsvm:
            # the kernel can be non-linear but its SMO algorithm does not scale
            # to large number of samples as LinearSVC does. Furthermore SVC multi-class mode
            #  is implemented using one vs one scheme while LinearSVC uses one vs the rest.
            # It is possible to implement one vs the rest with SVC by using the
            # sklearn.multiclass.OneVsRestClassifier wrapper. Finally SVC can fit dense data without memory copy
            # if the input is C-contiguous. Sparse data will still incur memory copy though.
            return sklearn.svm.SVC(kernel='linear',C=1, gamma=0.001)
        elif cls_type == g.CLASSIFIER_TYPE.SVMRbf:
            return sklearn.svm.SVC(kernel="rbf", C=1, gamma=0.0001)
        elif cls_type == g.CLASSIFIER_TYPE.SVR:
            return sklearn.svm.SVR(kernel='linear', max_iter=150)  # >> 150
        elif cls_type == g.CLASSIFIER_TYPE.NuSVR:
            return sklearn.svm.NuSVR(kernel='linear', max_iter=10)
        elif cls_type == g.CLASSIFIER_TYPE.RandomForestRegressor:
            return RandomForestRegressor(n_estimators=2, max_depth=50)
        elif cls_type == g.CLASSIFIER_TYPE.SGD:
            # loss = Defaults to 'hinge', which gives a linear SVM.
            # The 'log' loss gives logistic regression, a probabilistic classifier.
            return SGDClassifier(loss='log', n_iter=25)
        elif cls_type == g.CLASSIFIER_TYPE.Perceptron:
            return Perceptron(penalty="l2", n_iter=150, n_jobs=1)
        elif cls_type == g.CLASSIFIER_TYPE.Ada:
            return AdaBoostClassifier(base_estimator=SGDClassifier(loss='log'), n_estimators=50)
        elif cls_type == g.CLASSIFIER_TYPE.Bagging:
            return BaggingClassifier(base_estimator=SGDClassifier(loss='hinge'), n_estimators=50)

        else:
            raise NotImplementedError("The type of classifier: {0} is not implemented".format(cls_type))

    def _set_up_vectorizer(self, vectorizer_type):
        if vectorizer_type == g.VECTORIZER.Dict:
            return DictVectorizer(sparse=True)
        elif vectorizer_type == g.VECTORIZER.Count:
            return CountVectorizer(analyzer='word',
                                   input='content',
                                   lowercase=True,
                                   min_df=1,
                                   binary=False,
                                   stop_words='english')
        elif vectorizer_type == g.VECTORIZER.Idf:
            return TfidfVectorizer(use_idf=True, stop_words='english')
        elif vectorizer_type == g.VECTORIZER.Tf:
            return TfidfVectorizer(use_idf=False, stop_words='english')
        else:
            raise NotImplementedError

    def _set_up_additional_vectorizer(self):
        if self.cls_config.use_k_best>0:
            return SelectKBest(f_classif, k=self.cls_config.use_k_best)
        elif self.cls_config.use_tf:
            return TfidfTransformer(use_idf=self.cls_config.use_idf)
        elif self.cls_config.select_percent>0:
            return SelectPercentile(f_classif, percentile=self.cls_config.select_percent)
        elif self.cls_config.use_hashing:  # http://scikit-learn.org/stable/auto_examples/text/document_clustering.html
            if self.cls_config.use_idf:
                 # Perform an IDF normalization on the output of HashingVectorizer
                hasher = HashingVectorizer(n_features=10000,
                                           stop_words='english', non_negative=True,
                                           norm=None, binary=False)
                return make_pipeline(hasher, TfidfTransformer())
            else:
                return HashingVectorizer(n_features=10000,  # default
                                         stop_words='english',
                                         non_negative=False,
                                         norm='l2',
                                         binary=False)
        elif self.cls_config.variance_threshold > 0:
            return VarianceThreshold(threshold=(.9 * (1 - .9)))

        else:
            return None

    def set_x_train(self, train_list_of_dicts):
        if type(self.vec) == CountVectorizer:
            self.x_train = self.vec.fit_transform([self.remove_non_ascii(a) for a in train_list_of_dicts])
        else:
            self.x_train = self.vec.fit_transform(train_list_of_dicts)

        g.logger.debug(train_list_of_dicts[0])
        g.logger.debug(self.x_train[0:1])

        if self.use_tf_transformer:
            self.x_train = self.tfidf_transformer.fit_transform(self.x_train)

        if self.use_k_best:
            self.x_train = self.k_best.fit_transform(self.x_train, self.y)
            self.last_vec = self.k_best

        if self.use_pairwise_cosine:
            self.x_train_copy = copy.copy(self.x_train)
            self.x_train = sklearn.metrics.pairwise.pairwise_distances(self.x_train[0:1], self.x_train, metric='cosine', n_jobs=1)

        if self.cls_config.select_percent> 0:
            self.x_train = self.selector.fit_transform(self.x_train, self.y)

        if self.cls_config.use_hashing:
            self.x_train = self.hash_vectorizer.fit_transform(self.x_train)
        # self.Xtrain = euclidean_distances(self.Xtrain)

        if not type(self.vec) is CountVectorizer and not type(self.vec) is TfidfVectorizer:
            g.logger.debug("feature_names: %s" % self.vec.feature_names_)
            g.logger.debug("vocabulary_: %s" % self.vec.vocabulary_)
        # g.logger.debug("Xtrain: %s" % self.x_train)

    def set_y_train(self, handcoded_score_list):
        self.y = handcoded_score_list
        g.logger.debug("Y:: %s" % self.y)

    def train(self):
        if self.use_pairwise_cosine:
            self.cls.fit(transpose(self.x_train), self.y)    # transpose is used when cosine similarities are calculated
        else:
            self.cls.fit(self.x_train, self.y)

    def set_x_trial(self, trial_list_of_dicts):
        if type(self.vec) == CountVectorizer:
            self.x_trial = self.vec.transform([self.remove_non_ascii(a) for a in trial_list_of_dicts])  # [str(x) for x in trial_list_of_dicts]
        else:
            self.x_trial = self.vec.transform(trial_list_of_dicts)

        self.last_vec  = self.vec

        if self.use_tf_transformer:
            self.x_trial = self.tfidf_transformer.transform(self.x_trial)
            self.last_vec = self.tfidf_transformer

        if self.use_k_best:
            self.x_trial = self.k_best.transform(self.x_trial)

        if self.use_pairwise_cosine:
            self.x_trial = sklearn.metrics.pairwise.pairwise_distances(self.x_train_copy[0:1],
                                                                      self.x_trial,
                                                                      metric='cosine',
                                                                      n_jobs=-1)
        if self.cls_config.select_percent> 0:
            self.x_trial = self.selector.transform(self.x_trial)
            self.last_vec = self.selector

        if self.cls_config.use_hashing:
            self.x_trial = self.hash_vectorizer.transform(self.x_trial)
            self.last_vec = self.hash_vectorizer

        # self.Xtrial = euclidean_distances(self.XtrainCopy)
        g.logger.debug("x_trial: %s" % self.x_trial)

    def set_y_trial(self, trial_scores):
        self.y_trial = trial_scores

    def predict(self):
        if self.use_pairwise_cosine:
            self.predictions = self.cls.predict(transpose(self.x_trial)).tolist()
        else:
            self.predictions = self.cls.predict(self.x_trial).tolist()
        # if not type(self.vec) is CountVectorizer and type(self.cls) is not sklearn.svm.SVR:
        #     print classification_report(self.y_trial, self.predictions)
        return self.predictions

    def get_metrics(self):
        self.get_most_informant_features()
        self.sklearn_precision_score = sklearn.metrics.precision_score(self.y_trial,
                                                                       self.predictions,
                                                                       labels=self.y_trial)
        print self.sklearn_precision_score
        print "precision:", nltk.metrics.precision(set(self.y_trial), set(self.predictions))
        print "recall:", nltk.metrics.recall(set(self.y_trial), set(self.predictions))
        print "f_measure:", nltk.metrics.f_measure(set(self.y_trial), set(self.predictions))

    def save_model(self, custom_name=None):
        """
        Saves current trained classifier's model to file
        classifier_name_datetime.pkl
        :return:None
        """
        joblib.dump(self.cls, '../data/cls_models/{0}.pkl'.format(g.CLASSIFIER_TYPE.name[self.cls_type] + (custom_name if custom_name is not None else "")))
        joblib.dump(self.vec, '../data/cls_models/{0}.pkl'.format(g.VECTORIZER.name[self.vectorizer_type] + (custom_name if custom_name is not None else "")))
        joblib.dump(self.tfidf_transformer, '../data/cls_models/{0}.pkl'.format("TfidfTransformer"+ (custom_name if custom_name is not None else "")))
        joblib.dump(self.selector, '../data/cls_models/{0}.pkl'.format("Selector"+ (custom_name if custom_name is not None else "")))

    def save_model_in_db(self, dataset, selected_features):
        """
        Saves current trained classifier's model to file
        classifier_name_datetime.pkl
        :return:None
        """
        self.save_model()

    def load_model(self, custom_name=None):
        import os.path
        full_path_cls = "../data/cls_models/{0}.pkl".format(g.CLASSIFIER_TYPE.name[self.cls_type]+ (custom_name if custom_name is not None else ""))
        full_path_vec = '../data/cls_models/{0}.pkl'.format(g.VECTORIZER.name[self.vectorizer_type]+ (custom_name if custom_name is not None else ""))
        full_path_tf = '../data/cls_models/{0}.pkl'.format("TfidfTransformer"+ (custom_name if custom_name is not None else ""))
        full_path_sel = '../data/cls_models/{0}.pkl'.format("Selector"+ (custom_name if custom_name is not None else ""))

        if os.path.isfile(full_path_cls) and os.path.isfile(full_path_vec):
            self.cls = joblib.load(full_path_cls)
            self.vec = joblib.load(full_path_vec)
        else:
            raise Exception("Could not instantiate Classifier and Vectorizer - File not found!")

        if os.path.isfile(full_path_tf) and os.path.isfile(full_path_sel):
            self.tfidf_transformer = joblib.load(full_path_tf)
            self.selector = joblib.load(full_path_sel)
        else:
            raise Exception("Could not instantiate tfidf_transformer and selector- File not found!")

    def load_model_from_db(self):
        """
            always get the latest in database?
        """
        q = """SELECT dataset FROM SentiFeed.ClassifierModels order by datetime_trained asc limit 1;"""
        model = g.mysql_conn.execute_query(q)
        if model is not None:
            self.cls = model
        else:
            raise Exception("Could not load model for {0}".format(g.CLASSIFIER_TYPE.name[self.cls_type]))

    def get_most_informant_features(self, n=30):
        print "IMPORTANT FEATURES:\n"

        feature_names = self.vec.get_feature_names()
        if self.cls_type not in self.do_not_have_coefs:
            coefs_with_fns = sorted(zip(self.cls.coef_[0], feature_names))
            top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])

            for (coef_1, fn_1), (coef_2, fn_2) in top:
                important_feature = "\t%.4f\t%-15s\n\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2)
                print important_feature

            coefs_with_fns = sorted(zip(self.cls.coef_[0], feature_names))
            top = zip(coefs_with_fns[:200], coefs_with_fns[:-(200 + 1):-1])
            for (coef_1, fn_1), (coef_2, fn_2) in top:
                important_feature = "{0} {1} {2} {3}".format(coef_1, fn_1, coef_2, fn_2)
                g.logger.info("important_feature %s" % important_feature)
                self.important_features.append(important_feature)

    def cross_validation_split(self, percent=0.35):
        X_train, X_test, y_train, y_test = cross_validation.train_test_split([], [], test_size=percent, random_state=1)
        self.set_y_train(y_train)
        self.set_y_trial(y_test)
        self.set_x_train(X_train)
        self.set_x_trial(X_test)

    def predict_cross_validate(self, data, scores, num_of_validations=10):
        # X_train, X_test, y_train, y_test = cross_validation.train_test_split(data, scores, test_size=0.2, random_state=1)
        self.set_y_train(scores)
        # self.set_y_trial(data)
        self.set_x_train(data)
        # self.set_x_trial(X_test)
        all_data = self.x_train  # concatenate(self.Xtrain, self.Xtrial, axis=0)
        all_scores = scores
        final_scoring = cross_validation.cross_val_score(self.cls, all_data, all_scores, cv=num_of_validations, scoring='accuracy')
        self.print_cross_validation_accuracy(final_scoring)
        return final_scoring

    def cross_val_predict(self, data, scores):
        self.set_y_train(scores)
        # self.set_y_trial(data)
        self.set_x_train(data)
        # self.set_x_trial(X_test)
        all_data = self.x_train  # concatenate(self.Xtrain, self.Xtrial, axis=0)
        all_scores = scores
        return cross_validation.cross_val_predict(self.cls, all_data, y=all_scores, cv=10, n_jobs=1)

    def print_cross_validation_accuracy(self, scores):
        print scores
        print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

    def remove_non_ascii(self, text):
        return str(filter(lambda x: x in string.printable, text))

    def select_k_importance(self, k=10):
        """
        http://datascience.stackexchange.com/questions/6683/feature-selection-using-feature-importances-in-random-forests-with-scikit-learn
        :param k:
        :return:
        """
        return self.x_train[:, self.cls.feature_importances_.argsort()[::-1][:k]]

    def plot(self):
        import numpy as np
        import matplotlib.pyplot as plt
        # get the separating hyperplane
        w = self.cls.coef_[0]
        a = -w[0] / w[1]
        xx = np.linspace(-5, 5)
        yy = a * xx - (self.cls.intercept_[0]) / w[1]

        # plot the parallels to the separating hyperplane that pass through the
        # support vectors
        margin = 1 / np.sqrt(np.sum(self.cls.coef_ ** 2))
        yy_down = yy + a * margin
        yy_up = yy - a * margin

        # plot the line, the points, and the nearest vectors to the plane
        plt.figure(1, figsize=(4, 3))
        plt.clf()
        plt.plot(xx, yy, 'k-')
        plt.plot(xx, yy_down, 'k--')
        plt.plot(xx, yy_up, 'k--')

        plt.scatter(self.cls.support_vectors_[:, 0], self.cls.support_vectors_[:, 1], s=80,
                    facecolors='none', zorder=10)
        plt.scatter(self.x_train[:, 0], self.x_train[:, 1], c=self.y, zorder=10, cmap=plt.cm.Paired)

        plt.axis('tight')
        x_min = -4.8
        x_max = 4.2
        y_min = -6
        y_max = 6

        # XX, YY = np.mgrid[x_min:x_max:200j, y_min:y_max:200j]
        # Z = self.cls.predict(np.c_[XX.ravel(), YY.ravel()])

        # Put the result into a color plot
        # Z = self.x.reshape(XX.shape)
        # plt.figure(1, figsize=(4, 3))
        # plt.pcolormesh(self.x_trial, self.y_trial, Z, cmap=plt.cm.Paired)

        plt.xlim(x_min, x_max)
        plt.ylim(y_min, y_max)

        plt.xticks(())
        plt.yticks(())
        
    def save_settings(self, trials_id):
        try:
            g.mysql_conn.execute_query(Classifier._save_cls_settings_q().format(trials_id, g.CLASSIFIER_TYPE.name[self.cls_config.cls_type], str(self)))
            g.mysql_conn.execute_query(Classifier._save_vec_settings_q().format(trials_id, g.VECTORIZER.name[self.cls_config.vec_type], str(self.vec)))
        except:
            traceback.print_exc()

    @staticmethod
    def _save_cls_settings_q():
        return '''INSERT INTO SentiFeed.ClassifierSettings
                (
                trials_id,
                `name`,
                parameters
                )
                VALUES
                (
                {0},
                '{1}',
                '{2}'
                );
                '''

    @staticmethod
    def _save_vec_settings_q():
        return '''INSERT INTO SentiFeed.VectorizerSettings
                (
                trials_id,
                `name`,
                parameters
                )
                VALUES
                (
                {0},
                '{1}',
                "{2}"
                );
                '''


class MaxVoteClassifier(object):
    """
        Takes as input a list of pre-trained classifiers and calculates the Frequency Distribution of their predictions
    """
    def __init__(self, classifiers):
        self._classifiers = classifiers
        self.predictions = None

    def classify(self, tweet_fea):
        counts = FreqDist()
        for classifier in self._classifiers:
            classifier.set_x_trial([tweet_fea])
            counts[classifier.predict()[0]] += 1
            # counts[classifier.cls.predict(classifier.x_trial[i::classifier.x_trial[i].shape[1]])[0]] += 1

        return counts.max()

    def load(self):

        for cls in self._classifiers:
            cls.load_model("MAXVOTE")
