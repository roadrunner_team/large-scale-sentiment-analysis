from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from SentimentAnalysis.FigurativeTextAnalysis.models.Config import DatasetConfig, ClsConfig, ProcessConfig, TrialConfig
from SentimentAnalysis.FigurativeTextAnalysis.models.DatasetHandler import DatasetHandler
from SentimentAnalysis.FigurativeTextAnalysis.models.Trial_ import Trial

__author__ = 'mariakaranasou'

selected_features_all =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_no_emoticons =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    # '__POS_SMILEY__',           # * // << +
                    # '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_only_position_with_emoticons =[
                    # 'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_only_position_no_emoticons =[
                    # 'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    # '__POS_SMILEY__',           # * // << +
                    # '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]


selected_features_only_postags =[
                    'postags',              # * // << +
                    # # "__lemma_word__"
                    # # 'words',              #
                    # '__swn_score__',
                    # 's_word',               # * // << +
                    # 'pos_position_'
                    # '__res__',                  # * <<
                    # '__multiple_chars_in_a_row__',
                    # '__punctuation_percentage__',
                    # '__hashtag_lexicon_sum__',
                    # '__is_metaphor__',
                    # '__synset_length__',
                    # '__OH_SO__',                  # * // <<   +
                    # '__DONT_YOU__',               # * // <<   +
                    # '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    # '__CAPITAL__',                # * // <<   +
                    # '__HT__',                     # * <<      +
                    # '__HT_POS__',               # * // <<   +
                    # '__HT_NEG__',               # * // <<   +
                    # '__LINK__',               # //
                    # # '__POS_SMILEY__',           # * // << +
                    # # '__NEG_SMILEY__',           # * // << +
                    # '__NEGATION__',             # * // << +
                    # '__REFERENCE__',            # * // << +
                    # '__questionmark__',         # * // << +
                    # '__exclamation__',          # * // << +
                    # '__fullstop__',
                    # '__RT__',                 # //
                    # '__LAUGH__',              # //
                    # # '__LOVE__',               # //
                ]
selected_features_only_postags_and_swn =[
                    'postags',              # * // << +
                    # # "__lemma_word__"
                    # # 'words',              #
                    # '__swn_score__',
                    's_word',               # * // << +
                    # 'pos_position_'
                    # '__res__',                  # * <<
                    # '__multiple_chars_in_a_row__',
                    # '__punctuation_percentage__',
                    # '__hashtag_lexicon_sum__',
                    # '__is_metaphor__',
                    # '__synset_length__',
                    # '__OH_SO__',                  # * // <<   +
                    # '__DONT_YOU__',               # * // <<   +
                    # '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    # '__CAPITAL__',                # * // <<   +
                    # '__HT__',                     # * <<      +
                    # '__HT_POS__',               # * // <<   +
                    # '__HT_NEG__',               # * // <<   +
                    # '__LINK__',               # //
                    # # '__POS_SMILEY__',           # * // << +
                    # # '__NEG_SMILEY__',           # * // << +
                    # '__NEGATION__',             # * // << +
                    # '__REFERENCE__',            # * // << +
                    # '__questionmark__',         # * // << +
                    # '__exclamation__',          # * // << +
                    # '__fullstop__',
                    # '__RT__',                 # //
                    # '__LAUGH__',              # //
                    # # '__LOVE__',               # //
                ]


__author__ = 'mariakaranasou'


def task11():
    ds_config = DatasetConfig()
    ds_config.task11 = 15000
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()
    different_cls("task_11", ds)
    only_postags_and_swn("task_11", ds)
    ds = None


def task2b():
    ds_config = DatasetConfig()
    ds_config.task2b = 15000
    ds_config.task2bdev = 15000
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    different_cls("task2b", ds)
    only_postags_and_swn("task2b", ds)
    ds = None


def one_point_six():
    ds_config = DatasetConfig()
    ds_config.one_point_six_m = 30000
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    different_cls("one_point_six", ds)
    only_postags_and_swn("one_point_six", ds)
    ds = None


def all_50k():
    ds_config = DatasetConfig()
    ds_config.set_50K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()
    different_cls("50k", ds)
    only_postags_and_swn("50k", ds)
    ds = None


def all_70k():
    ds_config = DatasetConfig()
    ds_config.set_70K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()
    different_cls("70k", ds)
    only_postags_and_swn("70k", ds)
    ds = None


def all_90k():
    ds_config = DatasetConfig()
    ds_config.set_90K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    different_cls("90k", ds)
    only_postags_and_swn("90k", ds)
    ds = None


def all_110k():
    ds_config = DatasetConfig()
    ds_config.set_110K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    different_cls("110k", ds)
    only_postags_and_swn("110k", ds)
    ds = None


def only_postags_and_swn(title, ds):


    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags, ds)                # was WRONG
    trial_config = TrialConfig(process_config, selected_features_only_postags, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags SVMStandalone {0} Dict  - re-run".format(title))


    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags max_vote=True {0} Dict - re-run".format(title))


    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags NBayes {0} Dict - re-run".format(title))


    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.Perceptron, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags Perceptron {0} Dict - re-run".format(title))


    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags DecisionTree {0} Dict - re-run".format(title))


    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags SGD {0} Dict - re-run".format(title))

    # =========================================================================================== #
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags_and_swn, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags_and_swn, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags_and_swn SVMStandalone {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags_and_swn, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags_and_swn, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags_and_swn max_vote=True {0} Dict - re-run".format(title))


    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags_and_swn, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags_and_swn, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags_and_swn NBayes {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.Perceptron, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags_and_swn, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags_and_swn, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags_and_swn Perceptron {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags_and_swn, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags_and_swn, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags_and_swn DecisionTree {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_postags_and_swn, ds)
    trial_config = TrialConfig(process_config, selected_features_only_postags_and_swn, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_postags_and_swn SGD {0} Dict - re-run".format(title))


    cls_config = None
    process_config = None
    trial_config = None
    trial = None


def different_cls(title, ds):
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)       # was WRONG
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons NBayes {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.Perceptron, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons Perceptron {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)   # was WRONG
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons DecisionTree {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)        # was WRONG
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons SGD {0} Dict - re-run".format(title))

    # ============================================================================================ #
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,           #TODO:
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)  # was WRONG
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons NBayes {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.Perceptron, vec_type=g.VECTORIZER.Dict,       #TODO:
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)  # was WRONG
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons Perceptron {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons DecisionTree {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_no_emoticons, ds)   # was WRONG
    trial_config = TrialConfig(process_config, selected_features_only_position_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_no_emoticons SGD {0} Dict - re-run".format(title))

    # ============================================================================================ #
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)   # was WRONG
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons NBayes {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.Perceptron, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)   # was WRONG
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons Perceptron {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)   # was WRONG
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons DecisionTree {0} Dict - re-run".format(title))

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_only_position_with_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_only_position_with_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_only_position_with_emoticons SGD {0} Dict - re-run".format(title))

    cls_config = None
    process_config = None
    trial_config = None
    trial = None


def cross_validate(title, ds):

    ds.get_clean_total_data_list()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)

    clean_data = [trial.tweet_processor._get_clean_feature_dict(x) for x in  ds.total_data]

    scores = trial.tweet_processor.classifier.predict_cross_validate(clean_data, ds.total_scores)
    trial.save_cross_validate(title + " SVMStandalone Dict", scores)
    print title + " SVMStandalone Dict", scores

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(clean_data, ds.total_scores)
    trial.save_cross_validate(title + " NBayes Dict", scores)
    print title + " NBayes Dict", scores

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(clean_data, ds.total_scores)
    trial.save_cross_validate(title + " SGD Dict", scores)
    print title + " SGD Dict", scores

# ==================================== MISSING  ====================================== #

def all50k_missing_cross_val():
    ds_config = DatasetConfig()
    ds_config.set_50K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    # ds.get_test_and_trial_set()

    cross_validate("50k crossvalidation", ds)


def all70k_missing_cross_val():
    ds_config = DatasetConfig()
    ds_config.set_70K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    # ds.get_test_and_trial_set()

    cross_validate("70k crossvalidation", ds)


def all90k_missing_cross_val():
    ds_config = DatasetConfig()
    ds_config.set_90K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    # ds.get_test_and_trial_set()

    cross_validate("90k crossvalidation", ds)


def all110k_missing_cross_val():
    ds_config = DatasetConfig()
    ds_config.set_110K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    # ds.get_test_and_trial_set()

    cross_validate("110k crossvalidation", ds)


if __name__ == '__main__':

    task11()
    task2b()
    one_point_six()
    all_50k()
    all_70k()
    all_90k()
    all_110k()