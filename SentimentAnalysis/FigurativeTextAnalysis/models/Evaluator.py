import sklearn
from SentimentAnalysis.FigurativeTextAnalysis.models.Scorer import Scorer

__author__ = 'maria'


class Evaluator(object):
    def __init__(self, ground_truth, predictions):
        self.ground_truth = ground_truth
        self.predictions = predictions

    def get_accuracy(self):
        return sklearn.metrics.accuracy_score(self.ground_truth, self.predictions)

    def get_precision(self):
        return sklearn.metrics.precision_score(self.ground_truth, self.predictions)

    def get_recall(self):
        """
        sklearn.metrics.recall_score(y_true, y_pred, labels=None, pos_label=1, average='binary', sample_weight=None)
        :return:
        """
        return sklearn.metrics.recall_score(self.ground_truth, self.predictions)

    def get_f_measure(self):
        return sklearn.metrics.f1_score(self.ground_truth, self.predictions)

    def get_confusion_matrix(self):
        from sklearn.metrics import confusion_matrix
        return confusion_matrix(self.ground_truth, self.predictions)

    def get_mse(self):
        return sklearn.metrics.mean_squared_error(self.ground_truth, self.predictions)

    def get_classification_report(self):
        from sklearn.metrics import classification_report
        return classification_report(self.ground_truth, self.predictions)

    def get_cosine(self):
        scorer = Scorer(self.ground_truth, self.predictions)
        return scorer.cosine_()

    def precision_recall_f_score_support(self):
        return sklearn.metrics.precision_recall_fscore_support(self.ground_truth, self.predictions)

    def save(self, trials_id):
        from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g

        precision, recall, f_score, support = self.precision_recall_f_score_support()
        cosine = self.get_cosine()
        mse = self.get_mse()
        cls_report = self.get_classification_report()
        accuracy = self.get_accuracy()

        cls = [-1, 0, 1]
        for i in xrange(0, len(precision)):
            g.mysql_conn.execute_query(Evaluator.save_q().format(trials_id,
                                                            cls[i],
                                                            support[i],
                                                            accuracy,
                                                            precision[i],
                                                            recall[i],
                                                            f_score[i],
                                                            cosine,
                                                            mse,
                                                            str(cls_report)
                                                            ))

    @staticmethod
    def save_q():
        return '''INSERT INTO SentiFeed.Results
                (
                trials_id,
                class,
                support,
                accuracy,
                `precision`,
                recall,
                f1_score,
                cosine_similarity,
                mse,
                report
                )
                VALUES
                (
                {0},
                {1},
                {2},
                {3},
                {4},
                {5},
                {6},
                {7},
                {8},
                '{9}'
                );
                 '''