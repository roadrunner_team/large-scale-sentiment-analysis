import ast
import copy
import datetime
import nltk
from numpy.ma import transpose, concatenate
import scipy
from scipy.spatial.distance import pdist
import sklearn
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.feature_selection import VarianceThreshold, chi2, f_classif
from sklearn.feature_selection import SelectKBest
from sklearn.linear_model import SGDClassifier, Perceptron
from sklearn.metrics.pairwise import linear_kernel
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.svm import NuSVC
from sklearn.tree import DecisionTreeClassifier
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from sklearn.ensemble import RandomForestRegressor, AdaBoostClassifier, BaggingClassifier
from nltk.metrics import precision, recall, f_measure
from sklearn.metrics import classification_report
from sklearn.externals import joblib
from sklearn import cross_validation

# pairwise cosine= http://stackoverflow.com/questions/12118720/python-tf-idf-cosine-to-find-document-similarity

__author__ = 'maria'


class Classifier(object):
    """
        Wrapper class for scikit-learn classifiers.
    """
    def __init__(self, cls_type, vectorizer_type, use_pairwise_cosine=False, use_tf_transformer=True, use_k_best=False):
        self.estimator = sklearn.svm.LinearSVC()
        self.cls_type = cls_type
        self.cls = self._set_up_classifier(cls_type)
        self.vectorizer_type = vectorizer_type
        self.vec = self._set_up_vectorizer(vectorizer_type)
        self.Xtrain = None
        self.Y = None
        self.Xtrial = None
        self.YTrial = None
        self.predictions = []
        self.XtrainCopy = None
        self.use_pairwise_cosine = use_pairwise_cosine
        self.use_tf_transformer = use_tf_transformer
        self.tfidf_transformer = TfidfTransformer(use_idf=False)
        self.sklearn_precision_score = ""
        self.sel = VarianceThreshold(threshold=(.9 * (1 - .9)))
        self.k_best = SelectKBest(chi2, k=1900)
        self.use_k_best = use_k_best
        self.min_max_scaler = MinMaxScaler()
        self.standard_scaler = StandardScaler()
        self.do_not_have_coefs = [g.CLASSIFIER_TYPE.DecisionTree, g.CLASSIFIER_TYPE.RandomForestRegressor,
                                  g.CLASSIFIER_TYPE.SVR, g.CLASSIFIER_TYPE.SVC, g.CLASSIFIER_TYPE.Ada,
                                  g.CLASSIFIER_TYPE.Bagging]

    def _set_up_classifier(self, cls_type):
        if cls_type == g.CLASSIFIER_TYPE.NBayes:
            return MultinomialNB()
        elif cls_type == g.CLASSIFIER_TYPE.SVM:
            return OneVsRestClassifier(estimator=sklearn.svm.LinearSVC())
        elif cls_type == g.CLASSIFIER_TYPE.DecisionTree:
            return DecisionTreeClassifier()
        elif cls_type == g.CLASSIFIER_TYPE.SVMStandalone:
            return sklearn.svm.LinearSVC(C=1)  # , penalty="l1", dual=False
        elif cls_type == g.CLASSIFIER_TYPE.SVC:
            return sklearn.svm.SVC(C=1, gamma=0.001)
        elif cls_type == g.CLASSIFIER_TYPE.SVR:
            return sklearn.svm.SVR(kernel='linear', max_iter=150)  # >> 150
        elif cls_type == g.CLASSIFIER_TYPE.NuSVR:
            return sklearn.svm.NuSVR(kernel='linear', max_iter=10)
        elif cls_type == g.CLASSIFIER_TYPE.RandomForestRegressor:
            return RandomForestRegressor(n_estimators=2, max_depth=50)
        elif cls_type == g.CLASSIFIER_TYPE.SGD:
            return SGDClassifier(loss='perceptron', n_iter=25)
        elif cls_type == g.CLASSIFIER_TYPE.Perceptron:
            return Perceptron(penalty="l2", n_iter=50, n_jobs=-1)
        elif cls_type == g.CLASSIFIER_TYPE.Ada:
            return AdaBoostClassifier(base_estimator=SGDClassifier(loss='log'), n_estimators=50)
        elif cls_type == g.CLASSIFIER_TYPE.Bagging:
            return BaggingClassifier(base_estimator=SGDClassifier(loss='hinge'), n_estimators=50)

        else:
            raise NotImplementedError("The type of classifier: {0} is not implemented".format(cls_type))

    def _set_up_vectorizer(self, vectorizer_type):
        if vectorizer_type == g.VECTORIZER.Dict:
            return DictVectorizer(sparse=True)
        elif vectorizer_type == g.VECTORIZER.Count:
            return CountVectorizer(analyzer='word',
                                   input='content',
                                   lowercase=True,
                                   min_df=1,
                                   binary=False,
                                   stop_words='english')
        elif vectorizer_type == g.VECTORIZER.Idf:
            return TfidfVectorizer(use_idf=True, stop_words='english')
        elif vectorizer_type == g.VECTORIZER.Tf:
            return TfidfVectorizer(use_idf=False, stop_words='english')
        else:
            raise NotImplementedError

    def set_x_train(self, train_list_of_dicts):
        self.Xtrain = self.vec.fit_transform(train_list_of_dicts)
        g.logger.debug(train_list_of_dicts[0])
        g.logger.debug(self.Xtrain[0:1])

        if self.use_tf_transformer:
            self.Xtrain = self.tfidf_transformer.fit_transform(self.Xtrain).toarray()

        if self.use_k_best:
            self.Xtrain = self.k_best.fit_transform(self.Xtrain, self.Y)

        if self.use_pairwise_cosine:
            self.XtrainCopy = copy.copy(self.Xtrain)
            self.Xtrain = sklearn.metrics.pairwise.pairwise_distances(self.Xtrain[0:1], self.Xtrain, metric='cosine', n_jobs=1)

        # self.Xtrain = euclidean_distances(self.Xtrain)

        if not type(self.vec) is CountVectorizer and not type(self.vec) is TfidfVectorizer:
            g.logger.debug("feature_names: %s" % self.vec.feature_names_)
            g.logger.debug("vocabulary_: %s" % self.vec.vocabulary_)
        g.logger.debug("Xtrain: %s" % self.Xtrain)

    def set_y_train(self, handcoded_score_list):
        self.Y = handcoded_score_list
        g.logger.debug("Y:: %s" % self.Y)

    def train(self):
        print self.Xtrain.shape[0], len(self.Y)
        if self.use_pairwise_cosine:
            self.cls.fit(transpose(self.Xtrain), self.Y)     # transpose is used when cosine similarities are calculated
        else:
            self.cls.fit(self.Xtrain, self.Y)

    def set_x_trial(self, trial_list_of_dicts):
        self.Xtrial = self.vec.transform(trial_list_of_dicts)

        if self.use_tf_transformer:
            self.Xtrial = self.tfidf_transformer.transform(self.Xtrial)

        if self.use_k_best:
            self.Xtrial = self.k_best.transform(self.Xtrial)

        if self.use_pairwise_cosine:
            self.Xtrial = sklearn.metrics.pairwise.pairwise_distances(self.XtrainCopy[0:1],
                                                                      self.Xtrial,
                                                                      metric='cosine',
                                                                      n_jobs=1)
            # self.Xtrial = linear_kernel(self.Xtrial[0:1], self.Xtrial)
            # x = pdist(transpose(self.Xtrial.toarray()), 'cosine')

        # self.Xtrial = euclidean_distances(self.XtrainCopy)


    def set_y_trial(self, trial_scores):
        self.YTrial = trial_scores

    def predict(self):
        if self.use_pairwise_cosine:
            self.predictions = self.cls.predict(transpose(self.Xtrial)).tolist()
        else:
            self.predictions = self.cls.predict(self.Xtrial).tolist()
        if not type(self.vec) is CountVectorizer and not type(self.cls) in [Perceptron, sklearn.svm.SVR]:
            print classification_report(self.YTrial, self.predictions)
        return self.predictions

    def get_metrics(self, vec):
        self.get_most_informant_features(self.cls, vec)
        self.sklearn_precision_score = sklearn.metrics.precision_score(self.YTrial,
                                                                       self.predictions,
                                                                       labels=self.YTrial,
                                                                       average='weighted')
        print self.sklearn_precision_score
        print "precision:", nltk.metrics.precision(set(self.YTrial), set(self.predictions))
        print "recall:", nltk.metrics.recall(set(self.YTrial), set(self.predictions))
        print "f_measure:", nltk.metrics.f_measure(set(self.YTrial), set(self.predictions))

    def save_model(self):
        """
        Saves current trained classifier's model to file
        classifier_name_datetime.pkl
        :return:None
        """
        joblib.dump(self.cls, '../data/cls_models/{0}.pkl'.format(g.CLASSIFIER_TYPE.name[self.cls_type]))
        joblib.dump(self.vec, '../data/cls_models/{0}.pkl'.format(g.VECTORIZER.name[self.vectorizer_type]))

    def save_model_in_db(self, dataset, selected_features):
        """
        Saves current trained classifier's model to file
        classifier_name_datetime.pkl
        :return:None
        """
        self.save_model()
        # q = """INSERT INTO `SentiFeed`.`ClassifierModels`
        #                     (`cls_name`,
        #                     `dataset`,
        #                     `selected_features`,
        #                     `model`,
        #                     `datetime_trained`)
        #                     VALUES
        #                     (
        #                     '{0}',
        #                     '{1}',
        #                      "{2}",
        #                     "{3}",
        #                     "{4}"
        #                     );
        #                     """
        #
        # cls_str = str(self.cls)
        #
        # g.mysql_conn.update(q.format(g.CLASSIFIER_TYPE.name[self.cls_type],
        #                             dataset,
        #                             selected_features,
        #                             cls_str.replace("\"", "\'").replace("\'", "\\'"),
        #                             datetime.datetime.now()))

    def load_model(self):
        import os.path
        full_path_cls = "../data/cls_models/{0}.pkl".format(g.CLASSIFIER_TYPE.name[self.cls_type])
        full_path_vec = '../data/cls_models/{0}.pkl'.format(g.VECTORIZER.name[self.vectorizer_type])
        if os.path.isfile(full_path_cls) and os.path.isfile(full_path_vec):
            self.cls = joblib.load(full_path_cls)
            self.vec = joblib.load(full_path_vec)
        else:
            raise Exception("Model file name must be provided")

    def load_model_from_db(self):
        """
            always get the latest in database?
        """
        q = """SELECT dataset FROM SentiFeed.ClassifierModels order by datetime_trained asc limit 1;"""
        model = g.mysql_conn.execute_query(q)
        if model is not None:
            self.cls = model
        else:
            raise Exception("Could not load model for {0}".format(g.CLASSIFIER_TYPE.name[self.cls_type]))

    def set_x_trial(self, trial_list_of_dicts):
        self.Xtrial = self.vec.transform(trial_list_of_dicts)

        if self.use_tf_transformer:
            self.Xtrial = self.tfidf_transformer.transform(self.Xtrial).toarray()

        if self.use_k_best:
            self.Xtrial = self.k_best.transform(self.Xtrial)

        if self.use_pairwise_cosine:
            self.Xtrial = sklearn.metrics.pairwise.pairwise_distances(self.XtrainCopy[0:1],
                                                                      self.Xtrial,
                                                                      metric='cosine',
                                                                      n_jobs=1)
            # self.Xtrial = linear_kernel(self.Xtrial[0:1], self.Xtrial)
            # x = pdist(transpose(self.Xtrial.toarray()), 'cosine')

        # self.Xtrial = euclidean_distances(self.XtrainCopy)


    def set_y_trial(self, trial_scores):
        self.YTrial = trial_scores

    def predict(self):
        if self.use_pairwise_cosine:
            self.predictions = self.cls.predict(transpose(self.Xtrial)).tolist()
        else:
            self.predictions = self.cls.predict(self.Xtrial).tolist()
        if not type(self.vec) is CountVectorizer and not type(self.cls) in [Perceptron, sklearn.svm.SVR]:
            print classification_report(self.YTrial, self.predictions)
        return self.predictions

    def get_metrics(self, vec):
        self.get_most_informant_features(self.cls, vec)
        self.sklearn_precision_score = sklearn.metrics.precision_score(self.YTrial,
                                                                       self.predictions,
                                                                       labels=self.YTrial,
                                                                       average='weighted')
        print self.sklearn_precision_score
        print "precision:", nltk.metrics.precision(set(self.YTrial), set(self.predictions))
        print "recall:", nltk.metrics.recall(set(self.YTrial), set(self.predictions))
        print "f_measure:", nltk.metrics.f_measure(set(self.YTrial), set(self.predictions))

    def get_most_informant_features(self, classifier, vec, n=10):
        print "IMPORTANT FEATURES:\n"

        feature_names = vec.get_feature_names()
        if self.cls_type not in self.do_not_have_coefs:
            coefs_with_fns = sorted(zip(classifier.coef_[0], feature_names))
            top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])

            for (coef_1, fn_1), (coef_2, fn_2) in top:
                important_feature = "\t%.4f\t%-15s\n\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2)
                print important_feature
                g.logger.debug("important_feature %s" % important_feature)

    def cross_validation_split(self, percent=0.35):
        X_train, X_test, y_train, y_test = cross_validation.train_test_split([], [], test_size=percent, random_state=1)
        self.set_y_train(y_train)
        self.set_y_trial(y_test)
        self.set_x_train(X_train)
        self.set_x_trial(X_test)

    def predict_cross_validate(self, data, scores, num_of_validations=10):
        # X_train, X_test, y_train, y_test = cross_validation.train_test_split(data, scores, test_size=0.2, random_state=1)
        self.set_y_train(scores)
        # self.set_y_trial(data)
        self.set_x_train(data)
        # self.set_x_trial(X_test)
        all_data = self.Xtrain  # concatenate(self.Xtrain, self.Xtrial, axis=0)
        all_scores = scores
        return cross_validation.cross_val_score(self.cls, all_data, all_scores, cv=num_of_validations)

    def cross_val_predict(self, data, scores):
        self.set_y_train(scores)
        # self.set_y_trial(data)
        self.set_x_train(data)
        # self.set_x_trial(X_test)
        all_data = self.Xtrain  # concatenate(self.Xtrain, self.Xtrial, axis=0)
        all_scores = scores
        return cross_validation.cross_val_predict(self.cls, all_data, y=all_scores, cv=10, n_jobs=1)

    def print_cross_validation_accuracy(self, scores):
        print scores
        print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
