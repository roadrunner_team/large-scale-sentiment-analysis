from datetime import datetime
import nltk
from SentimentAnalysis.FigurativeTextAnalysis.models.Config import TrialConfig, ProcessConfig, ClsConfig, DatasetConfig
from SentimentAnalysis.FigurativeTextAnalysis.models.DatasetHandler import DatasetHandler
from SentimentAnalysis.FigurativeTextAnalysis.models.Evaluator import Evaluator
from SentimentAnalysis.FigurativeTextAnalysis.models.Scorer import Scorer
from SentimentAnalysis.FigurativeTextAnalysis.models.SelectedFeatures import SelectedFeatures
from SentimentAnalysis.FigurativeTextAnalysis.processors.TweetProcessor_ import TweetProcessor
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from TrialScore import TrialScore

__author__ = 'maria'


class Trial(object):
    """

    """
    def __init__(self, trial_config, use=0):
        self.trial_config = trial_config
        if self.trial_config is None:
            raise Exception("No trial configuration found!")
        self.tweet_processor = TweetProcessor(self.trial_config.process_config, use)
        self._id = None
        self.discretization = self.trial_config.discretization
        self.selected_features = SelectedFeatures(trial_config.selected_features)
        self.selected_features_id = None
        self.predictions = []
        self.x_train = []
        self.y_train = []
        self.x_test = []
        self.y_test = []
        self.trial_set_len = 0
        self.test_set_len = 0
        self.match = 0.0
        self.accuracy = 0.0
        self.precision = None
        self.recall = None
        self.f_measure = None
        self.cosine_similarity = None
        self.labels_range = None
        self.date_created = datetime.today()

    def start(self):
        if not self.trial_config.process_config.preprocess:
            self.selected_features_id = self.selected_features.save()

        self.tweet_processor.process()

    @staticmethod
    def insert_trials():
        return '''INSERT INTO SentiFeed.Trials
                (selected_features,
                title,
                date_time)
                VALUES
                ("{0}", "{1}", "{2}");'''

    @staticmethod
    def insert_cls_settings():
        return ''' '''

    @staticmethod
    def insert_vec_settings():
        return ''' '''

    @staticmethod
    def insert_q():
            return '''INSERT INTO SentiFeed.Trial
            ( selected_features_id, discretization, main_classifier, helper_classifier, trial_set_len, test_set_len, matched,
                correct, precision_, recall, f_measure, cosine_similarity, labels_range, date_created)
            VALUES
            ("{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", "{7}", "{8}", "{9}", "{10}", "{11}", "{12}", "{13}");'''

    def classify(self):
        self.selected_features_id = self.selected_features.save()  # save selected features and keep id
        self.tweet_processor.process()

    def get_stats(self):
        self.predictions = self.tweet_processor.predictions
        self.x_train = self.tweet_processor.feature_list_train
        self.x_test = self.tweet_processor.feature_list_test
        self.y_train = self.tweet_processor.score_list_train
        self.y_test = self.tweet_processor.score_list_test
        self.trial_set_len = len(self.tweet_processor.train_set)
        self.test_set_len = len(self.tweet_processor.test_set)
        self.match = self.calculate_match()
        self.accuracy = nltk.metrics.accuracy(self.y_test, self.predictions)
        self.precision = nltk.metrics.precision(set(self.y_test), set(self.predictions))
        self.recall = nltk.metrics.recall(set(self.y_test), set(self.predictions))
        self.f_measure = nltk.metrics.f_measure(set(self.y_test), set(self.predictions))
        self.cosine_similarity = self.calculate_cosine_similarity(self.tweet_processor.dataset_handler.trial_set_scores, self.predictions)

        print "accuracy", self.accuracy
        print "self.precision", self.precision
        print "self.recall", self.recall
        print "self.f_measure", self.f_measure
        print "COSINE SIMILARITY: ", self.cosine_similarity

    def save_results(self, title):
        """
        Gets metrics and data sets and saves Trial and Score results
        :return:
        :rtype:
        """
        # create a trial record
        self.save_self(title)

        # create a dataset settings record AND scores records
        self.trial_config.process_config.dataset_handler.save(self._id, self.tweet_processor.predictions)

        # create classifier and vectorizer settings records
        self.tweet_processor.classifier.save_settings(self._id)

        # create results records
        evaluator = Evaluator(self.tweet_processor.score_list_test, self.predictions)
        evaluator.save(self._id)

    def save_self(self, title):
        lastrow = g.mysql_conn.update(self.insert_trials().format(self.trial_config.selected_features, title, self.date_created))
        self._id = lastrow
        g.logger.info("Trial id {0} for {1}".format(self._id, title))

    def save_cross_validate(self, title, scores):

        self.save_self(title)
        q = ''' INSERT INTO `SentiFeed`.`Crossvalidation`
                (`trials_id`,
                `title`,
                `scores`)
                VALUES
                ({0},
                "{1}",
                "{2}");
                '''
        g.mysql_conn.update(q.format(self._id, title, str(scores)))

    def calculate_cosine_similarity(self, test_set, predictions):
        """
        Given the test_set and the corresponding predictions, cosine similarity is calculated using Scorer.
        :param test_set: The list of TweetBO's provided to the chosen classifier for prediction.
        :type test_set: list of TweetBO objects
        :param predictions: The classifier's results (correspond one by one with tweets in test_set)
        :type predictions: list of float numbers
        :return: total cosine similarity
        :rtype: float
        """
        gold_list = []
        predictions_list = []
        print len(test_set), len(predictions)
        print test_set[0], predictions[0]
        for i in range(0, len(test_set)):
            gold_list.append([i, test_set[i]])
            predictions_list.append([i, predictions[i]])

        print test_set[0], gold_list[0], predictions_list[0]
        scorer = Scorer(gold_list, predictions_list)
        return scorer.cosine()

    def calculate_match(self):
        """
        :return: Percentage of correct predictions
        :rtype: float
        """
        if len(self.y_test) > 0:
            return float(self.match) / float(len(self.y_test))
        return 0


# ============================================== TEST ================================================================ #
# * : final combination of features
selected_features_all =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                ]
selected_features_all_correct =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                ]
selected_features_all_correct_no_pos =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    # 'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                ]
selected_features_all_correct_no_res =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    # '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                ]

selected_features_no_emoticons =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    # '__POS_SMILEY__',           # * // << +
                    # '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_only_position_with_emoticons =[
                    # 'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_'
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_only_position_no_emoticons =[
                    # 'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_'
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    # '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]
def _get_clean_feature_dict(dicta):

        for k in dicta.keys():
            if not excluded(k, str(dicta[k])):
                if 's_word-' in k or 'swn_score' in k:
                    # dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral" \
                    #             if 0.95 <= dicta[k] <= 1.05\
                    #             else "somewhat_positive" if 1.2 >= dicta[k] > 1.05 \
                    #             else "somewhat_negative"

                    if dicta[k] > 1.2:
                        dicta[k] = "positive"
                    elif dicta[k] < 0.2:
                        dicta[k] = "negative"
                    elif 0.95 <= dicta[k] <= 1.05:
                        dicta[k] = "neutral"
                    elif 0.95 > dicta[k] > 0.2:
                        dicta[k] = "somewhat_negative"
                    elif 1.05 < dicta[k] < 1.2:
                        dicta[k] = "somewhat_positive"

                # if self.use > 0:
                #     if dicta[k] in g.VERBS:
                #         dicta[k] = "VB"
                #     elif dicta[k] in g.NOUNS:
                #         dicta[k] = "NN"
                #     elif dicta[k] in g.ADJECTIVES:
                #         dicta[k] = "ADJ"
                #     elif dicta[k] in g.ADVERBS:
                #         dicta[k] = "RB"

                if k == "__punctuation_percentage__":
                    dicta[k] = 1 if dicta[k] > 5 else 0
                # if k == "__multiple_chars_in_a_row__":
                #     dicta[k] = 0 if dicta[k] == False else 1
                #
                # if k in self.tags:
                #     dicta[k] = 0 if dicta[k] == "False" else 1
                # if self.use > 0:
                # if "__lemma_word__" in k:
                #     dicta[k] = "True"


            else:
                del dicta[k]

        return dicta

def excluded(k, dicta_k_value):
    if 's_word' in k:                                   # if k is a sentiwordnet feature
        if 's_word' not in selected_features_all:      # if swn is not wanted
            return True                                 # remove
        return False
    if '__contains__' in k:                                # if k is a sentiwordnet feature
        if '__contains__' not in selected_features_all:   # if swn is not wanted
            return True                                 # remove
        return False
    if '__synset_length__' in k:                                # if k is a sentiwordnet feature
        if '__synset_length__' not in selected_features_all:   # if swn is not wanted
            return True                                     # remove
        return False
    if '__swn_score__' == k:                                # if k is sentiwordnet total score
        if '__swn_score__' not in selected_features_all:   # if swn is not wanted
            return True                                 # remove
        return False
    if 'pos_position_' in k:
        if 'pos_position_' not in selected_features_all:
            return True
        return False

    if dicta_k_value in g.VERBS or \
        dicta_k_value in g.ADJECTIVES or \
        dicta_k_value in g.ADVERBS or \
        dicta_k_value in g.NOUNS or \
        dicta_k_value in g.OTHERS:
        if 'postags' not in selected_features_all:
            return True
        return False
    if 'word-' in dicta_k_value:
        if 'words' not in selected_features_all or 'word' not in selected_features_all:
            return True
        return False
    if 't-similarity' in k:
        if 't_similarity' not in selected_features_all:
            return True
        return False
    if '__res__' == k:
        if '__res__' not in selected_features_all:
            return True
        return False
    if '__lin__' == k:
        if '__lin__' not in selected_features_all:
            return True
        return False
    if '__path__' == k:
        if '__path__' not in selected_features_all:
            return True
        return False
    if '__wup__' == k:
        if '__wup__' not in selected_features_all:
            return True
        return False
    if dicta_k_value in ["positive", "negative", "somewhat_positive", "somewhat_negative", "neutral"]:
        if "__lemma_word__" not in selected_features_all:
            return True
        return False
    if k not in selected_features_all:
        return True
    return False

if __name__ == '__main__':
    ds_config = DatasetConfig()
    # from SentimentAnalysis.FigurativeTextAnalysis.models.Classifier_ import Classifier
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
    #                    max_vote=False,
    #                    select_percent=60,
    #                    use_idf=False,
    #                    use_tf=True,
    #                    use_hashing=False)
    # cls = Classifier(cls_config)
    # cls.load_model("ALL_FEATURES")
    #
    #
    # t1 = {'__POS_SMILEY__': 'False', '__REFERENCE__': 'True', '__OH_SO__': 'False', 's_word-1': 0.88, 's_word-0': 1.0, u'is': 'VBZ', '__DONT_YOU__': 'False', '__is_metaphor__': True, '__HT_NEG__': 'True', '__AS_GROUND_AS_VEHICLE__': 'False', '__hashtag_lexicon_sum__': 2.0, '__fullstop__': 'False', '__HT_POS__': 'False', u'__lemma_word__perfection': 'somewhat_negative', '__LINK__': 'False', '__HT__': 'False', '__CAPITAL__': 'False', '__exclamation__': 'True', '__LOVE__': 'False', '__multiple_chars_in_a_row__': 'False', 'pos_position_1': 'NN', 'pos_position_0': 'VBZ', '__swn_score__': 0.47, u'__lemma_word__is': 'neutral', 'word-1': u'perfection', 'word-0': u'is', '__NEGATION__': 'False', '__questionmark__': 'False', '__RT__': 'False', '__NEG_SMILEY__': 'False', '__punctuation_percentage__': 19.0, '__LAUGH__': 'False', 't-similarity': 0.0, u'perfection': 'NN'}
    #
    # t1 = _get_clean_feature_dict(t1)
    # cls.set_x_trial([t1])
    # print cls.predict()
    #
    # print cls.get_most_informant_features()

    ds_config.set_defaults()
    ds_config.set_110K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_tf=True,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    # trial.get_stats()
    # # trial.save_results("selected_features_all 40K without 5000 Feedback SVMStandalone  - rerun5")
    #
    # # ds_config.set_defaults()
    # # ds_config.set_110K_feedback()
    # # print ds_config
    # # ds = DatasetHandler(ds_config)
    # # ds.get_data_set()
    # # ds.get_feature_dicts_and_scores()
    # # ds.get_test_and_trial_set()

    ds.add("Feedback", 6000)

    # ds.get_back_up()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_tf=True,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    # trial.get_stats()
    # trial.save_results("selected_features_all 40K + 5000 Feedback SVMStandalone  - rerun5")


    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_tf=True,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_all_correct, ds)
    # trial_config = TrialConfig(process_config, selected_features_all_correct, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # trial.start()
    # trial.get_stats()
    #
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_tf=True,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_all_correct_no_pos, ds)
    # trial_config = TrialConfig(process_config, selected_features_all_correct_no_pos, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # trial.start()
    # trial.get_stats()
    #
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_tf=True,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_all_correct_no_res, ds)
    # trial_config = TrialConfig(process_config, selected_features_all_correct_no_res, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # trial.start()
    # trial.get_stats()
    # trial.save_results("110K without 1000 Feedback SVMStandalone")
    #
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_tf=True,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    # trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # trial.start()
    # trial.get_stats()
    # trial.save_results("selected_features_no_emoticons 110K without 1000 Feedback SVMStandalone  - rerun")