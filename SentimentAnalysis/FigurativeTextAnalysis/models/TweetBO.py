import re
import datetime
from nltk.corpus import wordnet
from SentimentAnalysis.FigurativeTextAnalysis.helpers.pyenchant_spell_checker import EnchantSpellChecker
from SentimentAnalysis.FigurativeTextAnalysis.models.POSTagger import POSTagger
from SentimentAnalysis.FigurativeTextAnalysis.models.SemanticAnalyser import SemanticAnalyzer
from SentimentAnalysis.FigurativeTextAnalysis.models.TextCleaner import TextCleaner
from SentimentAnalysis.FigurativeTextAnalysis.models.TextTagger import TextTagger
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g

__author__ = 'maria'

class Tweet(object):
    """

    """
    def __init__(self, id_, text, train, clean_text=None, tagged_text=None, pos_tagged_text=None, tags=None, initial_score=None,
                 feature_dict=None, words=None, corrected_words=None, metaphor_cls=None, table="TweetTestData", stopwords = None):
        self.id = id_
        self.text = text
        self.original_tweet = None
        self.clean_text = clean_text if clean_text is not None else ""
        self.tagged_text = tagged_text if tagged_text is not None else ""
        self.pos_tagged_text = pos_tagged_text if pos_tagged_text is not None else {}
        self.tags = tags if tags is not None else {}
        self.initial_score = initial_score if initial_score is not None else 0.0
        self.predicted_score = None
        self.spellchecker = EnchantSpellChecker()
        self.spellchecker.dict_exists('en')
        self.sentences = []
        self.words = words if words is not None else []
        self.links = []
        self.corrected_words = corrected_words if corrected_words is not None else []
        self.smileys_per_sentence = []
        self.uppercase_words_per_sentence = []
        self.reference = []
        self.hash_list = []
        self.non_word_chars_removed = []
        self.negations = []
        self.swn_score_dict = {}
        self.metaphor_dict = {}
        self.swn_score = 1.0
        self.similarity = 0.0
        self.feature_dict_original = feature_dict if feature_dict is not None else {}
        self.feature_dict = feature_dict if feature_dict is not None else {}
        self.words_dict = {}
        self.words_to_swn_score_dict = {}
        self.train = train
        self.NOUNS = []
        self.VERBS = []
        self.ADJ = []
        self.ADV = []
        self.table = table
        # ======= Helpers ========== #
        self.tagger = TextTagger()
        self.pos_tagger = POSTagger()
        self.semantic_analyser = SemanticAnalyzer()
        self.metaphor_cls = metaphor_cls
        self.synsets_lengthing = {}
        self.multiple_chars_in_a_row = 0
        self.stopwords = stopwords
        self.cleaner = None
        self.nvar = []
        self.final_hashtag_sentiment = None

    def serialize(self):
        serializable_self = {"id": self.id,
                             "text": self.text,
                             "original_tweet": self.original_tweet,
                             "clean_text": self.clean_text,
                             "tagged_text": self.tagged_text,
                             "pos_tagged_text": self.pos_tagged_text,
                             "tags": self.tags,
                             "initial_score": self.initial_score,
                             "predicted_score": self.predicted_score,
                             "sentences": self.sentences,
                             "words": self.words,
                             "links": self.links,
                             "corrected_words": self.corrected_words,
                             "smileys_per_sentence": self.smileys_per_sentence,
                             "uppercase_words_per_sentence": self.uppercase_words_per_sentence,
                             "reference": self.reference,
                             "hash_list": self.hash_list,
                             "non_word_chars_removed": self.non_word_chars_removed,
                             "negations": self.negations,
                             "swn_score_dict": self.swn_score_dict,
                             "swn_score": self.swn_score,
                             "similarity": self.similarity,
                             "feature_dict": self.feature_dict,
                             "words_dict": self.words_dict,
                             "words_to_swn_score_dict": self.words_to_swn_score_dict,
                             "train": self.train,
                             "synsets_lengthing": self.synsets_lengthing
                             }

        return serializable_self

    def tag(self):
        """
        Feature Extraction process: Retrieve features that will be lost in cleaning process,
        such as CAPITALIZED words, punctuation etc.
        :return:
        """
        self.tagged_text = self.tagger.tag_text(self.text)
        self.tags = self.tagger.get_tags()

    def clean(self):
        """
        Cleaning Process: Remove unneccessary information from tweet_text
        :return:
        """
        # init text cleaner
        self.cleaner = TextCleaner(self, self.stopwords)
        clean_words = self.cleaner.clean_tweet()
        self.clean_text = ' '.join(clean_words)

    def contains_metaphor(self):
        """
        Uses pre-trained classifier with metaphoric and non metaphoric data to predict if tweet is metaphoric or not.
        Boolean True/ False feature
        :return:
        """
        self.metaphor_cls.set_x_trial([self.text])
        self.metaphor_dict["__is_metaphor__"] = self.metaphor_cls.predict()[0]

    def get_multiple_char_words(self):
        """
        note: regex to find consecutive same chars re.findall(r'((\w)\2{2,})', s)
        :return:
        """
        counter = 0
        for pair in self.corrected_words:
            counter += len(pair[0]) - len(pair[1])
        if counter > 1:
            self.multiple_chars_in_a_row = round(float(counter) / float(len(self.text)), 2) * 100
        else:
            self.multiple_chars_in_a_row = "False"

        # if len(self.corrected_words)>1:
        #     self.multiple_chars_in_a_row = "True"
        # else:
        #     self.multiple_chars_in_a_row = "False"

    def get_synonym_lengthing(self):
        pos = ["n", "v", "a", "r"]

        if len(self.nvar) == 0:
            self._get_n_v_a_r()

        for lst in self.nvar:
            for each in lst:
                w_length = float(len(each))
                if w_length > 2:
                    try:
                        word_synsets = []
                        for ss in wordnet.synset("{0}.{1}.01".format(each, pos[self.nvar.index(lst)])):
                            for lemma in ss.lemma_names():
                                # yield (lemma, ss.name())
                                word_synsets.append(lemma)
                        ss_length = float(len(sorted(word_synsets, reverse=True)[0])) if len(word_synsets) > 0 else 0
                        if w_length >= ss_length:
                            self.synsets_lengthing["__synset_length__" + each] = w_length   # w_length/ss_length if ss_length > 0 else 1.0
                        else:
                            self.synsets_lengthing["__synset_length__" + each] = 0
                    except:
                        pass

    def get_final_hashtag_sentiment(self):
        """

        :return:
        """
        if len(self.hash_list[-1]) > 0:
            self.final_hashtag_sentiment = self.tagger.ht_handler.handle(self.hash_list[-1][0])
        else:
            self.final_hashtag_sentiment = 1.0

    def spell_check(self):
        for word in self.words:
            if not self.is_correct(word):
                corrected = self.spellchecker.correct_word(word)
                self.words[self.words.index(word)] = corrected[0].replace("'", "\\'") if len(corrected) > 0 else word

    def pos_tag(self):
        self.pos_tagged_text = self.pos_tagger.pos_stem_lematize(self)
        g.logger.debug("POS-TAGS: %s" % self.pos_tagged_text)
        for word, pos in self.pos_tagged_text.items():
            self.pos_position["pos_position_" + str(self.words.index(word))] = pos
        self._get_n_v_a_r()

    def get_simple_pos_for_swn(self, word):
        if self.pos_tagged_text[word] in g.NOUNS:
            return ' and Category = "n" '
        elif self.pos_tagged_text[word] in g.VERBS:
            return ' and Category = "v" '
        elif self.pos_tagged_text[word] in g.ADVERBS:
            return ' and Category = "r" '
        elif self.pos_tagged_text[word] in g.ADJECTIVES:
            return ' and Category = "a" '
        return ""

    def fix_words(self):
        for word in self.words:
            self.words[self.words.index(word)] = self.pos_tagger.lancaster_stemmer.stem(word)

    def fix_pos_tagging(self):
        for each in self.pos_tagged_text.keys():
            temp = self.pos_tagged_text[each]
            stemmed = self.pos_tagger.lancaster_stemmer.stem(each)
            del self.pos_tagged_text[each]
            self.pos_tagged_text[stemmed] = temp

    def get_swn_score_by_rank(self):
        """
            Take synset ranking into account when calculating score
        """

        final = 0.0
        excluded = 0
        for word in self.words:
            rank = 0.0
            final_ = 0.0
            if len(word) > 1:
                word = word.encode('utf-8').replace("'", "\"")
                try:
                    # first try get equals score with category
                    score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals_rank_test()
                                                        .format(word, self.get_simple_pos_for_swn(word)))
                    g.logger.debug("1: tried equals for {0} and got {1}".format(word, score))

                except:
                    try:
                        g.logger.error(g.sum_query_figurative_scale_equals_rank_test().format(word, self.get_simple_pos_for_swn(word)))
                        # if that failed, try get equals without category
                        score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals_rank_test().format(word, ""))
                        g.logger.debug("2: tried equals for {0} and got {1}".format(word, score))
                    except:
                        g.logger.debug("3: tried equals for {0} and got {1}".format(word, score))

                if len(score) == 0:
                    # if that failed, try get like with category
                    stemmed_word = self.pos_tagger.lancaster_stemmer.stem(word)
                    try:
                        score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like_rank_test()
                                                           .format(stemmed_word, self.get_simple_pos_for_swn(word)))
                        g.logger.debug("tried like for {0} and category got {1}".format(stemmed_word, score))
                    except:
                        if len(score) == 0:
                            # if that failed also, try get like without category
                            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like_rank_test()
                                                               .format(stemmed_word, ""))
                            g.logger.debug("tried like for {0} and got {1}".format(stemmed_word, score))
                    if len(score) == 0:
                        try:
                            # if that failed also, try get like without category
                            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like_rank_test()
                                                               .format(stemmed_word, ""))
                            g.logger.debug("tried like for {0} and got {1}".format(stemmed_word, score))
                        except:
                            g.logger.debug("ALL FAILED: tried like for {0} and got {1}".format(stemmed_word, score))
                # if score[0] is not None and score[0] <= 2.0:
                if len(score) > 0:
                    for each in score:
                        if len(each) == 2:
                            rank_ = int(each[1])
                            final_ += (each[0]/rank_)
                            rank += float(1.0/rank_)

                    if rank > 0:
                        finalfinal = final_/rank
                    else:
                        finalfinal = 0.0
                    g.logger.debug(finalfinal)

                    self.swn_score_dict['s_word-'+str(self.words.index(word))] = round(finalfinal, 2)
                    final += finalfinal
                else:
                    self.swn_score_dict['s_word-'+str(self.words.index(word))] = 1
                    excluded += 1
            else:
                excluded += 1
        if self.words.__len__() > excluded and self.words > 0:
            self.swn_score = round(final/float(self.words.__len__()-excluded), 2)
        else:
            if len(self.words) > 0:
                self.swn_score = round(final/float(self.words.__len__()), 2)
            else:
                self.swn_score = 1.0  # neutral

        if self.tags[g.TAGS.name[g.TAGS.__NEGATION__]] or\
           self.tags[g.TAGS.name[g.TAGS.__AS_GROUND_AS_VEHICLE__]]:
            self.swn_score = self.swn_score - 1.0 if self.swn_score > 1 else self.swn_score * 0.5

        # if self.tags[g.TAGS.name[g.TAGS.POS_SMILEY]] or\
        # self.tags[g.TAGS.name[g.TAGS.HT_POS]]:
        #     self.swn_score = self.swn_score + 1.0 if self.swn_score < 1 else self.swn_score

    def get_swn_score(self):
        '''
            UPDATE `sentifeed`.`sentiwordnet`
            SET
            `ConvertedScale` = (SentimentAssesment*5)/2 - 5
        '''

        final = 0.0
        excluded = 0
        for word in self.words:
            word = word.encode('utf-8')
            if len(word) > 1:
                score = [[10]]
                try:
                    # first try get equals score with category
                    score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals()
                                                        .format(word, self.get_simple_pos_for_swn(word)))
                except:
                    g.logger.error(g.sum_query_figurative_scale_equals().format(word, self.get_simple_pos_for_swn(word)))
                    # if that failed, try get equals without category
                    score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals().format(word, ""))
                    g.logger.debug("tried equals for {0} and got {1}".format(word, score))

                if len(score) > 0 and score[0][0] > 2.0:
                    # if that failed, try get like with category
                    # stemmed_word = self.pos_tagger.lancaster_stemmer.stem(word)
                    stemmed_word = word
                    try:
                        score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like()
                                                           .format(stemmed_word, self.get_simple_pos_for_swn(word)))
                        g.logger.debug("tried like for {0} and category got {1}".format(stemmed_word, score))
                    except:
                        if len(score) > 0 and score[0][0] > 2.0:
                            # if that failed also, try get like without category
                            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like()
                                                               .format(stemmed_word, ""))
                            g.logger.debug("tried like for {0} and got {1}".format(stemmed_word, score))
                if len(score) > 0 and score[0][0] > 2.0:
                            # if that failed also, try get like without category
                            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like()
                                                               .format(stemmed_word, ""))
                            g.logger.debug("tried like for {0} and got {1}".format(stemmed_word, score))
                if len(score) > 0 and score[0][0] is not None and score[0][0] <= 2.0:
                    self.swn_score_dict['s_word-'+str(self.words.index(word))] = round(score[0][0], 2)
                    final += score[0][0]
                else:
                    self.swn_score_dict['s_word-'+str(self.words.index(word))] = 1
                    excluded += 1
            else:
                excluded += 1
        if self.words.__len__() > excluded and self.words > 0:
            self.swn_score = round(final/float(self.words.__len__()-excluded), 2)
        else:
            if len(self.words) > 0:
                self.swn_score = round(final/float(self.words.__len__()), 2)
            else:
                self.swn_score = 1.0  # neutral

        if self.tags[g.TAGS.name[g.TAGS.__NEGATION__]] or\
           self.tags[g.TAGS.name[g.TAGS.__AS_GROUND_AS_VEHICLE__]]:
            self.swn_score = self.swn_score - 1.0 if self.swn_score > 1 else self.swn_score * 0.5
        #
        # if self.tags[g.TAGS.name[g.TAGS.POS_SMILEY]] or\
        # self.tags[g.TAGS.name[g.TAGS.HT_POS]]:
        #     self.swn_score = self.swn_score + 1.0 if self.swn_score < 1 else self.swn_score

    def get_semantic_similarity(self, type_):
        """
        Split tweet words into four major POS categories: NOUNS, VERBS, ADJ, ADV
        and then for each couple, get the semantic text similarity of the type_ that is requested
        :param type_:string that indicates either 'lin' or 'wup' or 'path' or 'resnik'
        :return: decimal score that indicates the average tweet semantic similarity
        """
        similarity = 0.0

        if len(self.nvar) == 0:
            self._get_n_v_a_r()

        length = 0.0
        for each in self.nvar:
            if len(each) > 1:
                for i in range(1, len(each)):
                    temp_similarity = self.semantic_analyser.get_similarity(each[i-1][0],
                                                                            each[i][0],
                                                                            each[i][1],
                                                                            type_)
                    if temp_similarity is not None:
                        similarity += temp_similarity
                        length += 1.0

        if length > 0:
            g.logger.debug("length %s" % length)
            self.similarity = float(similarity/length)
        else:
            g.logger.debug("length 0 similarity %s" % similarity)
            self.similarity = 1.0
        return self.similarity

    def get_words_to_swn_score_dict(self):
        """
        'someword' : 'positive', 'someotherword' : 'negative' ...
        :return:
        """
        for word in self.words:
            if len(word) > 1:
                dct = self.swn_score_dict['s_word-'+str(self.words.index(word))]
                # lemmatized_word = self.pos_tagger.lemmatizer.lemmatize(word, self._get_wn_pos(self.pos_tagged_text[word]))
                self.words_to_swn_score_dict["__lemma_word__" + word] = "positive" if dct > 1.2 else "negative" if dct < 0.2 else "neutral" \
                                                                if 0.95 < dct < 1.05\
                                                                else "somewhat_positive" if 1.2 > dct > 1.05 else "somewhat_negative"

                g.logger.debug("{0}\t{1}".format("||words_to_swn_score_dict||", self.words_to_swn_score_dict["__lemma_word__" + word]))

    def _get_wn_pos(self, pos_string):
        if pos_string in g.NOUNS:
            return wordnet.NOUN
        if pos_string in g.VERBS:
            return wordnet.VERB
        if pos_string in g.ADJECTIVES:
            return wordnet.ADJ
        if pos_string in g.ADVERBS:
            return wordnet.ADV
        return wordnet.NOUN

    def get_words_dict(self):
        for word in self.words:
            self.words_dict['word-'+str(self.words.index(word))] = word
        return self.words_dict

    def gather_dicts(self):
        """
        Get all neccessary information into feature_dict
        :return: None
        """
        self.feature_dict = dict(self.swn_score_dict.items() +
                                 self.metaphor_dict.items() +
                                 self.synsets_lengthing.items() +
                                 self.tags.items() +
                                 self.pos_tagged_text.items() +
                                 self.get_words_dict().items() +
                                 self.words_to_swn_score_dict.items() +
                                 self.pos_position.items())
        self.feature_dict["t-similarity"] = self.similarity
        self.feature_dict["__swn_score__"] = self.swn_score
        self.feature_dict["__multiple_chars_in_a_row__"] = self.multiple_chars_in_a_row
        sorted(self.feature_dict)

    def is_correct(self, word):
        """
        Uses PyEnchant spellchecker to tell if a word contains spelling mistakes or not
        :param word: string: word to be examined for errors
        :return:True if word contains errors False otherwise
        """
        return self.spellchecker.is_correct(word)

    def insert(self):
        q = g.insert_in_tweet_bo.format(self.id,
                                str(self.text).replace("\"", '\\"'),
                                str(self.initial_score).replace("\"", '\\"'),
                                str(self.clean_text).replace("\"", '\\"'),
                                str(self.tagged_text).replace("\"", '\\"'),
                                str(self.pos_tagged_text).replace("\"", '\\"'),
                                str(self.tags).replace("\"", '\\"'),
                                str(self.sentences).replace("\"", '\\"'),
                                str(self.words).replace("\"", '\\"'),
                                str(self.links).replace("\"", '\\"'),
                                str(self.corrected_words).replace("\"", '\\"'),
                                str(self.smileys_per_sentence).replace("\"", '\\"'),
                                str(self.uppercase_words_per_sentence).replace("\"", '\\"'),
                                str(self.reference).replace("\"", '\\"'),
                                str(self.hash_list).replace("\"", '\\"'),
                                str(self.non_word_chars_removed).replace("\"", '\\"'),
                                str(self.negations).replace("\"", '\\"'),
                                str(self.swn_score_dict).replace("\"", '\\"'),
                                self.swn_score,
                                str(self.feature_dict).replace("\"", '\\"'), 0,
                                self.train)
        g.logger.debug(q)
        try:
            g.mysql_conn.update(q)
        except:
            g.logger.error("Q:"+q)

    def update(self):
        qi = g.update_tweet_bo().format(
                        str(self.clean_text).replace("\"", "'"),
                        str(self.tagged_text).replace("\"", "'"),
                        str(self.pos_tagged_text).replace("\"", "'"),
                        str(self.tags).replace("\"", "'"),
                        str(self.initial_score).replace("\"", "'"),
                        str(self.sentences).replace("\"", "'"),
                        str(self.words).replace("\"", "'"),
                        str(self.links).replace("\"", "'"),
                        str(self.corrected_words).replace("\"", "'"),
                        str(self.smileys_per_sentence).replace("\"", "'"),
                        str(self.uppercase_words_per_sentence).replace("\"", "'"),
                        str(self.reference).replace("\"", "'"),
                        str(self.hash_list).replace("\"", "'"),
                        str(self.non_word_chars_removed).replace("\"", "'"),
                        str(self.negations).replace("\"", "'"),
                        str(self.swn_score_dict).replace("\"", "'"),
                        self.swn_score,
                        str(self.feature_dict).replace("\"", "'"),
                        0,
                        self.train,
                        datetime.datetime.now(),
                        str(self.words_to_swn_score_dict).replace("\"", "'"),
                        self.id, self.table)
        try:
            g.mysql_conn.execute_query(qi)
            g.logger.debug("Qi:"+qi)
        except:
            g.logger.error("error in Qi:"+qi)

    def _get_n_v_a_r(self):
        for k, v in self.pos_tagged_text.items():
            if v in g.NOUNS:
                self.NOUNS.append(v)
            elif v in g.VERBS:
                self.VERBS.append(v)
            elif v in g.ADJECTIVES:
                self.ADJ.append(v)
            elif v in g.ADVERBS:
                self.ADV.append(v)
        self.nvar = [self.NOUNS, self.VERBS, self.ADJ, self.ADV]
