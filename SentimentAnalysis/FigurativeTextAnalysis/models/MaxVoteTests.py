from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from SentimentAnalysis.FigurativeTextAnalysis.models.Config import DatasetConfig, ClsConfig, ProcessConfig, TrialConfig
from SentimentAnalysis.FigurativeTextAnalysis.models.DatasetHandler import DatasetHandler
from SentimentAnalysis.FigurativeTextAnalysis.models.Trial_ import Trial


selected_features_all =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_no_emoticons =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    # '__POS_SMILEY__',           # * // << +
                    # '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_only_position_with_emoticons =[
                    # 'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

selected_features_only_position_no_emoticons =[
                    # 'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_',
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    # '__POS_SMILEY__',           # * // << +
                    # '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]



def all_50k():
    ds_config = DatasetConfig()
    ds_config.set_50K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=False 50k Dict - MAXVOTE VS SVM")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=True 50k Dict - MAXVOTE VS SVM")

    ds = None


def all_70k():
    ds_config = DatasetConfig()
    ds_config.set_70K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=False 70k Dict - MAXVOTE VS SVM")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=True 70K Dict - MAXVOTE VS SVM")

    ds = None


def all_90k():
    ds_config = DatasetConfig()
    ds_config.set_90K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=False 90k Dict - MAXVOTE VS SVM")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=True 90K Dict - MAXVOTE VS SVM")

    ds = None


def all_110k():
    ds_config = DatasetConfig()
    ds_config.set_110K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()


    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=False 110k Dict - MAXVOTE VS SVM")

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=True,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    trial.start()
    trial.get_stats()
    trial.save_results("selected_features_no_emoticons max_vote=True 110K Dict - MAXVOTE VS SVM")

    ds = None

if __name__ == '__main__':
    all_50k()
    all_70k()
    all_90k()
    all_110k()