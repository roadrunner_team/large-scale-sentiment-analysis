from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from SentimentAnalysis.FigurativeTextAnalysis.models.Config import DatasetConfig, ClsConfig, ProcessConfig, TrialConfig
from SentimentAnalysis.FigurativeTextAnalysis.models.DatasetHandler import DatasetHandler
from SentimentAnalysis.FigurativeTextAnalysis.models.Trial_ import Trial

selected_features_all =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_'
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                ]

selected_features_no_emoticons =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_'
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    # '__POS_SMILEY__',           # * // << +
                    # '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]


__author__ = 'mariakaranasou'


def feedback():
    # ds_config = DatasetConfig()
    # ds_config.feedback = 1500
    # print ds_config
    # ds = DatasetHandler(ds_config)
    # ds.get_data_set()
    # ds.get_feature_dicts_and_scores()
    # # ds.get_test_and_trial_set()
    # cross_validate("FEEDBACK ONLY ", ds)
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_tf=True,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    # trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # trial.start()
    # trial.get_stats()
    # trial.save_results("Feedback ONLY SVMStandalone")
    #
    #
    # cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
    #                        max_vote=False,
    #                        select_percent=60,
    #                        use_idf=False,
    #                        use_tf=True,
    #                        use_hashing=False)
    # process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    # trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
    #
    # trial = Trial(trial_config, 0)
    # trial.start()
    # trial.get_stats()
    # trial.save_results("Feedback selected_features_no_emoticons ONLY Feedback SVMStandalone")

    ds_config = DatasetConfig()
    ds_config.set_defaults()
    ds_config.set_50K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    cross_validate("50K NO Feedback", ds)


    ds_config.set_defaults()
    ds_config.set_70K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    cross_validate("70K NO Feedback", ds)

    # 90K
    ds_config.set_defaults()
    ds_config.set_90K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    cross_validate("90K NO Feedback", ds)

    # 110K
    ds_config.set_defaults()
    ds_config.set_110K()
    print ds_config
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    cross_validate("110K NO Feedback", ds)


def cross_validate(title, ds):
    ds.get_clean_total_data_list()

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)

    clean_data = [trial.tweet_processor._get_clean_feature_dict(x) for x in  ds.total_data]

    scores = trial.tweet_processor.classifier.predict_cross_validate(clean_data, ds.total_scores)
    trial.save_cross_validate(title + " SVMStandalone Dict", scores)
    print title + " SVMStandalone Dict", scores

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(clean_data, ds.total_scores)
    trial.save_cross_validate(title + " NBayes Dict", scores)
    print title + " NBayes Dict", scores

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
    trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(clean_data, ds.total_scores)
    trial.save_cross_validate(title + " SGD Dict", scores)
    print title + " SGD Dict", scores

    ###########################################################################################
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)

    clean_data = [trial.tweet_processor._get_clean_feature_dict(x) for x in  ds.total_data]

    scores = trial.tweet_processor.classifier.predict_cross_validate(clean_data, ds.total_scores)
    trial.save_cross_validate(title + " selected_features_no_emoticons SVMStandalone Dict", scores)
    print title + " selected_features_no_emoticons SVMStandalone Dict", scores

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.NBayes, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(clean_data, ds.total_scores)
    trial.save_cross_validate(title + " selected_features_no_emoticons NBayes Dict", scores)
    print title + " selected_features_no_emoticons NBayes Dict", scores

    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
    trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")

    trial = Trial(trial_config, 0)
    scores = trial.tweet_processor.classifier.predict_cross_validate(clean_data, ds.total_scores)
    trial.save_cross_validate(title + " SGD Dict", scores)
    print title + " selected_features_no_emoticons SGD Dict", scores


feedback()


# --------------------------------------------------------------------------------------------------------------- #
# ds_config.set_defaults()
# ds_config.set_50K_feedback()
# print ds_config
# ds = DatasetHandler(ds_config)
# ds.get_data_set()
# ds.get_feature_dicts_and_scores()
# ds.get_test_and_trial_set()
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
# trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("50K with 1000 Feedback SVMStandalone")
#
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
# trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("selected_features_no_emoticons 50K with 1000 Feedback SVMStandalone - rerun")
#
# ds_config.set_defaults()
# ds_config.set_50K()
# print ds_config
# ds = DatasetHandler(ds_config)
# ds.get_data_set()
# ds.get_feature_dicts_and_scores()
# ds.get_test_and_trial_set()
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
# trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("50K without 1000 Feedback SVMStandalone")
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
# trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("selected_features_no_emoticons 50K without 1000 Feedback SVMStandalone  - rerun")
#
# # 70K
# ds_config.set_defaults()
# ds_config.set_70K_feedback()
# print ds_config
# ds = DatasetHandler(ds_config)
# ds.get_data_set()
# ds.get_feature_dicts_and_scores()
# ds.get_test_and_trial_set()
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
# trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("70K with 1000 Feedback SVMStandalone")
#
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
# trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("selected_features_no_emoticons 70K with 1000 Feedback SVMStandalone - rerun")
#
# ds_config.set_defaults()
# ds_config.set_70K()
# print ds_config
# ds = DatasetHandler(ds_config)
# ds.get_data_set()
# ds.get_feature_dicts_and_scores()
# ds.get_test_and_trial_set()
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
# trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("70K without 1000 Feedback SVMStandalone - rerun")
#
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
# trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("selected_features_no_emoticons 70K without 1000 Feedback SVMStandalone")
#
# # ================================================================================================
#
# # 90K
# ds_config.set_defaults()
# ds_config.set_90K_feedback()
# print ds_config
# ds = DatasetHandler(ds_config)
# ds.get_data_set()
# ds.get_feature_dicts_and_scores()
# ds.get_test_and_trial_set()
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
# trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("90K with 1000 Feedback SVMStandalone  - rerun")
#
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
# trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("selected_features_no_emoticons 90K with 1000 Feedback SVMStandalone")
#
# ds_config.set_defaults()
# ds_config.set_90K()
# print ds_config
# ds = DatasetHandler(ds_config)
# ds.get_data_set()
# ds.get_feature_dicts_and_scores()
# ds.get_test_and_trial_set()
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
# trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("90K without 1000 Feedback SVMStandalone  - rerun")
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
# trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("selected_features_no_emoticons 90K without 1000 Feedback SVMStandalone  - rerun")
#
# # 110K
# ds_config.set_defaults()
# ds_config.set_110K_feedback()
# print ds_config
# ds = DatasetHandler(ds_config)
# ds.get_data_set()
# ds.get_feature_dicts_and_scores()
# ds.get_test_and_trial_set()
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
# trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("110K with 1000 Feedback SVMStandalone")
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
# trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("selected_features_no_emoticons 110K with 1000 Feedback SVMStandalone  - rerun")
#
#
# ds_config.set_defaults()
# ds_config.set_110K()
# print ds_config
# ds = DatasetHandler(ds_config)
# ds.get_data_set()
# ds.get_feature_dicts_and_scores()
# ds.get_test_and_trial_set()
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_all, ds)
# trial_config = TrialConfig(process_config, selected_features_all, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("110K without 1000 Feedback SVMStandalone")
#
# cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
#                        max_vote=False,
#                        select_percent=60,
#                        use_idf=False,
#                        use_tf=True,
#                        use_hashing=False)
# process_config = ProcessConfig(cls_config, False, selected_features_no_emoticons, ds)
# trial_config = TrialConfig(process_config, selected_features_no_emoticons, "Tweet_Total")
#
# trial = Trial(trial_config, 0)
# trial.start()
# trial.get_stats()
# trial.save_results("selected_features_no_emoticons 110K without 1000 Feedback SVMStandalone  - rerun")