import ast
import copy
import string
import traceback

from SentimentAnalysis.FigurativeTextAnalysis.models.Classifier import Classifier
from SentimentAnalysis.FigurativeTextAnalysis.models.TweetBO import Tweet
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from SentimentAnalysis.FigurativeTextAnalysis.helpers.scale_converter import ScaleConverter

# Metrics
# http://streamhacker.com/2010/05/17/text-classification-sentiment-analysis-precision-recall/
# http://scikit-learn.org/stable/modules/classes.html#classification-metrics

__author__ = 'maria'

figurative_data_train_file ='../data/figurative_lang_data/newid_weightedtweetdata_retrieved.csv'
figurative_data_test_file = '../data/figurative_lang_data/task-11-trial_data.csv'


class TweetProcessor(object):
    """
    Handles Tweet preprocessing and classification.

    @train_file: csv with train tweets (~8000) as downloaded by script provided by SemEval
    @test_file: csv with test tweets (~1000) as downloaded by script provided by SemEval
    @selected_features: list of string representing the features that will be included in feature dictionaries
                        and will be used for classification
    @classifier_type: enum: the type of classifier to be used (see globals for possible values)
    @vectorizer_type: enum: the type of vectorizer to be used with the classifier (see globals for possible values)
    @discretization: float: this number is the step with which continuous labels will be partitioned to discrete labels.
                    e.g. labels=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5] with discretization 0.5 the final label range
                    will be: labels=[-5to-4.5, -4.5to-4.0, -4.0to-3.5, .... 3.5to4.0, 4.0to4.5, 4.5to5]
    """
    def __init__(self, train_file, test_file, selected_features, classifier_type, vectorizer_type, discretization=0.5,
                 test_or_final_ds="Final", use_tf=True, use_cosine=False, use_k_best=False, preprocess=False):
        self.preprocess = preprocess
        self.discrete_labels = {}
        self.train_file = train_file
        self.test_file = test_file
        self.selected_features = selected_features
        self.classifier_type = classifier_type
        self.test_or_final_ds = test_or_final_ds
        self.classifier = Classifier(cls_type=classifier_type,
                                     vectorizer_type=vectorizer_type,
                                     use_pairwise_cosine=use_cosine,
                                     use_tf_transformer=use_tf,
                                     use_k_best=use_k_best)
        self.train_set = []
        self.test_set = []
        self.feature_list_train = []
        self.score_list_train = []
        self.feature_list_test = []
        self.score_list_test = []
        self.feature_score_dict = {}
        self.scale_converter = ScaleConverter(old_max=5.0, old_min=-5.0, new_max=2.0, new_min=0.0)
        self.discretization = discretization
        self.discrete_labels = {}
        self.positive_range = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
        self.negative_range = [0.0, -1.0, -2.0, -3.0, -4.0, 5.0]
        self.initial_range = [-5.0, -4.0, -3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
        self.positive_labels = {}
        self.negative_labels = {}
        self._prepare_labels()
        self.predictions = []
        self.metaphor_cls = None
        self.stopwords = g.mysql_conn.execute_query(g.select_from_stop_words())
        self.sentiment_discretization_values = ["positive", "negative", "somewhat_positive", "somewhat_negative", "neutral"]
        self.semantic_similarity = ['__path__', '__lin__', '__wup__', '__res__']
        self.tags = ['__OH_SO__',                  # * // <<   +
                     '__DONT_YOU__',               # * // <<   +
                     '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                     '__CAPITAL__',                # * // <<   +
                     '__HT__',                     # * <<      +
                     '__HT_POS__',               # * // <<   +
                     '__HT_NEG__',               # * // <<   +
                     '__LINK__',               # //
                     '__POS_SMILEY__',           # * // << +
                     '__NEG_SMILEY__',           # * // << +
                     '__NEGATION__',             # * // << +
                     '__REFERENCE__',            # * // << +
                     '__questionmark__',         # * // << +
                     '__exclamation__',          # * // << +
                     '__fullstop__']

    def _get_metaphor_classifier(self):
        self.metaphor_cls = Classifier(g.CLASSIFIER_TYPE.SVMStandalone, g.VECTORIZER.Count, use_k_best=False)
        q_metaphor = "SELECT tweet_text FROM SentiFeed.Metaphors;"
        q_others_positive = "SELECT TweetText FROM SentiFeed.TwitterCorpusNoEmoticons where EmotionT = 'positive' limit 3225;"
        q_others_negative = "SELECT TweetText FROM SentiFeed.TwitterCorpusNoEmoticons where EmotionT = 'negative'  limit 3225;"

        met_data = [x[0] for x in g.mysql_conn.execute_query(q_metaphor)]
        oth_data_positive = [x[0] for x in g.mysql_conn.execute_query(q_others_positive)]
        oth_data_negative = [x[0] for x in g.mysql_conn.execute_query(q_others_negative)]

        t_met_data = [[x, True] for x in met_data]
        t_oth_pos_data = [[x, False] for x in oth_data_positive]
        t_oth_neg_data = [[x, False] for x in oth_data_negative]

        final_data = t_met_data + t_oth_neg_data + t_oth_pos_data

        self.metaphor_cls .set_y_train([x[1] for x in final_data])
        self.metaphor_cls .set_x_train([x[0] for x in final_data])
        self.metaphor_cls .train()

    def get_train_set_from_db(self):

        if self.test_or_final_ds == "Final":
            q = "select id, text, initial_score, feature_dict, train " \
                "from SentiFeed.TweetTestData;"
            table = "TweetTestData"

        else:
            q = "select id, text, initial_score, feature_dict, train " \
                "from SentiFeed.TweetTestData where train=1;"
            table = "TweetTestData"

        train_data = g.mysql_conn.execute_query(q)

        for row in train_data:

            try:
                # score = row[2]
                score = row[2]\
                        if self.preprocess\
                        else \
                        0 if row[2] == 0 \
                        else 1 if row[2] > 0  \
                        else -1
                feature_dict = ast.literal_eval(row[3]) if not self.preprocess else {}

                tweet = Tweet(row[0],                                    # id
                              row[1],                                    # text
                              initial_score=score,
                              feature_dict=feature_dict,  # feature dict --> processing has already taken place
                              train=row[4],                             # it is a train set
                              metaphor_cls=self.metaphor_cls,
                              table=table,
                              stopwords=self.stopwords
                              )
                self.train_set.append(tweet)
            except:
                g.logger.error("problem with test tweet:\t{0}".format(row[0]))
                pass

    def get_test_set_from_db(self):
        q = ""
        if self.test_or_final_ds == "Final":
            q = "select id, text, initial_score, feature_dict, train " \
                "from SentiFeed.TweetFinalTestData;"
            table = "TweetFinalTestData"
        else:
            q = "select id, text, initial_score, feature_dict, train " \
                 "from SentiFeed.TweetTestData where train=0;"
            table = "TweetTestData"

        test_data = g.mysql_conn.execute_query(q)
        print len(test_data)
        for row in test_data:
            try:
                # score = row[2]
                score = row[2]\
                        if self.preprocess\
                        else \
                        0 if row[2] == 0 \
                        else 1 if row[2] > 0  \
                        else -1

                feature_dict = ast.literal_eval(row[3]) if not self.preprocess else {}

                tweet = Tweet(row[0],                              # id
                              row[1],                              # text
                              initial_score=score,                 # handcoded score
                              feature_dict=feature_dict,           # feature dict --> processing has already taken place
                              train=row[4],                        # it is a trial/ test set
                              metaphor_cls=self.metaphor_cls,
                              table=table,
                              stopwords=self.stopwords
                              )
                self.test_set.append(tweet)
            except:
                g.logger.error("problem with train tweet:\t{0}".format(row[0]))
                pass

    def process_tweet_set(self, list_of_tweets):
        i = 0
        for tweet in list_of_tweets:
            g.logger.debug("#########################################################################################")
            g.logger.debug("{0}\t{1}".format(list_of_tweets.index(tweet), tweet.text))
            tweet.tag()
            tweet.clean()
            # tweet.spellcheck()
            # tweet.fix_words()
            tweet.pos_tag()
            # tweet.get_swn_score()
            tweet.get_swn_score_by_rank()
            tweet.contains_metaphor()
            tweet.get_synonym_lengthing()
            tweet.get_multiple_char_words()
            tweet.get_words_to_swn_score_dict()
            # tweet.fix_pos_tagging()
            tweet.gather_dicts()
            tweet.feature_dict['__res__'] = round(tweet.get_semantic_similarity('res'), 1)
            tweet.feature_dict['__path__'] = round(tweet.get_semantic_similarity('path'), 1)
            tweet.feature_dict['__lin__'] = round(tweet.get_semantic_similarity('lin'), 1)
            tweet.feature_dict['__wup__'] = round(tweet.get_semantic_similarity('wup'), 1)
            tweet.feature_dict['__FINAL_HASHTAG__'] = tweet.get_final_hashtag_sentiment()
            tweet.update()
            i += 1
            if i % 100 == 0:
                print(i)

    def excluded(self, k, dicta_k_value):
        if 's_word' in k:                                   # if k is a sentiwordnet feature
            if 's_word' not in self.selected_features:      # if swn is not wanted
                return True                                 # remove
            return False
        if '__contains__' in k:                                # if k is a sentiwordnet feature
            if '__contains__' not in self.selected_features:   # if swn is not wanted
                return True                                 # remove
            return False
        if '__synset_length__' in k:                                # if k is a sentiwordnet feature
            if '__synset_length__' not in self.selected_features:   # if swn is not wanted
                return True                                     # remove
            return False
        if '__swn_score__' == k:                                # if k is sentiwordnet total score
            if '__swn_score__' not in self.selected_features:   # if swn is not wanted
                return True                                 # remove
            return False
        if dicta_k_value in g.VERBS or dicta_k_value in g.ADJECTIVES or dicta_k_value in g.ADVERBS or dicta_k_value in g.NOUNS:
            if 'postags' not in self.selected_features:
                return True
            return False
        if 'word-' in dicta_k_value:
            if 'words' not in self.selected_features or 'word' not in self.selected_features:
                return True
            return False
        if 't-similarity' in k:
            if 't_similarity' not in self.selected_features:
                return True
            return False
        if '__res__' == k:
            if '__res__' not in self.selected_features:
                return True
            return False
        if '__lin__' == k:
            if '__lin__' not in self.selected_features:
                return True
            return False
        if '__path__' == k:
            if '__path__' not in self.selected_features:
                return True
            return False
        if '__wup__' == k:
            if '__wup__' not in self.selected_features:
                return True
            return False
        if dicta_k_value in self.sentiment_discretization_values:
            if "__lemma_word__" not in self.selected_features:
                return True
            return False
        if k not in self.selected_features:
            return True
        return False

    def get_feature_trainset(self):
        for tweet in self.train_set:
            try:
                dicta = copy.deepcopy(tweet.feature_dict)
                self._get_clean_feature_dict(dicta)
                sorted(dicta)
                self.feature_list_train.append(dicta)
                self.score_list_train.append(self._get_porper_score(tweet.initial_score))

            except Exception:
                g.logger.error("PROBLEM WITH:\t%s" % tweet.id, exc_info=True)

    def get_feature_testset(self):
        for tweet in self.test_set:
            try:
                dicta = copy.deepcopy(tweet.feature_dict)
                self._get_clean_feature_dict(dicta)
                # sorted(dicta)
                self.feature_list_test.append(dicta)
                self.score_list_test.append(self._get_porper_score(tweet.initial_score))
            except:
                g.logger.error("PROBLEM WITH:\t%s", tweet.id, exc_info=True)

    def classify(self, cross_validate=False):
        self.get_feature_trainset()
        self.get_feature_testset()

        if cross_validate:
            data = self.feature_list_train + self.feature_list_test
            scores = self.score_list_train + self.score_list_test

            print scores
            g.logger.debug(str(self.classifier.cross_val_predict(data, scores)))
            final = self.classifier.predict_cross_validate(data, scores)
            self.classifier.print_cross_validation_accuracy(final)
        else:
            self.classifier.set_y_train(self.score_list_train)
            self.classifier.set_x_train(self.feature_list_train)
            self.classifier.set_x_trial(self.feature_list_test)
            self.classifier.set_y_trial(self.score_list_test)
            self.classifier.train()
            self.predictions = self.classifier.predict()

        self.classifier.save_model_in_db(self.test_or_final_ds, selected_features=self.selected_features)
        match = 0

        for i in range(0, len(self.predictions)):
            # calculate accuracy
            if self.predictions[i] == self.score_list_test[i]:
                match += 1
            g.logger.debug("id\t{0}\ttext:\t{1}\tpredicted:\t{2}\tactual:\t{3}".format(self.test_set[i].id,
                                                                                        self.test_set[i].text,
                                                                                        self.predictions[i],
                                                                                        self.score_list_test[i]))

        results = "{0}/{1} PERCENTAGE: {2}% \n".format(match,
                                                       len(self.score_list_test),
                                                       (float(match)/float(len(self.score_list_test)))*100)
        g.logger.info(results)
        print results
        try:
            metrics = self.classifier.get_metrics()
            g.logger.info(metrics)
            print metrics
        except:
            pass

    def _insert(self, tweet):
        tweet._insert()

    @staticmethod
    def remove_non_ascii_chars(text):
        """
        to solve problem with extra / weird characters when getting data from database
        :return:
        """
        return str(filter(lambda x: x in string.printable, text))

    def get_modified_label_for(self, initial_score):
        for key, value in self.discrete_labels.items():

            if initial_score == 0.0:
                    return "zero"

            elif (value[1] < 0.0 or value[0] <= 0.0) and initial_score < 0.0:
                if value[0] <= initial_score < value[1]:
                    return str(key)

            elif initial_score > 0.0 and (value[0] <= initial_score <= value[1]):
                    return str(key)

    def _prepare_labels(self):
        labels = []
        d = (1.0/self.discretization)

        for each in self.initial_range:                                  # each is 0.0, 1.0 etc
            for i in range(0, int(d)):                                   # 0, 0.0*0.2 == 0.0 || 1.0/0.2 == 5.0
                labels.append(round(self.discretization*i+each, 1))      # 0.0, 0.2*1, 0.2*2 etc

        for i in range(1, len(labels)-1):
            self.discrete_labels['{0}To{1}'.format(labels[i-1], labels[i])] = [labels[i-1], labels[i]]
        self.discrete_labels['zero'] = [0.0, 0.0]
        print "Label range is: ", self.discrete_labels.keys()

    def _get_porper_score(self, initial_score):
        if self.discretization < 1.0:
            self.get_modified_label_for(round(initial_score, 1))
        if (self.discretization == 1.0
         and self.classifier_type != g.CLASSIFIER_TYPE.SVR):
            return round(initial_score, 0)
        if self.classifier_type >= g.CLASSIFIER_TYPE.SVR and\
            self.classifier_type is not g.CLASSIFIER_TYPE.Perceptron:
            return float(round(initial_score, 0))

        return round(initial_score, 0)

    def _get_clean_feature_dict(self, dicta):

        for k in dicta.keys():
            if not self.excluded(k, str(dicta[k])):
                if 's_word-' in k or 'swn_score' in k:
                    dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral" \
                                if 0.95 <= dicta[k] <= 1.05\
                                else "somewhat_positive" if 1.2 >= dicta[k] > 1.05 \
                                else "somewhat_negative"
                if dicta[k] in g.VERBS:
                    dicta[k] = "VB"
                elif dicta[k] in g.NOUNS:
                    dicta[k] = "NN"
                elif dicta[k] in g.ADJECTIVES:
                    dicta[k] = "ADJ"
                elif dicta[k] in g.ADVERBS:
                    dicta[k] = "RB"
                if 'contains_' in k:
                    dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral"\
                                if 0.95 < dicta[k] < 1.05\
                                else "somewhat_positive" if 1.2 > dicta[k] > 1.05 else "somewhat_negative"

                if k == "__punctuation_percentage__":
                    dicta[k] = 1 if dicta[k] > 20 else 0

                if k in self.tags:
                    dicta[k] = 0 if dicta[k] == "False" else 1


            else:
                del dicta[k]