import ast
import copy
import string
import traceback
import sklearn
from sklearn.metrics import classification_report, confusion_matrix

from SentimentAnalysis.FigurativeTextAnalysis.models.Classifier_ import Classifier, MaxVoteClassifier
from SentimentAnalysis.FigurativeTextAnalysis.models.Config import ClsConfig
from SentimentAnalysis.FigurativeTextAnalysis.models.TweetBO import Tweet
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g

# Metrics
# http://streamhacker.com/2010/05/17/text-classification-sentiment-analysis-precision-recall/
# http://scikit-learn.org/stable/modules/classes.html#classification-metrics

__author__ = 'maria'

figurative_data_train_file ='../data/figurative_lang_data/newid_weightedtweetdata_retrieved.csv'
figurative_data_test_file = '../data/figurative_lang_data/task-11-trial_data.csv'


class TweetProcessor(object):
    """
    Handles Tweet preprocessing and classification.

    @train_file: csv with train tweets (~8000) as downloaded by script provided by SemEval
    @test_file: csv with test tweets (~1000) as downloaded by script provided by SemEval
    @selected_features: list of string representing the features that will be included in feature dictionaries
                        and will be used for classification
    @classifier_type: enum: the type of classifier to be used (see globals for possible values)
    @vectorizer_type: enum: the type of vectorizer to be used with the classifier (see globals for possible values)
    @discretization: float: this number is the step with which continuous labels will be partitioned to discrete labels.
                    e.g. labels=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5] with discretization 0.5 the final label range
                    will be: labels=[-5to-4.5, -4.5to-4.0, -4.0to-3.5, .... 3.5to4.0, 4.0to4.5, 4.5to5]
    """
    def __init__(self, proccessor_config, use=0):
        self.use = use
        self.proccessor_config = proccessor_config
        if self.proccessor_config is None:
            raise Exception("No processing configuration found!")
        self.dataset_handler = self.proccessor_config.dataset_handler
        self.selected_features = self.proccessor_config.selected_features
        self.classifier = Classifier(proccessor_config.cls_config)
        self.train_set = []
        self.test_set = []
        self.feature_list_train = []
        self.score_list_train = []
        self.feature_list_test = []
        self.score_list_test = []
        self.predictions = []
        self.metaphor_cls = None
        self.stopwords = g.mysql_conn.execute_query(g.select_from_stop_words())
        self.sentiment_discretization_values = ["positive", "negative", "somewhat_positive", "somewhat_negative", "neutral"]
        self.semantic_similarity = ['__path__', '__lin__', '__wup__', '__res__']
        self.tags = ['__OH_SO__',
                     '__DONT_YOU__',
                     '__AS_GROUND_AS_VEHICLE__',
                     '__CAPITAL__',
                     '__HT__',
                     '__HT_POS__',
                     '__HT_NEG__',
                     '__LINK__',
                     '__POS_SMILEY__',
                     '__NEG_SMILEY__',
                     '__NEGATION__',
                     '__REFERENCE__',
                     '__questionmark__',
                     '__exclamation__',
                     '__fullstop__']

    def _get_metaphor_classifier(self):
        cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count,
                           max_vote=False,
                           select_percent=0,
                           use_idf=False,
                           use_hashing=False)
        self.metaphor_cls = Classifier(cls_config)

        try:
            self.metaphor_cls.load_model("Metaphor")
        except:
            q_metaphor = "SELECT tweet_text FROM SentiFeed.Metaphors;"
            q_others_positive = "SELECT text FROM SentiFeed.tweet_total where source='1.6M' and initial_score > 0  limit 3225 OFFSET 30000;"
            q_others_negative = "SELECT text FROM SentiFeed.tweet_total where source='1.6M' and initial_score < 0  limit 3225 OFFSET 30000;"

            met_data = [x[0] for x in g.mysql_conn.execute_query(q_metaphor)]
            oth_data_positive = [x[0] for x in g.mysql_conn.execute_query(q_others_positive)]
            oth_data_negative = [x[0] for x in g.mysql_conn.execute_query(q_others_negative)]

            t_met_data = [[x, True] for x in met_data]
            t_oth_pos_data = [[x, False] for x in oth_data_positive]
            t_oth_neg_data = [[x, False] for x in oth_data_negative]

            final_data = t_met_data + t_oth_neg_data + t_oth_pos_data

            self.metaphor_cls .set_y_train([x[1] for x in final_data])
            self.metaphor_cls .set_x_train([x[0] for x in final_data])
            self.metaphor_cls .train()
            self.metaphor_cls.save_model("Metaphor")
            pass

    def process(self):
        # self.dataset_handler.get_data_set()
        self._create_tweet_list()

        if self.proccessor_config.preprocess:
            self._get_metaphor_classifier()
            self._process_tweet_set()
        else:
            self._classify()

    def _create_tweet_list(self):
        for row in self.dataset_handler.data:

            try:
                score = row[2]
                feature_dict = ast.literal_eval(row[3]) if not self.proccessor_config.preprocess else {}

                tweet = Tweet(row[0],                                    # id
                              row[1],                                    # text
                              initial_score=score,
                              feature_dict=feature_dict,  # feature dict --> processing has already taken place
                              train=row[4],                             # it is a train set
                              metaphor_cls=self.metaphor_cls,
                              table=self.proccessor_config.table,
                              stopwords=self.stopwords
                              )
                self.train_set.append(tweet)
            except:
                g.logger.error("problem with test tweet:\t{0}".format(row[0]))
                g.logger.error(traceback.format_exc())
                pass

    def _process_tweet_set(self, list_of_tweets):
        i = 0
        for tweet in list_of_tweets:
            g.logger.debug("#########################################################################################")
            g.logger.debug("{0}\t{1}".format(list_of_tweets.index(tweet), tweet.text))
            tweet.tag()
            tweet.clean()
            # tweet.spellcheck()
            # tweet.fix_words()
            tweet.pos_tag()
            # tweet.get_swn_score()
            tweet.get_swn_score_by_rank()
            tweet.contains_metaphor()
            tweet.get_synonym_lengthing()
            tweet.get_multiple_char_words()
            tweet.get_words_to_swn_score_dict()
            # tweet.fix_pos_tagging()
            tweet.gather_dicts()
            tweet.feature_dict['__res__'] = round(tweet.get_semantic_similarity('res'), 1)
            tweet.feature_dict['__path__'] = round(tweet.get_semantic_similarity('path'), 1)
            tweet.feature_dict['__lin__'] = round(tweet.get_semantic_similarity('lin'), 1)
            tweet.feature_dict['__wup__'] = round(tweet.get_semantic_similarity('wup'), 1)
            tweet.update()
            i += 1
            if i % 100 == 0:
                print(i)

    def get_feature_trainset(self):
        for i in xrange(0, len(self.dataset_handler.test_set_data)):
            try:
                dicta = copy.deepcopy(self.dataset_handler.test_set_data[i])
                if not self.proccessor_config.cls_config.vec_type == g.VECTORIZER.Count:
                    self._get_clean_feature_dict(dicta)
                self.feature_list_train.append(dicta)
                self.score_list_train.append(self.dataset_handler.test_set_scores[i])

            except Exception:
                g.logger.error(traceback.format_exc())
                g.logger.error("PROBLEM WITH TRAINSET TWEET:\t%s" % self.dataset_handler.test_set_data[i], exc_info=True)

    def get_feature_testset(self):
        for i in xrange(0, len(self.dataset_handler.trial_set_data)):
            try:
                dicta = copy.deepcopy(self.dataset_handler.trial_set_data[i])
                if not self.proccessor_config.cls_config.vec_type == g.VECTORIZER.Count:
                    self._get_clean_feature_dict(dicta)
                self.feature_list_test.append(dicta)
                self.score_list_test.append(self.dataset_handler.trial_set_scores[i])
            except:
                g.logger.error(traceback.format_exc())
                g.logger.error("PROBLEM WITH TEST SET TWEET:\t%s", self.dataset_handler.trial_set_data[i], exc_info=True)

    def _classify(self):
        self.get_feature_trainset()
        self.get_feature_testset()
        if self.proccessor_config.cls_config.k_fold == 0:
            if self.proccessor_config.cls_config.max_vote:
                first_cls = Classifier(ClsConfig(cls_type=g.CLASSIFIER_TYPE.SGD,
                                                 vec_type=g.VECTORIZER.Dict,
                                                 use_tf=True,
                                                 use_k_best=0,
                                                 use_cosine=False,
                                                 k_fold=0))

                second_cls = Classifier(ClsConfig(cls_type=g.CLASSIFIER_TYPE.DecisionTree,
                                                 vec_type=g.VECTORIZER.Dict,
                                                 use_tf=True,
                                                 use_k_best=0,
                                                 use_cosine=False,
                                                 k_fold=0))
                first_cls.set_y_train(self.score_list_train)
                first_cls.set_x_train(self.feature_list_train)
                first_cls.train()
                print "{0} IS TRAINED".format(g.CLASSIFIER_TYPE.name[first_cls.cls_type])
                second_cls.set_y_train(self.score_list_train)
                second_cls.set_x_train(self.feature_list_train)
                second_cls.train()
                print "{0} IS TRAINED".format(g.CLASSIFIER_TYPE.name[second_cls.cls_type])

                self.classifier.set_y_train(self.score_list_train)
                self.classifier.set_x_train(self.feature_list_train)
                self.classifier.train()

                max_vote = MaxVoteClassifier([self.classifier, first_cls, second_cls])
                for each in self.feature_list_test:
                    self.predictions.append(max_vote.classify(each))
                first_cls.save_model()
                second_cls.save_model()

            else:
                self.classifier.set_y_train(self.score_list_train)
                self.classifier.set_x_train(self.feature_list_train)
                self.classifier.set_x_trial(self.feature_list_test)
                self.classifier.set_y_trial(self.score_list_test)
                self.classifier.train()
                self.classifier.save_model()

                self.predictions = self.classifier.predict()

            print classification_report(self.score_list_test, self.predictions)
            print confusion_matrix(self.score_list_test, self.predictions)
            print "MSE", sklearn.metrics.mean_squared_error(self.score_list_test, self.predictions)



            match = 0
            positive = 0
            negative = 0
            neutral = 0

            for i in range(0, len(self.predictions)):
                # g.logger.debug("tweet:{0}\tpredicted:{1}\texpected:{2}".format(self.dataset_handler.test_set_data[i], self.predictions[i], self.score_list_test[i]))
                # calculate accuracy
                if self.predictions[i] == self.score_list_test[i]:
                    match += 1
                    if self.predictions[i] ==1:
                        positive += 1
                    elif self.predictions[i] == -1:
                        negative += 1
                    else:
                        neutral += 1

            results = "{0}/{1} PERCENTAGE: {2}% \n".format(match,
                                                       len(self.score_list_test),
                                                       (float(match)/float(len(self.score_list_test)))*100)
            g.logger.info(results)
            print results, "pos | neg | neu ", positive, negative, neutral
            try:
                metrics = self.classifier.get_metrics()
                g.logger.info(metrics)

                if self.proccessor_config.cls_config.max_vote:
                    metrics = first_cls.get_metrics()
                    g.logger.info("NO 1" + metrics)
                    g.logger.info("NO 1" + first_cls.get_most_informant_features())
                    metrics = second_cls.get_metrics()
                    g.logger.info("NO 2" + metrics)
                    g.logger.info("NO 2" + second_cls.get_most_informant_features())

                print metrics
            except:
                pass
        else:
            print self.classifier.predict_cross_validate(self.feature_list_train + self.feature_list_test,
                                                         self.score_list_train + self.score_list_test,
                                                         num_of_validations=10)

        self.classifier.save_model()

    def _insert(self, tweet):
        tweet._insert()

    def _get_clean_feature_dict(self, dicta):

        for k in dicta.keys():
            if not self.excluded(k, str(dicta[k])):
                if 's_word-' in k or 'swn_score' in k:
                    # dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral" \
                    #             if 0.95 <= dicta[k] <= 1.05\
                    #             else "somewhat_positive" if 1.2 >= dicta[k] > 1.05 \
                    #             else "somewhat_negative"

                    if dicta[k] > 1.2:
                        dicta[k] = "positive"
                    elif dicta[k] < 0.2:
                        dicta[k] = "negative"
                    elif 0.95 <= dicta[k] <= 1.05:
                        dicta[k] = "neutral"
                    elif 0.95 > dicta[k] > 0.2:
                        dicta[k] = "somewhat_negative"
                    elif 1.05 < dicta[k] < 1.2:
                        dicta[k] = "somewhat_positive"

                # if self.use > 0:
                if dicta[k] in g.VERBS:
                    dicta[k] = "VB"
                elif dicta[k] in g.NOUNS:
                    dicta[k] = "NN"
                elif dicta[k] in g.ADJECTIVES:
                    dicta[k] = "ADJ"
                elif dicta[k] in g.ADVERBS:
                    dicta[k] = "RB"

                if k == "__punctuation_percentage__":
                    dicta[k] = 1 if dicta[k] > 5 else 0
                # if k == "__multiple_chars_in_a_row__":
                #     dicta[k] = 0 if dicta[k] == False else 1
                #
                # if k in self.tags:
                #     dicta[k] = 0 if dicta[k] == "False" else 1
                # if self.use > 0:
                # if "__lemma_word__" in k:
                #     dicta[k] = "True"


            else:
                del dicta[k]

        return dicta

    def excluded(self, k, dicta_k_value):
        if 's_word' in k:                                   # if k is a sentiwordnet feature
            if 's_word' not in self.selected_features:      # if swn is not wanted
                return True                                 # remove
            return False
        if '__contains__' in k:                                # if k is a sentiwordnet feature
            if '__contains__' not in self.selected_features:   # if swn is not wanted
                return True                                 # remove
            return False
        if '__synset_length__' in k:                                # if k is a sentiwordnet feature
            if '__synset_length__' not in self.selected_features:   # if swn is not wanted
                return True                                     # remove
            return False
        if '__swn_score__' == k:                                # if k is sentiwordnet total score
            if '__swn_score__' not in self.selected_features:   # if swn is not wanted
                return True                                 # remove
            return False
        if 'pos_position_' in k:
            if 'pos_position_' not in self.selected_features:
                return True
            return False

        if dicta_k_value in g.VERBS or \
            dicta_k_value in g.ADJECTIVES or \
            dicta_k_value in g.ADVERBS or \
            dicta_k_value in g.NOUNS:\
            #     or \
            # dicta_k_value in g.OTHERS:
            if 'postags' not in self.selected_features:
                return True
            return False
        if 'word-' in dicta_k_value:
            if 'words' not in self.selected_features or 'word' not in self.selected_features:
                return True
            return False
        if 't-similarity' in k:
            if 't_similarity' not in self.selected_features:
                return True
            return False
        if '__res__' == k:
            if '__res__' not in self.selected_features:
                return True
            return False
        if '__lin__' == k:
            if '__lin__' not in self.selected_features:
                return True
            return False
        if '__path__' == k:
            if '__path__' not in self.selected_features:
                return True
            return False
        if '__wup__' == k:
            if '__wup__' not in self.selected_features:
                return True
            return False
        if dicta_k_value in self.sentiment_discretization_values:
            if "__lemma_word__" not in self.selected_features:
                return True
            return False
        if k not in self.selected_features:
            return True
        return False

    @staticmethod
    def remove_non_ascii_chars(text):
        """
        to solve problem with extra / weird characters when getting data from database
        :return:
        """
        return str(filter(lambda x: x in string.printable, text))