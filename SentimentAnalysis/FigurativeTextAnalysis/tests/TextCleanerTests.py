from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from SentimentAnalysis.FigurativeTextAnalysis.models.TextCleaner import TextCleaner
from SentimentAnalysis.FigurativeTextAnalysis.models.TweetBO import Tweet

__author__ = 'm.karanasou'

import unittest


class TextCleanerTests(unittest.TestCase):

    def test_remove_non_ascii_chars(self):
        cleaner = TextCleaner(Tweet(0, "Test", None, None), None)
        cleaner.remove_non_ascii_chars()
        print cleaner.tweet.text
        print cleaner.tweet.clean_text

    def test_remove_RT(self):
        cleaner1 = TextCleaner(Tweet(0, "RT @IndianaWebSport: Good thing this is a 'better match-up' for Hibbert. I'd hate to see him in a 'bad match-up'. #sarcasm", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "RT @Winkerbell_: The American National Spelling Bee, where winners are always Indians.", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "RT@AcrossTheCourt_: LeBron fans using the same type of meme on KD that Kobe fans used to use on them. #Irony pic.twitter.com/g07gqgqcMo", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "Wow thanks. You're being such a great sport :) #sarcasm -_-", None, None), None)
        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text

        self.assertTrue(cleaner1.tweet.text != cleaner1.tweet.clean_text)
        self.assertTrue(cleaner2.tweet.text != cleaner2.tweet.clean_text)
        self.assertTrue(cleaner3.tweet.text != cleaner3.tweet.clean_text)
        self.assertTrue(cleaner4.tweet.text == cleaner4.tweet.clean_text)

    def test_identify_and_remove_laughter(self):
        "Lol I have some good friends . #sarcasm #fuckyou"
        cleaner1 = TextCleaner(Tweet(0, "Lol I have some good friends . #sarcasm #fuckyou", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "HOLD ON!!!!! Nigel Farage's wife is German? HAHAHAAHAHAHAHAHA!!!!!! You just couldn't write it! Hahahaha!!!! #Irony", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "omg Rosa plays Lynn and Marlene is played by Suki like literally they're so pretty", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "@Borislola maybe I will just start swearing and carrying on by insulting everybody to try to prove my point, because that's classy. #not", None, None), None)
        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text

        self.assertTrue(cleaner1.tweet.text != cleaner1.tweet.clean_text)
        self.assertTrue(cleaner2.tweet.text != cleaner2.tweet.clean_text)
        self.assertTrue(cleaner3.tweet.text != cleaner3.tweet.clean_text)
        self.assertTrue(cleaner4.tweet.text == cleaner4.tweet.clean_text)

    def test_split_sentences(self):
        cleaner1 = TextCleaner(Tweet(0, "Lol I have some good friends . #sarcasm #fuckyou", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "HOLD ON!!!!! Nigel Farage's wife is German? HAHAHAAHAHAHAHAHA!!!!!! You just couldn't write it! Hahahaha!!!! #Irony", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "omg Rosa plays Lynn and Marlene is played by Suki like literally they're so pretty", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "@Borislola maybe I will just start swearing and carrying on by insulting everybody to try to prove my point, because that's classy. #not", None, None), None)
        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.split_sentences()
        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.sentences

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.split_sentences()
        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.sentences

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.split_sentences()
        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.sentences

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.split_sentences()
        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.sentences

        self.assertTrue(len(cleaner1.tweet.sentences) == 2)
        self.assertTrue(len(cleaner2.tweet.sentences) == 6)  # ???
        self.assertTrue(len(cleaner3.tweet.sentences) == 1)
        self.assertTrue(len(cleaner4.tweet.sentences) == 2)

    def test_identify_negations(self):
        cleaner1 = TextCleaner(Tweet(0, "Lol I have some good friends . #sarcasm #fuckyou", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "HOLD ON!!!!! Nigel Farage's wife is German? HAHAHAAHAHAHAHAHA!!!!!! You just couldn't write it! Hahahaha!!!! #Irony", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "omg Rosa plays Lynn and Marlene is played by Suki like literally they're so pretty", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "@Borislola maybe I will just start swearing and carrying on by insulting everybody to try to prove my point, because that's classy. #not", None, None), None)
        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.found_negations

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.found_negations

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.found_negations

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.found_negations

        self.assertTrue(not cleaner1.found_negations)
        self.assertTrue(cleaner2.found_negations)
        self.assertTrue(not cleaner3.found_negations)
        self.assertTrue(cleaner4.found_negations)

    def test_split_sentences(self):
        cleaner1 = TextCleaner(Tweet(0, "Lol I have some good friends . #sarcasm #fuckyou", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "HOLD ON!!!!! Nigel Farage's wife is German? HAHAHAAHAHAHAHAHA!!!!!! You just couldn't write it! Hahahaha!!!! #Irony", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "omg Rosa plays Lynn and Marlene is played by Suki like literally they're so pretty", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "@Borislola maybe I will just start swearing and carrying on by insulting everybody to try to prove my point, because that's classy. #not", None, None), None)
        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.sentences

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.sentences

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.sentences

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.sentences

        self.assertTrue(len(cleaner1.tweet.sentences) == 2)
        self.assertTrue(len(cleaner2.tweet.sentences) == 6)  # ???
        self.assertTrue(len(cleaner3.tweet.sentences) == 1)
        self.assertTrue(len(cleaner4.tweet.sentences) == 2)

    def test_has_capitals(self):
        cleaner1 = TextCleaner(Tweet(0, "Lol I have some good friends . #sarcasm #fuckyou", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "HOLD ON!!!!! Nigel Farage's wife is German? HAHAHAAHAHAHAHAHA!!!!!! You just couldn't write it! Hahahaha!!!! #Irony", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "omg Rosa plays Lynn and Marlene is played by Suki like literally they're so pretty", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "@Borislola maybe I will just start swearing and carrying on by insulting everybody to try to prove my point, because that's classy. #not", None, None), None)
        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.uppercase_words_per_sentence

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.uppercase_words_per_sentence

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.uppercase_words_per_sentence

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.uppercase_words_per_sentence

        self.assertTrue(len(cleaner1.tweet.uppercase_words_per_sentence) == 0)
        self.assertTrue(len(cleaner2.tweet.uppercase_words_per_sentence) > 0)  # ???
        self.assertTrue(len(cleaner3.tweet.uppercase_words_per_sentence) == 0)
        self.assertTrue(len(cleaner4.tweet.uppercase_words_per_sentence) == 0)

    def test_remove_links(self):
        cleaner1 = TextCleaner(Tweet(0, "Lol I have some good friends . #sarcasm #fuckyou", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "HOLD ON!!!!! Nigel Farage's wife is German? HAHAHAAHAHAHAHAHA!!!!!! You just couldn't write it! Hahahaha!!!! #Irony", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "omg Rosa plays Lynn and Marlene is played by Suki like literally they're so pretty", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "@Borislola maybe I will just start swearing and carrying on by insulting everybody to try to prove my point, because that's classy. #not", None, None), None)
        cleaner5 = TextCleaner(Tweet(0, "However, it has #not been #seen www.monstermmorpg. com battling. redo follow @MonsterMMORPG #apple", None, None), None)
        cleaner6 = TextCleaner(Tweet(0, "RT @FlatBuddhaBelly: Sin City got beat out by Amish Country #momentum #marriageequality get it together #Nevada #irony http://t.co/4sH6jnaX", None, None), None)
        cleaner7 = TextCleaner(Tweet(0, "RT @CaptnThundrCunt: @LuiDeksel You're right. I wish my pussy wasn't so loose :( #sarcasm pic.twitter.com/KOLOd4dGCN", None, None), None)

        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()
        cleaner1.remove_links()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.links
        print cleaner1.tweet.sentences

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()
        cleaner2.remove_links()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.links
        print cleaner2.tweet.sentences

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()
        cleaner3.remove_links()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.links
        print cleaner3.tweet.sentences

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()
        cleaner4.remove_links()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.links
        print cleaner4.tweet.sentences

        cleaner5.remove_non_ascii_chars()
        cleaner5.remove_RT()
        cleaner5.identify_and_remove_laughter()
        cleaner5.identify_negations()
        cleaner5.split_sentences()
        cleaner5.has_capitals()
        cleaner5.remove_links()

        print cleaner5.tweet.text
        print cleaner5.tweet.clean_text
        print cleaner5.tweet.links
        print cleaner5.tweet.sentences

        cleaner6.remove_non_ascii_chars()
        cleaner6.remove_RT()
        cleaner6.identify_and_remove_laughter()
        cleaner6.identify_negations()
        cleaner6.split_sentences()
        cleaner6.has_capitals()
        cleaner6.remove_links()

        print cleaner6.tweet.text
        print cleaner6.tweet.clean_text
        print cleaner6.tweet.links
        print cleaner6.tweet.sentences

        cleaner7.remove_non_ascii_chars()
        cleaner7.remove_RT()
        cleaner7.identify_and_remove_laughter()
        cleaner7.identify_negations()
        cleaner7.split_sentences()
        cleaner7.has_capitals()
        cleaner7.remove_links()

        print cleaner7.tweet.text
        print cleaner7.tweet.clean_text
        print cleaner7.tweet.links
        print cleaner7.tweet.sentences

        self.assertTrue(len(cleaner1.tweet.links) == 0)
        self.assertTrue(len(cleaner2.tweet.links) == 0)  # ???
        self.assertTrue(len(cleaner3.tweet.links) == 0)
        self.assertTrue(len(cleaner4.tweet.links) == 0)
        self.assertTrue(len(cleaner5.tweet.links) > 0)
        self.assertTrue(len(cleaner6.tweet.links) > 0)
        self.assertTrue(len(cleaner7.tweet.links) > 0)

    def test_store_and_remove_emoticons(self):
        cleaner1 = TextCleaner(Tweet(0, "'Wow it's a good thing I left the windows of my moms car down when it was raining & hailing :):):) #not", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "Isn't it AMAZING? :)))))) #sarcasm instagram.com/p/nl6UCJAdts/", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "RT @CaptnThundrCunt: @LuiDeksel You're right. I wish my pussy wasn't so loose :( #sarcasm pic.twitter.com/KOLOd4dGCN", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "I can't believe class day is tomorrow... #mixedemotions I pray my class can act somewhat mature :/ #yeahright #weredoomed", None, None), None)

        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()
        cleaner1.remove_links()
        cleaner1.store_and_remove_emoticons()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.smileys_per_sentence
        print cleaner1.tweet.sentences

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()
        cleaner2.remove_links()
        cleaner2.store_and_remove_emoticons()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.smileys_per_sentence
        print cleaner2.tweet.sentences

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()
        cleaner3.remove_links()
        cleaner3.store_and_remove_emoticons()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.smileys_per_sentence
        print cleaner3.tweet.sentences

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()
        cleaner4.remove_links()
        cleaner4.store_and_remove_emoticons()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.smileys_per_sentence
        print cleaner4.tweet.sentences

        self.assertTrue(len(cleaner1.tweet.smileys_per_sentence) > 0)
        self.assertTrue(len(cleaner2.tweet.smileys_per_sentence) > 0)
        self.assertTrue(len(cleaner3.tweet.smileys_per_sentence) > 0)
        self.assertTrue(len(cleaner4.tweet.smileys_per_sentence) > 0)

    def test_remove_reference(self):
        cleaner1 = TextCleaner(Tweet(0, "'Wow it's a good thing I left the windows of my moms car down when it was raining & hailing :):):) #not", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "Isn't it AMAZING? :)))))) #sarcasm instagram.com/p/nl6UCJAdts/", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "RT @CaptnThundrCunt: @LuiDeksel You're right. I wish my pussy wasn't so loose :( #sarcasm pic.twitter.com/KOLOd4dGCN", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "I can't believe class day is tomorrow... #mixedemotions I pray my class can act somewhat mature :/ #yeahright #weredoomed", None, None), None)

        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()
        cleaner1.remove_links()
        cleaner1.store_and_remove_emoticons()
        cleaner1.remove_reference()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.reference
        print cleaner1.tweet.sentences

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()
        cleaner2.remove_links()
        cleaner2.store_and_remove_emoticons()
        cleaner2.remove_reference()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.reference
        print cleaner2.tweet.sentences

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()
        cleaner3.remove_links()
        cleaner3.store_and_remove_emoticons()
        cleaner3.remove_reference()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.reference
        print cleaner3.tweet.sentences

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()
        cleaner4.remove_links()
        cleaner4.store_and_remove_emoticons()
        cleaner4.remove_reference()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.reference
        print cleaner4.tweet.sentences

        self.assertTrue(len(cleaner1.tweet.reference[0]) == 0)
        self.assertTrue(len(cleaner2.tweet.reference[0]) == 0)
        self.assertTrue(len(cleaner3.tweet.reference[0]) > 0)
        self.assertTrue(len(cleaner4.tweet.reference[0]) == 0)

    def test_remove_special_chars(self):
        cleaner1 = TextCleaner(Tweet(0, "'Wow it's a good thing I left the windows of my moms car down when it was raining & hailing :):):) #not", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "Isn't it AMAZING? :)))))) #sarcasm instagram.com/p/nl6UCJAdts/", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "RT @CaptnThundrCunt: @LuiDeksel You're right. I wish my pussy wasn't so loose :( #sarcasm pic.twitter.com/KOLOd4dGCN", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "I can't believe class day is tomorrow... #mixedemotions I pray my class can act somewhat mature :/ #yeahright #weredoomed", None, None), None)

        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()
        cleaner1.remove_links()
        cleaner1.store_and_remove_emoticons()
        cleaner1.remove_reference()
        cleaner1.remove_special_chars()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.non_word_chars_removed
        print cleaner1.tweet.sentences

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()
        cleaner2.remove_links()
        cleaner2.store_and_remove_emoticons()
        cleaner2.remove_reference()
        cleaner2.remove_special_chars()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.non_word_chars_removed
        print cleaner2.tweet.sentences

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()
        cleaner3.remove_links()
        cleaner3.store_and_remove_emoticons()
        cleaner3.remove_reference()
        cleaner3.remove_special_chars()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.non_word_chars_removed
        print cleaner3.tweet.sentences

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()
        cleaner4.remove_links()
        cleaner4.store_and_remove_emoticons()
        cleaner4.remove_reference()
        cleaner4.remove_special_chars()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.non_word_chars_removed
        print cleaner4.tweet.sentences

        self.assertTrue(len(cleaner1.tweet.non_word_chars_removed[0]) > 0)
        self.assertTrue(len(cleaner2.tweet.non_word_chars_removed[0]) > 0)
        self.assertTrue(len(cleaner3.tweet.non_word_chars_removed[0]) > 0)
        self.assertTrue(len(cleaner4.tweet.non_word_chars_removed[0]) > 0)

    def test_fix_space(self):
        cleaner1 = TextCleaner(Tweet(0, "'Wow it's a good thing I left the windows of my moms car down when it was raining & hailing :):):) #not", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "Isn't it AMAZING? :)))))) #sarcasm instagram.com/p/nl6UCJAdts/", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "RT @CaptnThundrCunt: @LuiDeksel You're right. I wish my pussy wasn't so loose :( #sarcasm pic.twitter.com/KOLOd4dGCN", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "I can't believe class day is tomorrow... #mixedemotions I pray my class can act somewhat mature :/ #yeahright #weredoomed", None, None), None)

        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()
        cleaner1.remove_links()
        cleaner1.store_and_remove_emoticons()
        cleaner1.remove_reference()
        cleaner1.remove_special_chars()
        B4_1 = cleaner1.tweet.sentences
        print "B4", cleaner1.tweet.sentences
        cleaner1.fix_space()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.sentences

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()
        cleaner2.remove_links()
        cleaner2.store_and_remove_emoticons()
        cleaner2.remove_reference()
        cleaner2.remove_special_chars()
        B4_2 = cleaner2.tweet.sentences
        print "B4", cleaner2.tweet.sentences
        cleaner2.fix_space()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.sentences

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()
        cleaner3.remove_links()
        cleaner3.store_and_remove_emoticons()
        cleaner3.remove_reference()
        cleaner3.remove_special_chars()
        B4_3 = cleaner3.tweet.sentences
        print "B4", cleaner3.tweet.sentences
        cleaner3.fix_space()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.sentences

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()
        cleaner4.remove_links()
        cleaner4.store_and_remove_emoticons()
        cleaner4.remove_reference()
        cleaner4.remove_special_chars()
        B4_4 =cleaner4.tweet.sentences
        print "B4", cleaner4.tweet.sentences
        cleaner4.fix_space()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.sentences

        self.assertTrue(len(B4_1[0]) > len(cleaner1.tweet.sentences[0]))
        self.assertTrue(len(B4_2[0]) > len(cleaner2.tweet.sentences[0]))
        self.assertTrue(len(B4_3[0]) > len(cleaner3.tweet.sentences[0]))
        self.assertTrue(len(B4_4[0]) > len(cleaner4.tweet.sentences[0]))

    def test_split_words(self):
        cleaner1 = TextCleaner(Tweet(0, "'Wow it's a good thing I left the windows of my moms car down when it was raining & hailing :):):) #not", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "Isn't it AMAZING? :)))))) #sarcasm instagram.com/p/nl6UCJAdts/", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "RT @CaptnThundrCunt: @LuiDeksel You're right. I wish my pussy wasn't so loose :( #sarcasm pic.twitter.com/KOLOd4dGCN", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "I can't believe class day is tomorrow... #mixedemotions I pray my class can act somewhat mature :/ #yeahright #weredoomed", None, None), None)

        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()
        cleaner1.remove_links()
        cleaner1.store_and_remove_emoticons()
        cleaner1.remove_reference()
        cleaner1.remove_special_chars()
        cleaner1.fix_space()
        cleaner1.split_words()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.sentences
        print cleaner1.tweet.words

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()
        cleaner2.remove_links()
        cleaner2.store_and_remove_emoticons()
        cleaner2.remove_reference()
        cleaner2.remove_special_chars()
        cleaner2.fix_space()
        cleaner2.split_words()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.sentences
        print cleaner2.tweet.words

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()
        cleaner3.remove_links()
        cleaner3.store_and_remove_emoticons()
        cleaner3.remove_reference()
        cleaner3.remove_special_chars()
        cleaner3.fix_space()
        cleaner3.split_words()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.sentences
        print cleaner3.tweet.words

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()
        cleaner4.remove_links()
        cleaner4.store_and_remove_emoticons()
        cleaner4.remove_reference()
        cleaner4.remove_special_chars()
        cleaner4.fix_space()
        cleaner4.split_words()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.sentences
        print cleaner4.tweet.words

        # todo: better assertions
        self.assertTrue(len(cleaner1.tweet.words) > 0)
        self.assertTrue(len(cleaner2.tweet.words) > 0)
        self.assertTrue(len(cleaner3.tweet.words) > 0)
        self.assertTrue(len(cleaner4.tweet.words) > 0)

    def test_handle_negations(self):
        cleaner1 = TextCleaner(Tweet(0, "'Wow it's a good thing I left the windows of my moms car down when it was raining & hailing :):):) #not", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "I am totally not tired at all... #sarcasm #gonnatrytofightthetiredness", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "RT @CaptnThundrCunt: @LuiDeksel You're right. I wish my pussy wasn't so loose :( #sarcasm pic.twitter.com/KOLOd4dGCN", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "I can't believe class day is tomorrow... #mixedemotions I pray my class can act somewhat mature :/ #yeahright #weredoomed", None, None), None)

        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()
        cleaner1.remove_links()
        cleaner1.store_and_remove_emoticons()
        cleaner1.remove_reference()
        cleaner1.remove_special_chars()
        cleaner1.fix_space()
        cleaner1.split_words()
        cleaner1.handle_negations()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.sentences
        print cleaner1.tweet.words
        print cleaner1.tweet.negations

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()
        cleaner2.remove_links()
        cleaner2.store_and_remove_emoticons()
        cleaner2.remove_reference()
        cleaner2.remove_special_chars()
        cleaner2.fix_space()
        cleaner2.split_words()
        cleaner2.handle_negations()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.sentences
        print cleaner2.tweet.words
        print cleaner2.tweet.negations

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()
        cleaner3.remove_links()
        cleaner3.store_and_remove_emoticons()
        cleaner3.remove_reference()
        cleaner3.remove_special_chars()
        cleaner3.fix_space()
        cleaner3.split_words()
        cleaner3.handle_negations()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.sentences
        print cleaner3.tweet.words
        print cleaner3.tweet.negations

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()
        cleaner4.remove_links()
        cleaner4.store_and_remove_emoticons()
        cleaner4.remove_reference()
        cleaner4.remove_special_chars()
        cleaner4.fix_space()
        cleaner4.split_words()
        cleaner4.handle_negations()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.sentences
        print cleaner4.tweet.words
        print cleaner4.tweet.negations

        self.assertTrue(len(cleaner1.tweet.negations) > 0)
        self.assertTrue(len(cleaner2.tweet.negations) > 0)
        self.assertTrue(len(cleaner3.tweet.negations) > 0)
        self.assertTrue(len(cleaner4.tweet.negations) > 0)

    def test_convert_to_lower(self):
        cleaner1 = TextCleaner(Tweet(0, "'Wow it's a good thing I left the windows of my moms car down when it was raining & hailing :):):) #not", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "I am totally not tired at all... #sarcasm #gonnatrytofightthetiredness", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "RT @CaptnThundrCunt: @LuiDeksel You're right. I wish my pussy wasn't so loose :( #sarcasm pic.twitter.com/KOLOd4dGCN", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "I can't believe class day is tomorrow... #mixedemotions I pray my class can act somewhat mature :/ #yeahright #weredoomed", None, None), None)

        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()
        cleaner1.remove_links()
        cleaner1.store_and_remove_emoticons()
        cleaner1.remove_reference()
        cleaner1.remove_special_chars()
        cleaner1.fix_space()
        cleaner1.split_words()
        cleaner1.handle_negations()
        cleaner1.convert_to_lower()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.sentences
        print cleaner1.tweet.words
        print cleaner1.tweet.negations

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()
        cleaner2.remove_links()
        cleaner2.store_and_remove_emoticons()
        cleaner2.remove_reference()
        cleaner2.remove_special_chars()
        cleaner2.fix_space()
        cleaner2.split_words()
        cleaner2.handle_negations()
        cleaner2.convert_to_lower()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.sentences
        print cleaner2.tweet.words
        print cleaner2.tweet.negations

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()
        cleaner3.remove_links()
        cleaner3.store_and_remove_emoticons()
        cleaner3.remove_reference()
        cleaner3.remove_special_chars()
        cleaner3.fix_space()
        cleaner3.split_words()
        cleaner3.handle_negations()
        cleaner3.convert_to_lower()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.sentences
        print cleaner3.tweet.words
        print cleaner3.tweet.negations

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()
        cleaner4.remove_links()
        cleaner4.store_and_remove_emoticons()
        cleaner4.remove_reference()
        cleaner4.remove_special_chars()
        cleaner4.fix_space()
        cleaner4.split_words()
        cleaner4.handle_negations()
        cleaner4.convert_to_lower()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.sentences
        print cleaner4.tweet.words
        print cleaner4.tweet.negations

        # self.assertTrue(len(cleaner1.tweet.negations) > 0)
        # self.assertTrue(len(cleaner2.tweet.negations) > 0)
        # self.assertTrue(len(cleaner3.tweet.negations) > 0)
        # self.assertTrue(len(cleaner4.tweet.negations) > 0)

    def test_remove_multiples(self):
        cleaner1 = TextCleaner(Tweet(0, "'Wow it's a good thing I left the windows of my moms car down when it was raining & hailing :):):) #not", None, None), None)
        cleaner2 = TextCleaner(Tweet(0, "@MAGGIECHICKEN HA! im printing mine @ school coz my printer can be pretty dodddgy. cost me like 15 bucks  :( ", None, None), None)
        cleaner3 = TextCleaner(Tweet(0, "This wedding tomorrow is about to be sooooo fun #sarcasm", None, None), None)
        cleaner4 = TextCleaner(Tweet(0, "I Havent Talked To My BAEEEEEEE None YESTERDAY ! #Not Coool", None, None), None)

        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()
        cleaner1.remove_links()
        cleaner1.store_and_remove_emoticons()
        cleaner1.remove_reference()
        cleaner1.remove_special_chars()
        cleaner1.fix_space()
        cleaner1.split_words()
        cleaner1.handle_negations()
        cleaner1.convert_to_lower()
        cleaner1.remove_multiples()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.sentences
        print cleaner1.tweet.words
        print cleaner1.tweet.corrected_words

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()
        cleaner2.remove_links()
        cleaner2.store_and_remove_emoticons()
        cleaner2.remove_reference()
        cleaner2.remove_special_chars()
        cleaner2.fix_space()
        cleaner2.split_words()
        cleaner2.handle_negations()
        cleaner2.convert_to_lower()
        cleaner2.remove_multiples()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.sentences
        print cleaner2.tweet.words
        print cleaner2.tweet.corrected_words

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()
        cleaner3.remove_links()
        cleaner3.store_and_remove_emoticons()
        cleaner3.remove_reference()
        cleaner3.remove_special_chars()
        cleaner3.fix_space()
        cleaner3.split_words()
        cleaner3.handle_negations()
        cleaner3.convert_to_lower()
        cleaner3.remove_multiples()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.sentences
        print cleaner3.tweet.words
        print cleaner3.tweet.corrected_words

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()
        cleaner4.remove_links()
        cleaner4.store_and_remove_emoticons()
        cleaner4.remove_reference()
        cleaner4.remove_special_chars()
        cleaner4.fix_space()
        cleaner4.split_words()
        cleaner4.handle_negations()
        cleaner4.convert_to_lower()
        cleaner4.remove_multiples()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.sentences
        print cleaner4.tweet.words
        print cleaner4.tweet.corrected_words

        self.assertTrue(len(cleaner1.tweet.corrected_words) == 0)
        self.assertTrue(len(cleaner2.tweet.corrected_words) == 1)
        self.assertTrue(len(cleaner3.tweet.corrected_words) > 0)
        self.assertTrue(len(cleaner4.tweet.corrected_words) == 2)

    def test_remove_stop_words(self):
        stopwords = g.mysql_conn.execute_query(g.select_from_stop_words())
        cleaner1 = TextCleaner(Tweet(0, "'Wow it's a good thing I left the windows of my moms car down when it was raining & hailing :):):) #not", stopwords, None), None)
        cleaner2 = TextCleaner(Tweet(0, "I am totally not tired at all... #sarcasm #gonnatrytofightthetiredness", stopwords, None), None)
        cleaner3 = TextCleaner(Tweet(0, "This wedding tomorrow is about to be sooooo fun #sarcasm", stopwords, None), None)
        cleaner4 = TextCleaner(Tweet(0, "I Havent Talked To My BAEEEEEEE since YESTERDAY ! #Not Coool", stopwords, None), None)

        cleaner1.remove_non_ascii_chars()
        cleaner1.remove_RT()
        cleaner1.identify_and_remove_laughter()
        cleaner1.identify_negations()
        cleaner1.split_sentences()
        cleaner1.has_capitals()
        cleaner1.remove_links()
        cleaner1.store_and_remove_emoticons()
        cleaner1.remove_reference()
        cleaner1.remove_special_chars()
        cleaner1.fix_space()
        cleaner1.split_words()
        cleaner1.handle_negations()
        cleaner1.convert_to_lower()
        cleaner1.remove_multiples()
        cleaner1.remove_stop_words()

        print cleaner1.tweet.text
        print cleaner1.tweet.clean_text
        print cleaner1.tweet.sentences
        print cleaner1.tweet.words
        print cleaner1.stop_words_removed

        cleaner2.remove_non_ascii_chars()
        cleaner2.remove_RT()
        cleaner2.identify_and_remove_laughter()
        cleaner2.identify_negations()
        cleaner2.split_sentences()
        cleaner2.has_capitals()
        cleaner2.remove_links()
        cleaner2.store_and_remove_emoticons()
        cleaner2.remove_reference()
        cleaner2.remove_special_chars()
        cleaner2.fix_space()
        cleaner2.split_words()
        cleaner2.handle_negations()
        cleaner2.convert_to_lower()
        cleaner2.remove_multiples()
        cleaner2.remove_stop_words()

        print cleaner2.tweet.text
        print cleaner2.tweet.clean_text
        print cleaner2.tweet.sentences
        print cleaner2.tweet.words
        print cleaner2.stop_words_removed

        cleaner3.remove_non_ascii_chars()
        cleaner3.remove_RT()
        cleaner3.identify_and_remove_laughter()
        cleaner3.identify_negations()
        cleaner3.split_sentences()
        cleaner3.has_capitals()
        cleaner3.remove_links()
        cleaner3.store_and_remove_emoticons()
        cleaner3.remove_reference()
        cleaner3.remove_special_chars()
        cleaner3.fix_space()
        cleaner3.split_words()
        cleaner3.handle_negations()
        cleaner3.convert_to_lower()
        cleaner3.remove_multiples()
        cleaner3.remove_stop_words()

        print cleaner3.tweet.text
        print cleaner3.tweet.clean_text
        print cleaner3.tweet.sentences
        print cleaner3.tweet.words
        print cleaner3.stop_words_removed

        cleaner4.remove_non_ascii_chars()
        cleaner4.remove_RT()
        cleaner4.identify_and_remove_laughter()
        cleaner4.identify_negations()
        cleaner4.split_sentences()
        cleaner4.has_capitals()
        cleaner4.remove_links()
        cleaner4.store_and_remove_emoticons()
        cleaner4.remove_reference()
        cleaner4.remove_special_chars()
        cleaner4.fix_space()
        cleaner4.split_words()
        cleaner4.handle_negations()
        cleaner4.convert_to_lower()
        cleaner4.remove_multiples()
        cleaner4.remove_stop_words()

        print cleaner4.tweet.text
        print cleaner4.tweet.clean_text
        print cleaner4.tweet.sentences
        print cleaner4.tweet.words
        print cleaner4.stop_words_removed

        self.assertTrue(len(cleaner1.tweet.corrected_words) == 0)
        self.assertTrue(len(cleaner2.tweet.corrected_words) == 0)
        self.assertTrue(len(cleaner3.tweet.corrected_words) > 0)
        self.assertTrue(len(cleaner4.tweet.corrected_words) == 2)