from SentimentAnalysis.FigurativeTextAnalysis.models.TextTagger import TextTagger

__author__ = 'maria'

import unittest


class TextTaggerTester(unittest.TestCase):

    def test_laugh_pattern(self):
        self.maxDiff = None
        text = "the amount of schoolwork i have is so absurd i might just watch the WNBA instead. ahahahaha #NOT"
        tags_result ={'__OH_SO__': "False",
                      '__LOVE__': "False",
                      '__AS_GROUND_AS_VEHICLE__': "False",
                      '__HT__': "False",
                      '__HT_POS__': "False",
                      '__HT_NEG__': "True",
                      '__POS_SMILEY__': "False",
                      '__LINK__': "False",
                      '__CAPITAL__': "True",
                      '__DONT_YOU__': "False",
                      '__RT__': "False",
                      '__NEG_SMILEY__': "False",
                      '__REFERENCE__': "False",
                      '__NEGATION__': "True",
                      '__LAUGH__': "True",
                      '__exclamation__': "False",
                      '__fullstop__': "False",
                      '__hashtag_lexicon_sum__': 3.0,
                      '__punctuation_percentage__': 4,
                      '__questionmark__': "False"
                      }
        tagger = TextTagger()
        tagger.tag_text(text)
        print tagger._tags

        self.assertDictEqual(tags_result, tagger._tags)

    def test_OH_SO_pattern(self):
        text = "Oh, so the amount of schoolwork i have is so absurd i might just watch the wnba instead. ahahahaha"
        tagger = TextTagger()
        tagger.tag_text(text)
        print tagger._tags

        self.assertTrue(tagger._tags["__OH_SO__"] == "True")
        self.assertTrue(tagger._tags["__LOVE__"] == "False")
        self.assertTrue(tagger._tags["__POS_SMILEY__"] == "False")
        self.assertTrue(tagger._tags["__LAUGH__"] == "True")

        # self.assertDictEqual(tags_result, tagger._tags)

    def test_CAPITALS_pattern(self):
        text = "Oh, so the amount of schoolwork i have is so absurd i might just watch the WNBA instead. #not #boring"
        tags_result ={'OH_SO': "True", 'LOVE': "False", 'AS_GROUND_AS_VEHICLE': "False", 'HT': "False", 'HT_POS': "False",
                      'HT_NEG': "True", 'POS_SMILEY': "False", 'LINK': "False",
                      'CAPITAL': "True", 'DONT_YOU': "False", 'RT': "False", 'NEG_SMILEY': "False", 'REFERENCE': "False",
                      'NEGATION': "True",
                      'LAUGH': "False"}
        tagger = TextTagger()
        tagger.tag_text(text)
        print tagger._tags

        self.assertTrue(tagger._tags["__OH_SO__"] == "True")
        self.assertTrue(tagger._tags["__LOVE__"] == "False")
        self.assertTrue(tagger._tags["__POS_SMILEY__"] == "False")
        self.assertTrue(tagger._tags["__HT_NEG__"] == "True")
        self.assertTrue(tagger._tags["__LAUGH__"] == "False")
        self.assertTrue(tagger._tags["__CAPITAL__"] == "True")
        self.assertTrue(tagger._tags["__NEGATION__"] == "True")


    def test_negation_pattern(self):
        text = "the amount of schoolwork i have is so absurd i might just watch the WNBA instead#NOT"
        tagger = TextTagger()
        tagger.tag_text(text)
        print tagger._tags

        self.assertTrue(tagger._tags["__OH_SO__"] == "False")
        self.assertTrue(tagger._tags["__LOVE__"] == "False")
        self.assertTrue(tagger._tags["__POS_SMILEY__"] == "False")
        self.assertTrue(tagger._tags["__HT_NEG__"] == "True")
        self.assertTrue(tagger._tags["__LAUGH__"] == "False")
        self.assertTrue(tagger._tags["__CAPITAL__"] == "True")
        self.assertTrue(tagger._tags["__NEGATION__"] == "True")

    def test_ht_neg_pattern2(self):
        text = "the amount of schoolwork i have is so absurd i might just watch the WNBA instead#NOT#fail"
        # text2 = "the amount of schoolwork i have is so absurd i might just watch the WNBA instead#NOT"

        tagger = TextTagger()
        tagger.tag_text(text)
        # tagger.tag_text(text2)
        print tagger._tags

        self.assertTrue(tagger._tags["__OH_SO__"] == "False")
        self.assertTrue(tagger._tags["__LOVE__"] == "False")
        self.assertTrue(tagger._tags["__POS_SMILEY__"] == "False")
        self.assertTrue(tagger._tags["__HT_NEG__"] == "True")
        self.assertTrue(tagger._tags["__LAUGH__"] == "False")
        self.assertTrue(tagger._tags["__CAPITAL__"] == "True")
        self.assertTrue(tagger._tags["__NEGATION__"] == "True")


    def test_positive_smiley_pattern(self):
        text = "@JedJiggle hahaa yeah! :D and on my other acc i had diff pics n have to make totally new one now cuz they look tots weird.... niiiiice! #not"
        text = "@erickaaa haha awww. i had amazing pizza 2day for much after my photoshoot =D i will take u out hehe..u gonna answer ur fone 2night?  xx :) "
        tagger = TextTagger()
        tagger.tag_text(text)
        print tagger._tags

        self.assertTrue(tagger._tags["__OH_SO__"] == "False")
        self.assertTrue(tagger._tags["__REFERENCE__"] == "True")
        self.assertTrue(tagger._tags["__LOVE__"] == "False")
        self.assertTrue(tagger._tags["__POS_SMILEY__"] == "True")
        self.assertTrue(tagger._tags["__HT_NEG__"] == "True")
        self.assertTrue(tagger._tags["__LAUGH__"] == "True")
        self.assertTrue(tagger._tags["__CAPITAL__"] == "False")
        self.assertTrue(tagger._tags["__NEGATION__"] == "True")
        self.assertTrue(tagger._tags["__exclamation__"] == "True")
        self.assertTrue(tagger._tags["__fullstop__"] == "True")
        self.assertTrue(tagger._tags["__punctuation_percentage__"] > 1)

    def test_negative_smiley_pattern(self):
        text = "RT @CaptnThundrCunt: @LuiDeksel You're right. I wish my pussy wasn't so loose :( #sarcasm pic.twitter.com/KOLOd4dGCN"
        tagger = TextTagger()
        tagger.tag_text(text)
        print tagger._tags

        self.assertTrue(tagger._tags["__OH_SO__"] == "False")
        self.assertTrue(tagger._tags["__REFERENCE__"] == "True")
        self.assertTrue(tagger._tags["__RT__"] == "True")
        self.assertTrue(tagger._tags["__DONT_YOU__"] == "False")
        self.assertTrue(tagger._tags["__LOVE__"] == "False")
        self.assertTrue(tagger._tags["__POS_SMILEY__"] == "False")
        self.assertTrue(tagger._tags["__NEG_SMILEY__"] == "True")
        self.assertTrue(tagger._tags["__exclamation__"] == "False")
        self.assertTrue(tagger._tags["__HT__"] == "True")
        self.assertTrue(tagger._tags["__LAUGH__"] == "False")
        self.assertTrue(tagger._tags["__CAPITAL__"] == "False")       # 1 due to RT!!
        self.assertTrue(tagger._tags["__NEGATION__"] == "True")
        self.assertTrue(tagger._tags["__punctuation_percentage__"] >10)

    def test_rt_pattern(self):
        text = "rt:@CaptnThundrCunt: @LuiDeksel You're right. I wish my pussy wasn't so loose :( #sarcasm pic.twitter.com/KOLOd4dGCN"
        tagger = TextTagger()
        tagger.tag_text(text)
        print tagger._tags

        self.assertTrue(tagger._tags["__OH_SO__"] == "False")
        self.assertTrue(tagger._tags["__REFERENCE__"] == "True")
        self.assertTrue(tagger._tags["__RT__"] == "True")
        self.assertTrue(tagger._tags["__DONT_YOU__"] == "False")
        self.assertTrue(tagger._tags["__LOVE__"] == "False")
        self.assertTrue(tagger._tags["__POS_SMILEY__"] == "False")
        self.assertTrue(tagger._tags["__NEG_SMILEY__"] == "True")
        self.assertTrue(tagger._tags["__exclamation__"] == "False")
        self.assertTrue(tagger._tags["__HT__"] == "True")
        self.assertTrue(tagger._tags["__LAUGH__"] == "False")
        self.assertTrue(tagger._tags["__CAPITAL__"] == "False")       # 1 due to RT!!
        self.assertTrue(tagger._tags["__NEGATION__"] == "True")
        self.assertTrue(tagger._tags["__punctuation_percentage__"] >10)

    def test_dont_you_pattern(self):
        text = "Don't you love it when people never text you back?"
        tagger = TextTagger()
        tagger.tag_text(text)
        print tagger._tags

        self.assertTrue(tagger._tags["__OH_SO__"] == "False")
        self.assertTrue(tagger._tags["__REFERENCE__"] == "False")
        self.assertTrue(tagger._tags["__RT__"] == "False")
        self.assertTrue(tagger._tags["__DONT_YOU__"] == "True")
        self.assertTrue(tagger._tags["__LOVE__"] == "False")
        self.assertTrue(tagger._tags["__POS_SMILEY__"] == "False")
        self.assertTrue(tagger._tags["__NEG_SMILEY__"] == "False")
        self.assertTrue(tagger._tags["__exclamation__"] == "False")
        self.assertTrue(tagger._tags["__questionmark__"] == "True")
        self.assertTrue(tagger._tags["__fullstop__"] == "False")
        self.assertTrue(tagger._tags["__HT__"] == "False")
        self.assertTrue(tagger._tags["__LAUGH__"] == "False")
        self.assertTrue(tagger._tags["__CAPITAL__"] == "False")       # 1 due to RT!!
        self.assertTrue(tagger._tags["__NEGATION__"] == "True")
        self.assertTrue(tagger._tags["__punctuation_percentage__"] >1)
        self.assertTrue(tagger._tags["__hashtag_lexicon_sum__"] == "False")

if __name__ == '__main__':
    unittest.main()
