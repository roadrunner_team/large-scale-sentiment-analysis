import copy
import unittest
from itertools import combinations
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from SentimentAnalysis.FigurativeTextAnalysis.models.Config import ClsConfig, ProcessConfig, TrialConfig, DatasetConfig
from SentimentAnalysis.FigurativeTextAnalysis.models.DatasetHandler import DatasetHandler
from SentimentAnalysis.FigurativeTextAnalysis.models.Trial_ import Trial

__author__ = 'maria'
selected_features =[
                    'postags',              # * // << +
                    "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    "pos_position_"
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    '__LOVE__',               # //
                ]
selected_features_2 =[
                    # 'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    "pos_position_"
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    '__LOVE__',               # //
                ]
selected_features_3 =[
                    'postags',              # * // << +
                    "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    "pos_position_"
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    # '__POS_SMILEY__',           # * // << +
                    # '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]


class TrialTests(unittest.TestCase):

    def combinations_test(self):
        """
        This should lead to :29^29 combinations multiplied by 9 classifiers, so it is not viable.
        source: http://stackoverflow.com/questions/464864/python-code-to-pick-out-all-possible-combinations-from-a-list
        :return:
        :rtype:
        """
        g.logger.info(list(combinations(selected_features, len(selected_features))))
        for i in xrange(1, len(selected_features)+1):
            g.logger.info(list(combinations(selected_features, i)))
            print i

    def incrementally_add_features_test_Test(self):
        """
        Incrementally add features
        :return:None
        :rtype:None
        """
        for i in range(1, len(selected_features)+1):
            print i
            trial = Trial(selected_features[:i],
              g.CLASSIFIER_TYPE.SVMStandalone,
              g.VECTORIZER.Dict,
              g.DISCRETIZATION.ONE,
              "Test",
              # "Final",
              use_tf=True,
              use_cosine=False,
              use_k_best=False,
              )
            trial.classify()
            trial.save_results()

    def incrementally_add_features_test_Final(self):
        """
        Incrementally add features
        :return:None
        :rtype:None
        """
        for i in range(1, len(selected_features)+1):
            print i
            cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict, max_vote=False)
            process_config = ProcessConfig(cls_config, False, selected_features[:i])
            trial_config = TrialConfig(process_config, selected_features[:i], "Tweet_Total")

            trial = Trial(trial_config)
            trial.start()
            trial.get_stats()

    def incrementally_add_and_remove_if_score_suffers_Test(self):
        """
        Incrementally add features and if score decreases do not include the last feature in next run
        :return:None
        :rtype:None
        """
        global selected_features
        previous_score = 0.0
        remove_indexes = []
        ds = DatasetHandler()
        ds.get_data_set()
        ds.get_feature_dicts_and_scores()
        ds.get_test_and_trial_set()
        for i in range(0, len(selected_features)):
            selected_features_mod = copy.copy(selected_features[:i+1])
            for each in remove_indexes:
                selected_features_mod.remove(each)

            print selected_features_mod
            print i
            cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict, max_vote=False)
            process_config = ProcessConfig(cls_config, False, selected_features_mod, ds)
            trial_config = TrialConfig(process_config, selected_features_mod, "Tweet_Total")

            trial = Trial(trial_config)
            trial.start()
            trial.get_stats()
            if previous_score > trial.accuracy:
                print previous_score, trial.accuracy, "going to remove {0}".format(selected_features[i])
                remove_indexes.append(selected_features[i])
            else:
                previous_score = trial.accuracy

    def incrementally_add_and_remove_if_score_suffers_Final(self):
        """
        Incrementally add features and if score decreases do not include the last feature in next run
        :return:None
        :rtype:None
        """
        global selected_features
        previous_score = 0.0
        remove_indexes = []
        ds = DatasetHandler()
        ds.get_data_set()
        ds.get_feature_dicts_and_scores()
        ds.get_test_and_trial_set()
        for i in range(0, len(selected_features)):
            selected_features_mod = copy.copy(selected_features[:i+1])
            for each in remove_indexes:
                selected_features_mod.remove(each)

            print selected_features_mod
            print i
            cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict, max_vote=False)
            process_config = ProcessConfig(cls_config, False, selected_features_mod, ds)
            trial_config = TrialConfig(process_config, selected_features_mod, "Tweet_Total")

            trial = Trial(trial_config)
            trial.start()
            trial.get_stats()
            if previous_score > trial.accuracy:
                print previous_score, trial.accuracy, "going to remove {0}".format(selected_features[i])
                remove_indexes.append(selected_features[i])
            else:
                previous_score = trial.accuracy

    def one_by_one_features_test_Test(self):
        """
        Incrementally add features
        :return:None
        :rtype:None
        """
        for i in range(0, len(selected_features)):
            print i
            cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict, max_vote=False)
            process_config = ProcessConfig(cls_config, False, selected_features[i])
            trial_config = TrialConfig(process_config, selected_features[i], "Tweet_Total")

            trial = Trial(trial_config)
            trial.start()
            trial.get_stats()

    def one_by_one_features_test_Final(self):
        """
        Incrementally add features
        :return:None
        :rtype:None
        """
        for i in range(0, len(selected_features)):
            print i
            cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict, max_vote=False)
            process_config = ProcessConfig(cls_config, False, selected_features[i])
            trial_config = TrialConfig(process_config, selected_features[i], "Tweet_Total")

            trial = Trial(trial_config)
            trial.start()
            trial.get_stats()

    def by_15_features_test(self):
        """
        Combinations by 15
        :return:None
        :rtype:None
        """
        combinations_list = list(combinations(selected_features, 15))
        print len(combinations_list)
        for combination in combinations_list:
            print combination
            cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict, max_vote=False)
            process_config = ProcessConfig(cls_config, False, combination)
            trial_config = TrialConfig(process_config, combination, "Tweet_Total")

            trial = Trial(trial_config)
            trial.start()
            trial.get_stats()

    def figurative_only(self):
        ds_config = DatasetConfig(task11=15000)
        ds = DatasetHandler(ds_config)
        print ds_config
        ds.get_data_set()
        ds.get_feature_dicts_and_scores()
        ds.get_test_and_trial_set()
        cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                               max_vote=False,
                               select_percent=0,
                               use_idf=False,
                               use_hashing=True)
        process_config = ProcessConfig(cls_config, False, selected_features, ds)
        trial_config = TrialConfig(process_config, selected_features, "Tweet_Total")

        trial = Trial(trial_config, 0)
        trial.start()
        trial.get_stats()

        cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                               max_vote=False,
                               select_percent=0,
                               use_idf=False,
                               use_hashing=True)
        process_config = ProcessConfig(cls_config, False, selected_features, ds)
        trial_config = TrialConfig(process_config, selected_features_3, "Tweet_Total")

        trial = Trial(trial_config, 0)
        trial.start()
        trial.get_stats()

        cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                               max_vote=False,
                               select_percent=20)
        process_config = ProcessConfig(cls_config, False, selected_features, ds)
        trial_config = TrialConfig(process_config, selected_features, "Tweet_Total")
        trial = Trial(trial_config, 0)
        trial.start()
        trial.get_stats()

        cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                               max_vote=False,
                               select_percent=20)
        process_config = ProcessConfig(cls_config, False, selected_features, ds)
        trial_config = TrialConfig(process_config, selected_features_3, "Tweet_Total")
        trial = Trial(trial_config, 0)
        trial.start()
        trial.get_stats()

        cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                               max_vote=False,
                               select_percent=0,
                               use_hashing=True,
                               use_idf=True)
        process_config = ProcessConfig(cls_config, False, selected_features_2, ds)
        trial_config = TrialConfig(process_config, selected_features_2, "Tweet_Total")
        trial = Trial(trial_config, 0)
        trial.start()
        trial.get_stats()

    def semeval_task_b_only(self):
        ds_config = DatasetConfig(task2b=15000, task2bdev=10000)
        ds = DatasetHandler(ds_config)
        print ds_config
        ds.get_data_set()
        ds.get_feature_dicts_and_scores()
        ds.get_test_and_trial_set()
        cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                               max_vote=False,
                               select_percent=0,
                               use_idf=False,
                               use_hashing=True)
        process_config = ProcessConfig(cls_config, False, selected_features, ds)
        trial_config = TrialConfig(process_config, selected_features, "Tweet_Total")

        trial = Trial(trial_config, 0)
        trial.start()
        trial.get_stats()

        cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                               max_vote=False,
                               select_percent=60,
                               use_idf=False,
                               use_hashing=True)
        process_config = ProcessConfig(cls_config, False, selected_features, ds)
        trial_config = TrialConfig(process_config, selected_features, "Tweet_Total")

        trial = Trial(trial_config, 0)
        trial.start()
        trial.get_stats()

