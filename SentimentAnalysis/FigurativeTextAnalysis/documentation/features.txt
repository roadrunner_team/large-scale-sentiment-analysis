Επισυνάπτονται κάποια αποτελέσματα της αρχικής υλοποίησης που έχω κάνει.
Έχοντας επεξεργαστεί το κάθε tweet έχω δημιουργήσει για το καθένα, ένα dictionary
με features (περιγράφεται ακολούθως),ώστε να μπορώ να κάνω training χρησιμοποιώντας
τα features που με ενδιαφέρουν κάθε φορά.
Για την ώρα, τα scores που χρησιμοποιώ (από το training/test data set) 
είναι τα round scores, διότι και με Naive Bayes και με SVN, έχω πρόβλημα
στο να δώσω τους real numbers για να γίνει το training. (O svm συγκεκριμένα θεωρεί ότι είναι infeasible το train)
Στα results, η στήλη train αναφέρεται στο εάν το συγκεκριμένο tweet ανήκει
στην train list (true/1) ή στην test list (false/0),
τα score_round0 και score_round1 είναι αντιστοίχως τα αποτελέσματα για training με
στρογγυλοποιημένα initial scores σε 0 και 1 ψηφία. Το score_round0 έχει καλύτερα αποτελέσματα,
δυστυχώς όμως όχι ικανοποιητικά (40% σωστά predicted αποτελέσματα).
Αυτο που παρατηρώ είναι ότι στις περισσότερες περιπτώσεις έχουμε 1 βαθμό διαφορά,
πχ. predicted: -3, initial:-2, οπότε πιθανόν να μπορούν να βελτιωθούν αρκετά με μικρά tweaks.
Όσον αφορά το positive-negative-neutral, σωστή αναγνώριση γίνεται περίπου στο 80% των περιπτώσεων.
Ο classifier που χρησιμοποιώ είναι ο MultinomialNB του scikitlearn.

Επίσης, να σημειώσω πως για να είναι δυνατόν να γίνει το training/ predict,
τα features που σχετίζονται με το SentiWordNet score
(λόγω του ότι μπορεί να πάρουν αρνητική τιμή* και ο classifier δεν το δέχεται),
μετατράπηκαν σε string (κάτι που τελικά φαίνεται να επηρεάζει τα αποτελέσματα).
Στη συνέχεια, σκοπό έχω να ενσωματώσω figurative-speech related features,
(draft: https://coggle.it/diagram/5474458ce16ccf550fa097ed )

feature_dict{
  's_word-{index of word}':SentiWordNet score for word taking POS-tag into account when available,
  'swn_score': Sum(SentiWordNet score for each word)/ count of words that have score,
  'actual word': POS-tag # e.g. 'spy': 'VB',
  'POS_SMILEY': false/true  [0,1],  # presence or absence of positive emoticons
  'NEG_SMILEY': false/true[0,1],    # ------ // ------------ negative emoticons
  'LINK': false/true[0,1],          # ------ // ------------ hyperlinks
  'HT': false/true[0,1],            # ------ // ------------ neutral hashtags (use of SentiWordNet)
  'HT_POS': false/true[0,1],        # ------ // ------------ negative hashtags (use of SentiWordNet)
  'HT_NEG': false/true[0,1],        # ------ // ------------ positive hashtags (use of SentiWordNet)
  'RT': false/true[0,1],            # ------ // ------------ retweet
  'REFERENCE': false/true[0,1],     # ------ // ------------ reference e.g. @user
  'CAPITAL': false/true[0,1],       # ------ // ------------ capitalized words (whole not partial)
  'LOVE': false/true[0,1],          # ------ // ------------ emoticons to indicate love/strong positive emotion
  'LAUGH': false/true[0,1],         # ------ // ------------ laughter and abbreviations
  'NEGATION': false/true[0,1],      # ------ // ------------ any kind of negation
  'word-{index of word}': actual word # after the cleaning of tweet
  't-similarity': path similarity for word pairs  # this needs improvement
}
* Το score του SentiWordNet έχει μετατραπεί από κλίμακα [0,2] σε κλίμακα [-5,5],
ώστε να δώ εάν θα έχει κάποιο αντίκτυπο στο training. Θα δοκιμάσω και με την original
κλίμακα.

EXAMPLE:
"Change British spelling to American spelling or risk being hung as a spy for the Queen."
{
  's_word-9': 0.13,
  's_word-8': 0.0,
  'spy': 'VB',
  's_word-1': 0.0,
  's_word-0': 0.06,
  's_word-3': 0.0,
  's_word-2': 0.63,
  's_word-5': -0.78,
  's_word-7': 0.16,
  's_word-6': 0.0,
  'POS_SMILEY': 0,
  'LINK': 0,
  'RT': 0,
  'swn_score': 0.14,
  'REFERENCE': 0,
  'HT_NEG': 0,
  'word-3': 'American',
  'word-6': 'hung',
  'queen': 'NN',
  'LOVE': 0,
  'risk': 'NN',
  'British': 'JJ',
  'word-1': 'British',
  'HT_POS': 0,
  'American': 'NNP',
  'word-2': 'spelling',
  'word-5': 'risk',
  'HT': 0,
  'word-7': 'spy',
  'CAPITAL': 0,
  'word-9': 'queen',
  'word-8': 'the',
  'spelling': 'NN',
  'change': 'VB',
  'NEG_SMILEY': 0,
  'word-0': 'change',
  'NEGATION': 0,
  't-similarity': 0.11969696969696969,
  'LAUGH': 1,
  'hung': 'VBD',
  'the': 'DT'
}
