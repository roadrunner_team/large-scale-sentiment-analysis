SETUP:
========================================================================================================================
1. Database:
    - Install MySql (e.g. using XAMPP or directly from MySql site) and MySql Workbench.
    - Unzip and run FigurativeTextAnalysis/data/dumps/FigurativeDataDump20141117.sql.zip
    # creates database, tables and populates SentiWordNet, Tweets and data for some Trials I have performed.
    # You can delete all data in Trials, TrialScores and SelectedFeatures if you want.
    - Modify database package (__init__.py) change user and passwd in
      self.conn = MySQLdb.connect(host="localhost", user="root", passwd="", db="SentiFeed")
      to reflect your credentials.

2. Requirements:
    Install the python packages referred in  FigurativeTextAnalysis/documentation/requirements.txt

3. Run:
    - The way FigurativeTextAnalysis is currently constructed: just run Trial.py* (FigurativeTextAnalysis/models/Trial.py)
    In Trial.py there is a selected_features list. To see a change in results, comment/ uncomment this lists values.
    For the time being, results are saved in Trial, SelectedFeatures and Scores table.
    Abstract.png shows some details about how it all works.
    - To run ui: run : python manage.py runserver
    - To run TweetUtils: uncomment examples and

    * if you run this from an IDE like PyCharm just right-click run the file.
    Else, from cmd/terminal run: "/path/to/python2.7 exe” “path/to/Trial.py”
    or "/path/to/python2.7 exe” “path/to/tweet_utils.py” for TweetUtils
    if this throws errors "module somename not found", modify your PYTHONPATH to include the path to the root project
    (/path/to/figurative-text-analysis)

NOTE: TweetUtils will be the library-like implementation of this system.
