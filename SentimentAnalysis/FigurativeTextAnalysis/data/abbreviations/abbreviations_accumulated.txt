<3	Love	This less than three- acronym is used to be a crudely-shaped heart and is used to signify the word love-
2	Too	Short way to say to-, too-, or two- that conserves characters
4	For	Short way to say for- that conserves characters
B4	Before	Short way to say before- that conserves characters
BC	Because
B/C	Because	Short way to say because- that conserves characters
BTW	By The Way  Short way to say by the way- that conserves characters
CHK	Check	Short way to say check- that conserves characters
EM	Email	Short way to say email- that conserves characters
EML	Email	Short way to say email- that conserves characters
FB 	Facebook	Short way to say Facebook- that conserves characters
GR8	Great	Short way to say great- that conserves characters
HT	Hat Tip	This acronym is used when someone wants to virtually give credit to, or tip their hat at someone
IDC	I Don�t Care	Short way to say I don�t care- that conserves characters
IDK	I Don�t Know	Short way to say I don�t know- that conserves characters
IMO	In My Opinion	Short way to say in my opinion- that conserves characters
JK	Just Kidding	Short way to say just kidding- that conserves characters
LI	LinkedIn	Short way to say LinkedIn- that conserves characters
LOL	Laugh Out Loud	People put this acronym when they want to show that they laughed in response
NSFW	Not Safe For Work	This acronym means that the link contains adult material and is like it says, not safe to view at work
PPL	People	Short way to say People- that conserves characters
#RE	Reply	Short way to show that someone is replying to another�s tweet
S/O	Shout Out	This acronym is used when someone wants to give praise or a shout-out to another person on Twitter
TMB	Tweet Me Back	Put in tweets when someone wants others to respond to their tweet
TY	Thank You	Short way to say thank you- that conserves characters
U	You	Short way to say you- that conserves characters
YT	YouTube	Short way to say YouTube- that conserves characters
YW	You�re Welcome	Short way to say you�re welcome- that conserves characters
B4	Before
b/c	Because
bc	Because
BRB	Be Right Back
BTW	By The Way
DM	Direct message	a private message or �direct message� to a person you follow.
EM	Email
EML	Email
Fab	Fabulous
FB	Facebook
FF	Follow Friday	(#followfriday)Twitter �endorsement� Tweeted show the user's favorite people on twitter. (see this cool article on how to automatically populate a follow friday list)
F2F	face to face
B2B	Business to business
B2C	Business to Client	(Business to Community)
FYI	For Your Information
Gr8	Great
GTG	Got To Go
G2G	Got To Go
HT	Heard Through	instead of a classic -RT-
HTH	Happy To Help
IC	I See
IDC	I don't care
IDK	I don�t know
IM	Instant Message
JK	Just Kidding
J/K	Joking
L8	Late
LI	LinkedIn
LMAO	Laughing My A** Off
LMK	Let Me Know
LOL	Laughing Out Loud	not �lots of love� like my mother thinks it is.
#MT 	Modified Tweet
NSFW	Not Safe For Work
OH	Overheard
OMG	Oh My God
OMW 	On My Way
ORLY	Oh Really?!!
ROFL	Rolling On The Floor Laughing
SMH	Shaking My Head
Thx	Thanks
Tx	Thanks
TMI	Too Much Information
Trend	Popular A topic -Trending- or -popular- right now on Twitter
TTYL	Talk To You Later
TTYS	Talk To You Soon
TY	Thank You
YT	YouTube
A/S/L	what is your age, sex, and location?
ADDY	address
AFAIK	as far as I know
AFK	away from keyboard
B	I'm back
BBFN	bye bye for now
BBL	be back later
BF	boyfriend
BFD	big fuckin' deal
BFN	bye for now
BG	big grin
BO	brain overload
BRB	be right back
BTW	by the way
CMIIW	correct me if I'm wrong
CU	see you
CUL8R	see you later
CUL	see you later
CYA	see ya	(good-bye)
EG	evil grin
F2F	face to face    (in person)
FAQ	frequently asked question list
FIIOOH	forget it. I'm out of here
FITB	fill in the blank
FOFLMAO	falling on the floor laughing my ass off
FRP	fantasy role playing
FUBAR	fucked up beyond all recognition
FWIW	for what it's worth
FYI	for your information
G2G	I've got to go
G	grin	(same as a smiley)
GAL	get a life
GD&R	grinning, ducking and running
GF	girlfriend
GG	good game	(if playing an online game)
GRIN	grin	(same as a smiley)
HB	hurry back
HTH	I hope this helps
IANAL	I am not a lawyer
IC	I see
IC	in character	(while role playing)
ICWUM	I see what you mean
IIRC	if I remember correctly
IM	instant message (can be a verb)
IMHO	in my humble opinion
IMNSHO	in my not so humble opinion
IMO	in my opinion
IOW	in other words
IRL	in real life
JK	just kidding
L8R	see you later
LDR	long distance relationship
LMAO	laughing my ass off
LOL	laughing out loud
MOTAS	member of the appropriate sex
MOTOS	member of the opposite sex
MOTSS	member of the same sex
NE1	anyone
NM	never mind
NP	no problem
Ob-	obligatory	(as a prefix)
Objoke	obligatory joke
OIC	oh, I see
OMG	oh, my God!
OOC	out of character	(while role playing)
OS	operating system
OTOH	on the other hand
PAW	parents are watching
PDA	public display of affection
PM	private message
POV	point of view
PUTER	computer
RL	real life
ROFL	rolling on the floor laughing
ROTFL	rolling on the floor laughing
ROTFLMAO	rolling on the floor laughing my ass off
RPG	role playing game
RTFM	read the fucking manual	(before asking a question)
SO	significant other
THX	thanks
TIC	tongue-in-cheek
TMI	too much information
TTFN	ta-ta for now   (good-bye)
TTYL	talk to you later
TTYS	talk to you soon
TY	thank you
TYVM	thank you very much
WB	welcome back
WTF	what the fuck?
WU?	what's up?
WUF?	where are you from?