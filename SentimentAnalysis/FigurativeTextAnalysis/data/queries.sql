UPDATE SentiFeed.TwitterCorpusV2, SentiFeed.TwitterCorpus
SET SentiFeed.TwitterCorpusV2.InitialAspect = SentiFeed.TwitterCorpus.Aspect
WHERE SentiFeed.TwitterCorpusV2.TweetId = SentiFeed.TwitterCorpus.id;

UPDATE SentiFeed.CalculationMethod
SET
Accuracy = {0},
CombinedAccuracy = <{CombinedAccuracy: 0}>,
AspectAccuracy = <{AspectAccuracy: 0}>,
AspectScoreAccuracy = <{AspectScoreAccuracy: 0}>,
DateLastEvaluated = <{DateLastEvaluated: }>
WHERE id = <{expr}>;


CREATE TABLE SentiFeed.ManualTwitterCorpus
(
    id int PRIMARY KEY NOT NULL,
    TweetId bigint  NOT NULL,
    TweetText varchar (140) NOT NULL,
    CreateDate varchar (150) NOT NULL,
    Query varchar (100) NOT NULL,
    User varchar (100) NOT NULL
);
ALTER TABLE SentiFeed.ManualTwitterCorpus ADD CONSTRAINT unique_id UNIQUE (id);
ALTER TABLE SentiFeed.ManualTwitterCorpus
CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT ;


CREATE TABLE TwitterCorpusNoEmoticons (
  id int(11) NOT NULL AUTO_INCREMENT,
  TweetId bigint(20) NOT NULL,
  TweetText varchar(140) NOT NULL,
  CreateDate varchar(150) NOT NULL,
  Query varchar(100) NOT NULL,
  User varchar(100) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY unique_id (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO SentiFeed.Source (SourceTable) VALUES ('TwitterCorpus');
INSERT INTO SentiFeed.Source (SourceTable) VALUES ('ManualTwitterCorpus');
INSERT INTO SentiFeed.Source (SourceTable) VALUES ('TwitterCorpusNoEmoticons');


CREATE TABLE SentiFeed.ProcessedTweets (
  id INT NOT NULL AUTO_INCREMENT,
  TweetId BIGINT NOT NULL,
  TweetInitial VARCHAR(150) NULL,
  TweetProcessed VARCHAR(150) NULL,
  Hashtags VARCHAR(150) NULL,
  Capitals VARCHAR(150) NULL,
  Negations VARCHAR(150) NULL,
  PRIMARY KEY (id, TweetId));

CREATE TABLE SlangTranslation (
  Id int(11) NOT NULL AUTO_INCREMENT,
  SlangTerm varchar(150) NOT NULL,
  SlangTranslation varchar(1000) DEFAULT NULL,
  SlangScore float DEFAULT NULL,
  PRIMARY KEY (Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

############ Convert SWN Scale ################
UPDATE SentiFeed.SentiWordNetTemp
SET ConvertedScale = (((SentimentAssesment- 0.0)*5.0)/1.0)+ -5.0


CREATE TABLE `sentifeed`.`metaphors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tweet_id` BIGINT(22) NULL,
  `tweet_text` VARCHAR(200) NULL,
  `feature_dict` LONGTEXT NULL,
  `cleaned_text` VARCHAR(200) NULL,
  `created_at` DATETIME NULL,
  `source` VARCHAR(250) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `sentifeed`.`nrchashtaglexicon` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `term` VARCHAR(200) NULL,
  `score` FLOAT NULL,
  `n_pos` INT NULL,
  `n_neg` INT NULL,
  `neg` VARCHAR(200) NULL,
  `neg_first` VARCHAR(200) NULL,
  `dataset` VARCHAR(200) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `sentifeed`.`abbreviations` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `abbreviation` VARCHAR(150) NULL,
  `expansion` VARCHAR(500) NULL,
  `meaning` VARCHAR(1000) NULL,
  PRIMARY KEY (`id`));

-- Table to store Classifier models
CREATE TABLE `SentiFeed`.`ClassifierModels` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cls_name` VARCHAR(200) NULL,
  `dataset` VARCHAR(500) NULL,
  `selected_features` LONGTEXT NULL,
  `model` LONGTEXT NULL,
  `datetime_trained` DATETIME NULL,
  PRIMARY KEY (`id`));


-- Table to store all datasets
CREATE TABLE `Tweet_Total` (
  `id` bigint(20) NOT NULL,
  `text` varchar(250) DEFAULT NULL,
  `clean_text` varchar(250) DEFAULT NULL,
  `tagged_text` varchar(500) DEFAULT NULL,
  `pos_tagged_text` varchar(500) DEFAULT NULL,
  `tags` varchar(1000) DEFAULT NULL,
  `initial_score` float DEFAULT NULL,
  `sentences` varchar(250) DEFAULT NULL,
  `words` varchar(500) DEFAULT NULL,
  `links` varchar(250) DEFAULT NULL,
  `corrected_words` varchar(150) DEFAULT NULL,
  `smileys_per_sentence` varchar(150) DEFAULT NULL,
  `uppercase_words_per_sentence` varchar(250) DEFAULT NULL,
  `reference` varchar(150) DEFAULT NULL,
  `hash_list` varchar(150) DEFAULT NULL,
  `non_word_chars_removed` varchar(250) DEFAULT NULL,
  `negations` varchar(150) DEFAULT NULL,
  `swn_score_dict` varchar(1000) DEFAULT NULL,
  `swn_score` float DEFAULT NULL,
  `feature_dict` longtext,
  `score` float DEFAULT NULL,
  `train` int(11) DEFAULT '1',
  `round_score0` float DEFAULT NULL,
  `round_score1` float DEFAULT NULL,
  `round_score2` float DEFAULT NULL,
  `pnn` varchar(45) DEFAULT NULL,
  `modified` varchar(45) DEFAULT 'False',
  `polarity` varchar(45) DEFAULT NULL,
  `words_to_swn_score_dict` longtext,
  `date_updated` datetime DEFAULT NULL,
  `source` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


UPDATE `SentiFeed`.`Tweet_Total`
SET `initial_score` = -1
WHERE `initial_score` = 0 and source = "1.6M";

DELETE from SentiFeed.Tweet_Total where source = "1.6M" and initial_score >1;

