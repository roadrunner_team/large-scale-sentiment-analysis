__author__ = 'm.karanasou'
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g

def insert_abbreviations_in_db():
    abbrev = []
    q = """
    INSERT INTO `sentifeed`.`abbreviations`
            (`abbreviation`,
            `expansion`,
            `meaning`)
            VALUES
            (
            "{0}",
            "{1}",
            "{2}");
    """
    seen = []
    with open("../data/abbreviations/abbreviations_accumulated.txt", 'rb') as abbrev_file:
        for line in abbrev_file.readlines():
            split_line = line.lower().replace("\r", "").replace("\n", "").split("\t")
            if split_line[0] not in seen:
                seen.append(split_line[0])
                abbrev.append(split_line)

    for each in abbrev:
        print each
        if len(each) > 2:
            g.mysql_conn.update(q.format(each[0], each[1], each[2]))
        else:
            g.mysql_conn.update(q.format(each[0], each[1], ""))


def parse_abbreviations():
    q = """
    INSERT INTO `sentifeed`.`abbreviations`
            (`abbreviation`,
            `expansion`,
            `meaning`)
            VALUES
            (
            "{0}",
            "{1}",
            "{2}");
    """
    abbrev = []
    seen = []
    with open('../data/abbreviations/abbreviations.csv', 'rb') as abbrev_file:
        for line in abbrev_file.readlines():
            split_line = line.split(",")
            term = split_line[0].replace(".", "")
            deployed_term = split_line[1].replace("\n", "")\
                                        .replace("\"", "")\
                                        .replace("(in the Bible) ", "")\
                                        .replace("(s)", "")\
                                        .replace("(n)", "")\
                                        .replace("(ly)", "")\
                                        .replace(" (to)", "")\
                                        .replace(" (dialect)", "")\
                                        .replace(" (with)", "")\
                                        .replace("(al)", "")\
                                        .replace(" (of)", "")\
                                        .replace("(in dates) ", "")\
                                        .replace("(in the Apocrypha) ", "")
            if term not in seen:
                seen.append(term)
                abbrev.append([term, deployed_term])

    for each in abbrev:
        g.mysql_conn.update(q.format(each[0], each[1], ""))
