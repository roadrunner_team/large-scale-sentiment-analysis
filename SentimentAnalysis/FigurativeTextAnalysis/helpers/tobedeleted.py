from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from FigurativeTextAnalysis.helpers.globals import g

__author__ = 'm.karanasou'
from nltk.corpus import wordnet as wn

def test_helper_classifier__NB():
    train_data =["Name", "FirstName", "FName", "LName", "LastName", "SName"]
    # test_data = g.mysql_conn.execute_query(self.q_text.format(0))
    match = 0.0

    x_train = ["Name", "FirstName", "FName", "LName", "LastName", "SName"]
    y_train = ["HumanName", "HumanName", "HumanName", "HumanSurname", "HumanSurname", "HumanSurname"]
    x_test = ["firstN", "s_name"]
    y_test = ["HumanName", "HumanSurname"]
    cls = MultinomialNB()
    # vectorizer = CountVectorizer(strip_accents='unicode')
    vectorizer = TfidfVectorizer(use_idf=False)
    X = vectorizer.fit_transform(x_train)
    cls.fit(X, y_train)
    xx = vectorizer.transform(x_test)
    predictions= cls.predict(xx)
    for i in range(0, len(predictions)):
        if predictions[i] == y_test[i]:
            match += 1.0
    print predictions
    print match, len(predictions),  match/float(len(predictions))

    print wn.synset('{0}.n.01'.format("person")).lemmas()
    print wn.synset('{0}.n.01'.format("soul")).lemmas()
    print wn.synset('{0}.n.01'.format("name")).lemmas()


def update_swn_rank():
    q = 'UPDATE sentifeed.sentiwordnet SET rank="{0}", updated="{1}" WHERE SentiWordNetTempId="{2}" and Category="{3}" and SysTerms="{4}";'
    i = 0
    with open("../data/swn_corpus/SentiWordNet_3.0.0_20130122.txt", "r") as swn_file:
        for line in swn_file.readlines():
            i += 1
            split_line = line.strip().split("\t")
            category = split_line[0]
            id = split_line[1]

            for synset in split_line[4].split(" "):
                split_synset = synset.split("#")
                syns_name = split_synset[0]
                rank = split_synset[1].replace("#", "")
                g.mysql_conn.update(q.format(rank, 1, id, category, syns_name.replace("'", "\'")))
            print i

# test_helper_classifier__NB()
# update_swn_rank()