import ast
import csv
from decimal import Decimal
from random import shuffle
import re

# import requests
import traceback
import datetime
from sklearn import cross_validation
from SentimentAnalysis.FigurativeTextAnalysis.models.Classifier_ import Classifier
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from SentimentAnalysis.FigurativeTextAnalysis.models.Config import ClsConfig

__author__ = 'maria'

word_list = ['juuusttt', 'a', 'testyy', 'looooveee', 'tweet']
'''counter = 0
duplicates_in_word = 0

for word in list:
    counter = 0
    duplicates_in_word = 0
    final_word = ''
    print "initial word = "+word
    for i in range(0, word.__len__()-1):  #for each character in this word
        print i, word[i]

        if word[i] == word[i+1]:#if the i-th character is equal with the previous
            if counter <= 2:# and no duplicates till now
                print word[i], word[i+1]
                final_word +=word[i+1] #then it's ok to keep all characters till now
            if (i == word.__len__()-2):
                if word[i]== word.__len__()-1:
                    print "blah",i
                    final_word += word[i+1]
        else:
            final_word += word[i]
            counter += 1                #increase duplicate counter

    print final_word
'''
'''        else:                           # else if i-th character is not equal to the previous
            if counter > 1 :            #check if counter indicates that before i there were more than 2 letters in row
                print "duplicate indicator, do sth"
                print "dupl counter:", duplicates_in_word
                duplicates_in_word+=1   # this counter indicates how many unique duplicates in word, e.g. in juuuustttt there are two 'u' and 't'

                if duplicates_in_word >1:# if it is the second unique
                    final_word = word[i-counter-2:]
                    print "dupl >1"
                else:
                    for i in range(counter-2, counter+1):
                        final_word = word[:i-counter+1] + word[counter+1:]
                    print "dupl <1 "

                print "dupl counter:", duplicates_in_word
            else:
                final_word = word
            counter = 0                 #reset counter and go for the next letter in word'''

def remove_multiples():
    """
    assume that two characters in a row are valid. If not they should be corrected in spell-checking
    :return:
    """
    counter = 0
    final_word = ''
    print word_list.__len__()
    #for l in range(0, word_list.__len__()):
    for word in word_list:
        for i in range(1, word.__len__()):
            if word[i - 1] == word[i]:
                counter += 1
            else:
                if counter > 2:
                    print "duplicate indicator, do sth"
                    for j in range(counter - 2, counter + 1):
                        print word[:j - counter + 1] + word[counter + 1:-1]
                        final_word += word[:j - counter + 1] + word[counter + 1:-1]
                        print final_word
                counter = 0


def remove_multiples_v2(word_list):
    for word in word_list:
        multi_counter = 0
        index_list = []
        l_word = list(word)
        if l_word.__len__() > 2:  # It doesn' t make sense to look for multiples in words with less than 3 characters
            for i in range(0, l_word.__len__()):
                try:
                    # if this letter equals the next letter then increment multi_counter
                    if l_word[i] == l_word[i+1]:
                        multi_counter += 1
                    else:
                        multi_counter = 0

                except IndexError:
                    if l_word[i] == l_word[i-1] and l_word[i] == l_word[i-2]:
                        multi_counter += 1
                    continue
                try:
                    # if more than 2 duplicates, keep the index of letter to be removed
                    if multi_counter > 1:
                        print 'multiples {0} {1}'.format(l_word[i+1], i+1)
                        index_list.append(i+1)
                except IndexError:
                    if l_word[i] == l_word[i-1] and multi_counter > 1:
                        index_list.append(i)
                    pass
        # in order to avoid index errors, start removing from the end of the word
        reverse_indexes = sorted(index_list, reverse=True)
        for each in reverse_indexes:
            l_word.remove(l_word[each])

        final_word = ''.join(l_word)
        print final_word

def removeRT(text):
    rt = re.findall('^RT\s{0,1}(@\w+:){0,1}', text)
    if rt!=[]:
        print rt
        text = re.sub('^RT:{0,1}', '', text)
        print(text)

#removeRT('RT: @someone:test etbwAESJBF GRDFG NNNnubga ret rt')

def laughter(text):
    found_haha = re.findall(r'((o(m)*(g)*)|o-m-g|o\sm\sg)', text)
    print found_haha

######################################################################

def streaming():
    s = requests.Session()

    headers = {'connection': 'keep-alive', 'content-type': 'application/json', 'x-powered-by': 'Express', 'transfer-encoding': 'chunked'}
    req = requests.Request("GET",'http://127.0.0.1:8000/static/data/big.txt',
                           headers=headers).prepare()

    resp = s.send(req, stream=True)

    for line in resp.iter_lines():
        if line:
            yield line


def read_stream():

    for line in streaming():
        print line


def test_swn_rank():
    final = 0.0
    rank = 0.0
    from globals import g
    q = g.sum_query_figurative_scale_equals_rank_test().format("love", "and Category = 'v' ")
    print q
    score = g.mysql_conn.execute_query(q)
    print score
    for each in score:
        rank_ = int(each[1])
        final += (each[0]/rank_)
        rank += float(1.0/rank_)
    print final/rank


def _get_metaphor_classifier():
    tmp = Classifier(g.CLASSIFIER_TYPE.SVMStandalone, g.VECTORIZER.Dict)
    q_metaphor = "SELECT tweet_text FROM SentiFeed.Metaphors;"
    q_others_positive = "SELECT TweetText FROM SentiFeed.TwitterCorpusNoEmoticons where EmotionT = 'positive' limit 3225;"
    q_others_negative = "SELECT TweetText FROM SentiFeed.TwitterCorpusNoEmoticons where EmotionT = 'negative'  limit 3225;"

    met_data = [x[0] for x in g.mysql_conn.execute_query(q_metaphor)]
    oth_data_positive = [x[0] for x in g.mysql_conn.execute_query(q_others_positive)]
    oth_data_negative = [x[0] for x in g.mysql_conn.execute_query(q_others_negative)]

    t_met_data = [[x, True] for x in met_data]
    t_oth_pos_data = [[x, False] for x in oth_data_positive]
    t_oth_neg_data = [[x, False] for x in oth_data_negative]

    final_data = t_met_data + t_oth_neg_data + t_oth_pos_data

    tmp.set_y_train([x[1] for x in final_data])
    tmp.set_x_train([x[0] for x in final_data])
    tmp.train()
    tmp.set_x_trial(["He is the apple of my eye", "I love being out in the rain!", "he is the light of my soul"])
    print tmp.predict()

def test_str_dict_classifier():
    tmp = Classifier(g.CLASSIFIER_TYPE.SVMStandalone, g.VECTORIZER.Count, use_tf_transformer=True)
    # q_metaphor = "SELECT tweet_text FROM SentiFeed.Metaphors;"


    q_train = "select initial_score, feature_dict " \
                "from SentiFeed.TweetTestData where train=1;"
    q_test = "select initial_score, feature_dict " \
                "from SentiFeed.TweetTestData where train=0;"

    data_train = [x for x in g.mysql_conn.execute_query(q_train)]
    data_test = [x for x in g.mysql_conn.execute_query(q_test)]
    y_train = []
    y_trial = []

    for x in data_train:
        # y_train.append(round(x[0], 0))
        if x[0] > 0:
            y_train.append(2)
        elif x[0] < 0:
            y_train.append(0)
        else:
            y_train.append(1)

    for x in data_test:
        # y_trial.append(round(x[0], 0))
        if x[0] > 0:
            y_trial.append(2)
        elif x[0] < 0:
            y_trial.append(0)
        else:
            y_trial.append(1)

    tmp.set_y_train(y_train)
    # tmp.set_x_train([ast.literal_eval(x[1]) for x in data_train])
    tmp.set_x_train([x[1] for x in data_train])
    tmp.train()
    tmp.set_y_trial(y_trial)
    # tmp.set_x_trial([ast.literal_eval(x[1]) for x in data_test])
    tmp.set_x_trial([x[1] for x in data_test])

    predictions = tmp.predict()
    match = 0
    for i in range(0, len(predictions)):
        if y_trial[i] == predictions[i]:
            match += 1
        # print y_trial[i],  " prediction is: ", predictions[i]

    print float(match)/ float(len(predictions)), len(predictions)


def test_pos_neg_cls():

    q_others_positive = "SELECT EmotionT, FeatureDictionary FROM SentiFeed.TwitterCorpusNoEmoticons where EmotionT = 'positive' limit 8225;"
    q_others_negative = "SELECT EmotionT, FeatureDictionary FROM SentiFeed.TwitterCorpusNoEmoticons where EmotionT = 'negative'  limit 8225;"

    tmp = Classifier(g.CLASSIFIER_TYPE.SVMStandalone, g.VECTORIZER.Dict, use_tf_transformer=True)


    data = [x for x in g.mysql_conn.execute_query(q_others_positive)] + [x for x in g.mysql_conn.execute_query(q_others_negative)]
    shuffle(data)

    data_train = [x for x in data[:12800]]
    data_test = [x for x in data[12800:]]
    y_train = []
    y_trial = []

    for x in data_train:
        y_train.append(x[0])

    for x in data_test:
        y_trial.append(x[0])

    tmp.set_y_train(y_train)
    tmp.set_x_train([ast.literal_eval(x[1]) for x in data_train])
    # tmp.set_x_train([x[1] for x in data_train])
    tmp.train()
    # tmp.set_y_trial(y_trial)
    tmp.set_x_trial([ast.literal_eval(x[1]) for x in data_test])
    tmp.set_x_trial([x[1] for x in data_test])

    predictions = tmp.predict()
    match = 0
    for i in range(0, len(predictions)):
        if y_trial[i] == predictions[i]:
            match += 1
        print y_trial[i],  " prediction is: ", predictions[i]

    print float(match) / float(len(predictions)), len(predictions)


def import_semeval2013_taks2_data():
    q_insert = '''INSERT INTO tweettestdata_semeval2013task2b_dev
                                (id,
                                text,
                                initial_score)
                                VALUES
                                ("{0}",
                                "{1}",
                                "{2}"
                                );''';


    f_in = '../data/semeval2013/task2/training_data_dev_b.txt'
    f_scores = '../data/semeval2013/task2/twitter-train-full-B.tsv'
    emotions_score = {
        "positive": 1,
        "negative":-1,
        "neutral": 0,
        "objective": 0,
        "objective-OR-neutral": 0
    }
    tweets = []
    emotions = []

    with open(f_scores) as scores:
        for line in scores.readlines():
            _id, code, em = line.strip("\r\n").split("\t")
            emotions.append([_id, em])

    with open(f_in) as a:
        for line in a.readlines():
            _id, tweet, e = line.strip("\r\n").split("\t")
            temp_emotion = 5
            # for em in emotions:
                # if em[0] == _id:
            temp_emotion = emotions_score[e.replace("\"", "")]
            if temp_emotion == 5:
                print _id

            tweets.append([_id, tweet, temp_emotion])

    for each in tweets:
        g.mysql_conn.execute_query(q_insert.format(each[0], each[1].replace("\"", "\'"), each[2]))


def insert_1_6M_data():
    q_insert = '''INSERT INTO tweet1_6m_positive_negative
                                (id,
                                text,
                                initial_score)
                                VALUES
                                ("{0}",
                                "{1}",
                                "{2}"
                                );''';

    f = open('../data/1.6M/training.1600000.processed.noemoticon.csv', 'rt')
    reader = csv.reader(f)
    for score, id, date, query, user, text in reader:
        try:
            # g.mysql_conn.execute_query(q_insert.format(id, text + (" :) " if score > 2 else " :( " if score < 2 else ""), score))
            g.mysql_conn.execute_query(q_insert.format(id, text, score))
        except:
            print id, score

            g.logger.debug([score, id, date, query, user, text])
            pass


def update_duplicates_1_6M():
    f = open('../data/1.6M/training.1600000.processed.noemoticon.csv', 'rt')
    q_ =""" SELECT * FROM tweet1_6m_positive_negative where id={0} and initial_score={1}"""
    q_upd =""" UPDATE tweet1_6m_positive_negative SET initial_score=5 where id={0};"""
    reader = csv.reader(f)
    for score, id, date, query, user, text in reader:
        exists = g.mysql_conn.execute_query(q_.format(id, score))
        if len(exists) == 0:
            g.mysql_conn.execute_query(q_upd.format(id))
            print id


def find_differences_between_feature_dicts_for_same_tweet():
    q1 = """SELECT feature_dict FROM SentiFeed.TweetTestData """
    q2 = """SELECT feature_dict FROM SentiFeed_bak.TweetTestData """

    new = g.mysql_conn.execute_query(q1)
    old = g.mysql_conn.execute_query(q2)



    for i in range(0, len(new)):
        dict_old = ast.literal_eval(old[i][0])
        dict_new = ast.literal_eval(new[i][0])
        for k,v in dict_new.items():
            if "__" in k:
                tmp = dict_new[k]
                new_k = k.replace("__", "")
                del dict_new[k]
                dict_new[new_k] = tmp

        unmatched_items = set(dict_old.items()) ^ set(dict_new.items())
        g.logger.debug("old:\t{0}".format(old[i][0]))
        g.logger.debug("new:\t{0}".format(new[i][0]))
        for k, v in sorted(unmatched_items):
            g.logger.debug("unmatched:\t{0}||{1}".format(k, v))


def insert_into_tweet_total():
    # source = ["Task11", "Task11", "Task2b", "1.6M", "Manual", "Task2bDev"]
    source = ["1.6M"]
    # tables = ["TweetTestData", "TweetFinalTestData", "tweettestdata_semeval2013task2b", "tweet1_6m_positive_negative", "tweet_manual_data", "tweettestdata_semeval2013task2b_dev"]
    tables = ["tweet1_6m_positive_negative"]

    for i in xrange(0, len(source)):
        q_s = """ SELECT id, text, initial_score, train, modified from SentiFeed.{0};""".format(tables[i])
        q_ins = """ INSERT INTO `SentiFeed`.`Tweet_Total`
                    (`id`,
                    `text`,
                    `initial_score`,
                    `train`,
                    `modified`,
                    `date_updated`,
                    `source`)
                    VALUES
                    (
                    {0},
                    "{1}",
                    {2},
                    {3},
                    "{4}",
                    "{5}",
                    "{6}"
                    );
                    """
        data = g.mysql_conn.execute_query(q_s)

        for row in data:
            try:
                score = -1 if row[2] == -1 else 1 if row[2] == 4 else 5
                text = row[1].replace("\\","\\\\").replace("\"", "\'") + (" :) " if score == 1 else " :( " if score == -1 else "")
                g.mysql_conn.execute_query(q_ins.format(row[0], text, row[2], row[3],
                                                    str(datetime.datetime.now()),
                                                    str(datetime.datetime.now()),
                                                    source[i]))
            except:
                print q_ins.format(row[0], row[1].replace("\"", "\'"), row[2], row[3],
                                                    str(datetime.datetime.now()),
                                                    str(datetime.datetime.now()),
                                                    source[i])
                pass


# insert_into_tweet_total()


def evaluate_metaphor():
    from sklearn.metrics import classification_report
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Count, max_vote=False)
    metaphor_cls = Classifier(cls_config)
    q_metaphor = "SELECT tweet_text FROM SentiFeed.Metaphors;"
    q_others_positive = "SELECT text FROM SentiFeed.tweet_total where source='1.6M' and initial_score > 0  limit 3225 OFFSET 20000;"
    q_others_negative = "SELECT text FROM SentiFeed.tweet_total where source='1.6M' and initial_score < 0  limit 3225 OFFSET 20000;"

    met_data = [x[0] for x in g.mysql_conn.execute_query(q_metaphor)]
    oth_data_positive = [x[0] for x in g.mysql_conn.execute_query(q_others_positive)]
    oth_data_negative = [x[0] for x in g.mysql_conn.execute_query(q_others_negative)]

    t_met_data = [[x, True] for x in met_data]
    t_oth_pos_data = [[x, False] for x in oth_data_positive]
    t_oth_neg_data = [[x, False] for x in oth_data_negative]

    final_data = t_oth_neg_data + t_met_data + t_oth_pos_data
    print final_data[0]
    shuffle(final_data)
    ts = [x[0] for x in final_data]
    tss = [x[1] for x in final_data]
    test_set_data, \
    trial_set_data, \
    test_set_scores, \
    trial_set_scores = cross_validation.train_test_split( ts,
                                                          tss,
                                                          test_size=0.4,
                                                          random_state=5)

    print len(trial_set_data), len(test_set_data)
    metaphor_cls.set_y_train(test_set_scores)
    metaphor_cls.set_x_train(test_set_data)
    metaphor_cls.train()
    metaphor_cls.set_y_trial(trial_set_scores)
    metaphor_cls.set_x_trial(trial_set_data)
    predictions = metaphor_cls.predict()
    print classification_report(trial_set_scores, predictions)


def insert_neutral():
    duplicates = []
    q = """ INSERT INTO `SentiFeed`.`Tweet_Total`
                    (`id`,
                    `text`,
                    `initial_score`,
                    `train`,
                    `modified`,
                    `date_updated`,
                    `source`)
                    VALUES
                    (
                    {0},
                    "{1}",
                    {2},
                    {3},
                    "{4}",
                    "{5}",
                    "{6}"
                    );
                    """


    with open('../data/feedback/all_tweets_for_preprocess.csv') as manual:
        reader = csv.reader(manual,delimiter=',')
        i = 0
        for row in reader:
            # split_line = line.strip("\n").replace("\"","\\\"")
            # index = 2 if split_line[0] == "-" else 1
            try:
                # if split_line[2:] not in duplicates:

                    # print split_line[0], index
                    # print split_line[index + 1:]
                print row[0] + str(i)
                g.mysql_conn.execute_query(q.format(str(row[0][:5]) + str(i),  row[1].strip("\n").replace("\"","\\\""), row[2], 1,
                                                        str(datetime.datetime.now()),
                                                        str(datetime.datetime.now()),
                                                        "Feedback"))
                # duplicates.append(split_line[2:])
                i += 1
            except:
                print q.format(row[0] + str(i),  row[1].strip("\n").replace("\"","\\\""), row[2], 1,
                                                            str(datetime.datetime.now()),
                                                            str(datetime.datetime.now()),
                                                            "Feedback")
                raise
        print i


# insert_neutral()
# insert_neutral()
# evaluate_metaphor()