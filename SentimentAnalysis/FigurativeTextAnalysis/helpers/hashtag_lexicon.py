import re

__author__ = 'mariakaranasou'

from globals import g

def read_files_into_db():

    unigrams = '../data/HashtagSentimentAffLexNegLex/HS-AFFLEX-NEGLEX-unigrams.txt'
    bigrams = '../data/HashtagSentimentAffLexNegLex/HS-AFFLEX-NEGLEX-bigrams.txt'

    lst = [unigrams, bigrams]
    name_lst = ["unigrams", "bigrams"]

    NEG = "_NEG"
    NEGFIRST = "_NEGFIRST"

    q = """INSERT INTO `SentiFeed`.`NRCHashtagLexicon`
            (
            `term`,
            `score`,
            `n_pos`,
            `n_neg`,
            `neg`,
            `neg_first`,
            `dataset`)
            VALUES
            (
            "{0}",
            "{1}",
            "{2}",
            "{3}",
            "{4}",
            "{5}",
            "{6}"
            );
          """
    i = 0
    for each in lst:
        name = name_lst[lst.index(each)]
        with open(each, 'rb') as fl:
            for line in fl.readlines():
                split_line = line.replace("\n", "").split("\t")
                neg = re.findall(NEG, split_line[0])
                neg_first = re.findall(NEGFIRST, split_line[0])

                final_term = split_line[0].replace(NEG, "").replace(NEGFIRST, "")

                q_formatted = q.format(final_term,
                                        split_line[1],
                                        split_line[2],
                                        split_line[3],
                                        len(neg) if neg is not None else 0,
                                        len(neg_first) if neg is not None else 0,
                                        name)
                # print q_formatted

                g.mysql_conn.update(q_formatted)

                i += 1
                if i % 100:
                    print i
