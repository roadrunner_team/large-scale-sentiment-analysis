__author__ = 'mariakaranasou'

from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g

def import_metaphors_in_db(metaphor_file):
    results = []
    q = """INSERT INTO `SentiFeed`.`Metaphors`
            (
           `tweet_id`,
            `tweet_text`,
            `feature_dict`,
            `cleaned_text`,
            `created_at`,
            `source`)
            VALUES
            (
            "{0}",
            "{1}",
            "{2}",
            "{3}",
            "{4}",
            "{5}");
            """
    with open(metaphor_file, "rb") as mf:
        for line in mf.readlines():
            split_line = line.strip("\r\n").split(",")
            id = split_line[0]
            created_at = split_line[1]
            text = split_line[2].replace("\"", "\'")

            # print id, created_at, text
            results.append([id, created_at, text])
    for tweet in results:
        g.mysql_conn.update(q.format(tweet[0], tweet[2],"null", "null",  tweet[1], metaphor_file))

import_metaphors_in_db('../data/metaphors/MetaphorMagnet_tweets.csv')
import_metaphors_in_db('../data/metaphors/metaphorminute_tweets.csv')