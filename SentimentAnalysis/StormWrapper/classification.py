import json
from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
from SentimentAnalysis.FigurativeTextAnalysis.models.Classifier import Classifier
import storm

__author__ = 'mariakaranasou'

cls = Classifier(cls_type=g.CLASSIFIER_TYPE.SVMStandalone,
                 vectorizer_type=g.VECTORIZER.Dict,
                 use_pairwise_cosine=False,
                 use_tf_transformer=True,
                 use_k_best=True)
cls.load_model()


class ClassificationBolt(storm.BasicBolt):
    """
    {
    // The tuple's id - this is a string to support languages lacking 64-bit precision
    "id": "-6955786537413359385",
    // The id of the component that created this tuple
    "comp": "1",
    // The id of the stream this tuple was emitted to
    "stream": "1",
    // The id of the task that created this tuple
    "task": 9,
    // All the values in this tuple
    "tuple": ["snow white and the seven dwarfs", "field2", 3]
    }
    :param args:
    :return:
    """
    def process(self, tup):
        global cls
        stream_tweet = json.loads(tup.values[0])
        cls.set_x_trial([stream_tweet.feature_dict])
        stream_tweet.predicted_score = cls.predict()[0]
        storm.emit([stream_tweet])

ClassificationBolt().run()
