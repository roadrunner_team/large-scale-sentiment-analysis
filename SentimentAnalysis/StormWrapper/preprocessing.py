import json
from SentimentAnalysis.TweetUtils.models.config import Config
from SentimentAnalysis.TweetUtils.tweet_utils import TweetUtils
import storm
from SentimentAnalysis.TweetUtils.helpers.globals import g

__author__ = 'mariakaranasou'


class PreprocessingBolt(storm.BasicBolt):
    """
    {
    // The tuple's id - this is a string to support languages lacking 64-bit precision
    "id": "-6955786537413359385",
    // The id of the component that created this tuple
    "comp": "1",
    // The id of the stream this tuple was emitted to
    "stream": "1",
    // The id of the task that created this tuple
    "task": 9,
    // All the values in this tuple
    "tuple": ["snow white and the seven dwarfs", "field2", 3]
    }
    :param args:
    :return:
    """
    def process(self, tup):
        stream_tweet = json.loads(tup.values[0])
        utils = TweetUtils(Config(True, True))
        tweet = utils.process(str(stream_tweet["text"].encode("utf-8")))
        tweet.original_tweet = stream_tweet
        storm.emit([json.dumps(tweet.serialize())])

PreprocessingBolt().run()