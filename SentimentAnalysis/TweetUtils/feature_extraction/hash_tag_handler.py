import nltk
from SentimentAnalysis.TweetUtils.feature_extraction.name_helper import NameHelper

__author__ = 'maria'

import re
from SentimentAnalysis.TweetUtils.helpers.globals import g
import SentimentAnalysis.TweetUtils.helpers.pyenchant_spell_checker as sp_ch


class HashTagHandler(object):
    """
    The idea is to:
    1. First check if whole hashtag consists of CAPITALS
        if yes: then have a check to tell if it is spelled correctly
        else: goto 3.
    2. Try split hashtag by Capitals, e.g.: #ThisIsAHashtag ==> This Is A Hasthag
        - have a check to tell if every word is spelled correctly
        else: goto 3.
    3. Spellcheck those who are not
    """

    def __init__(self):
        self.hashtag = None
        self.corrected_hashtag = None
        self.spell_checker = sp_ch.EnchantSpellChecker()
        self.spell_checker.dict_exists('en')
        self.handled_hashtag = []
        self.name_helper = NameHelper(pos_tag=False)
        self.lancaster_stemmer = nltk.LancasterStemmer()

    def _set_hasthag(self, hashtag):
        self.hashtag = hashtag
        self.handled_hashtag = []

    def handle(self, hashtag, type=None):
        self._set_hasthag(hashtag.replace("#", ""))
        self.corrected_hashtag = self.name_helper.get_name(self.hashtag)

        for word in self.corrected_hashtag:
            if type is None:
                self.handled_hashtag.append(self._get_score_for_hasthag(word))
            else:
                self.handled_hashtag.append(self.calculate_from_ht_lexicon(word))

        return self._calculate_result()

    def _split_all_lower(self, hashtag):
        """
        source: http://stackoverflow.com/questions/8870261/how-to-split-text-without-spaces-into-list-of-words
        :param hashtag:
        :return:
        """

        """Uses dynamic programming to infer the location of spaces in a string
        without spaces."""

        # Find the best match for the i first characters, assuming cost has
        # been built for the i-1 first characters.
        # Returns a pair (match_cost, match_length).
        def best_match(i):
            candidates = enumerate(reversed(cost[max(0, i-g.max_word):i]))
            return min((c + g.word_cost.get(hashtag[i-k-1:i], 9e999), k+1) for k, c in candidates)

        # Build the cost array.
        cost = [0]
        for i in range(1, len(hashtag)+1):
            c,k = best_match(i)
            cost.append(c)

        # Backtrack to recover the minimal-cost string.
        out = []
        i = len(hashtag)
        while i > 0:
            c, k = best_match(i)
            assert c == cost[i]
            out.append(hashtag[i-k:i])
            i -= k

        return reversed(out)

    def _is_every_letter_capital(self):
        """
            Check for capitalized words.
            :return: True if all letters are capital False otherwise
        """
        all_capitals = re.sub(r"[A-Z]", "", self.hashtag)
        return len(all_capitals) == 0  # bug len(all_capitals) > 0 will return True when one or more letters is not capital

    def _try_split_hashtag_by_capitals(self, hashtag):
        """
            To try get the aspect out of hashtags -- just to have some comparison for pos tag aspect
            assumption is that hashtag will be splittable by capitals //Pascal or camelCase like
            :return:None
        """
        return [a for a in re.split(r'([A-Z][a-z]*\d*)', str(hashtag)) if a]

    def _try_spellcheck(self, word):
        suggestions = self.spell_checker.correct_word(word)
        if len(suggestions) > 0:
            word = suggestions[0]
        return word

    def _is_spelling_correct(self, word):
        """
        Checks for spelling errors
        :param word: str
        :return: True if no errrors False otherwise
        """
        return self.spell_checker.is_correct(word)

    def _get_score_for_hasthag(self, hashtag):
        """
        Get SentiWordNet Score for word and return hashtag type (HT_POS, HT_NEG, HT)
        :param hashtag: str
        :return: enum HT_POS || HT_NEG || HT
        """
        if len(hashtag)> 2:
            word = hashtag
            result = None
            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale2_equals().format(word, ""))
            if score is not None and len(score) > 0:
                result = score[0][0]
            if result is not None and score[0][0] > 5.0:
                g.logger.debug("greater than 5.0")
                word = self.lancaster_stemmer.stem(hashtag)
                score = g.mysql_conn.execute_query(g.sum_query_figurative_scale2_like().format(word, ""))
                if score is not None and len(score) > 0:
                    result = score[0][0]
            if result is not None and result <= 5.0:
                g.logger.debug("hastag word result : %s" % result)
                if result > 0.0:
                    return g.TAGS.__HT_POS__
                elif result < 0.0:
                    return g.TAGS.__HT_NEG__
                return g.TAGS.__HT__
        return g.TAGS.__HT__

    def _calculate_result(self):
        """
            HTEmotionEvaluation = {
                                HT_POS if count(Positive) > count(Negative) > 0
                                HT_NEG if count(Negative) >= count(Positive) > 0
                                HT if count(Positive) == count(Negative) ==0
                                }
        :return:
        """
        g.logger.debug("handled_hashtag:%s" % self.handled_hashtag)
        if type(self.handled_hashtag) == list:
            # self.handled_hashtag.append(self.handled_hashtag[-1])  # weight the last score more
            pos = self.handled_hashtag.count(g.TAGS.__HT_POS__)
            neg = self.handled_hashtag.count(g.TAGS.__HT_NEG__)
            neu = self.handled_hashtag.count(g.TAGS.__HT__)
            # print "HANDLED HASHTAG", self.handled_hashtag, g.TAGS.__HT_POS__, g.TAGS.__HT_NEG__

            if pos > neg:
                return g.TAGS.__HT_POS__
            elif (neg >= pos) and (neg >= neu) and neg > 0:
                return g.TAGS.__HT_NEG__
            else:
                return g.TAGS.__HT__
        else:
            return self.handled_hashtag

    def reset(self):
        self.hashtag = None
        self.handled_hashtag = []

    def _is_all_lower(self):
       return len(re.findall(r"[A-Z]", self.hashtag)) == 0

    def calculate_from_ht_lexicon(self, hashtag):
        """
        Both parts, AffLex and NegLex, of each lexicon are contained in the same file. The NegLex entries have suffixes '_NEG' or '_NEGFIRST'.

        In the unigram lexicon:
        '_NEGFIRST' is attached to terms that directly follow a negator;
        '_NEG' is attached to all other terms in negated contexts (not directly following a negator).

        In the bigram lexicon:
        '_NEG' is attached to all terms in negated contexts.

        Both suffixes are attached only to nouns, verbs, adjectives, and adverbs. All other parts of speech do not get these suffixes attached.

        :return:
        """
        if len(hashtag)> 2:
            q = """SELECT round(Sum(score)/count(score), 2) FROM SentiFeed.NRCHashtagLexicon where term = "{0}"; """

            result = g.mysql_conn.execute_query(q.format(hashtag))
            if len(result) > 0 and result[0][0] is not None:
                if result[0][0] > 0:
                    return g.TAGS.__HT_POS__
                if result[0][0] < 0:
                    return g.TAGS.__HT_NEG__
                return g.TAGS.__HT__
        return g.TAGS.__HT__