from SentimentAnalysis.TweetUtils.helpers.globals import g
__author__ = 'm.karanasou'


class PriorPolarity(object):
    def __init__(self, feature_config, words, pos_tagged_words):
        self.config = feature_config
        self.words = words
        self.pos_tagged_words = pos_tagged_words
        self.swn_scores = None
        self.swn_score_total = 1.0  # neutral
        self.excluded = 0
        self.final = 0

    def get_simple_pos_for_swn(self, word):
        if self.pos_tagged_words[word] in g.NOUNS:
            return ' and Category = "n" '
        elif self.pos_tagged_words[word] in g.VERBS:
            return ' and Category = "v" '
        elif self.pos_tagged_words[word] in g.ADVERBS:
            return ' and Category = "r" '
        elif self.pos_tagged_words[word] in g.ADJECTIVES:
            return ' and Category = "a" '
        return ""

    def get_total(self):
        return self.swn_score_total

    def get_polarity(self):
        self.swn_scores = {}
        for word in self.words:
            self.swn_scores['s_word-'+self.words.index(word)] = self._get_score(word)
        if self.words.__len__() > self.excluded and self.words > 0:
            self.swn_score_total = round(self.final/float(self.words.__len__()- self.excluded), 2)
        else:
            if len(self.words) > 0:
                self.swn_score_total = round(self.final/float(self.words.__len__()), 2)
            else:
                self.swn_score_total = 1.0  # neutral
        return self.swn_scores

    def _get_score(self, word):
        """
        Get score according to ranking
        :param word:
        :return:
        """
        self.final = 0.0
        self.excluded = 0
        if len(word) > 1:
            try:
                # first try get equals score with category
                score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals()
                                                    .format(word, self.get_simple_pos_for_swn(word)))
            except:
                g.logger.error(g.sum_query_figurative_scale_equals().format(word, self.get_simple_pos_for_swn(word)))
                # if that failed, try get equals without category
                score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals().format(word, ""))
                g.logger.debug("tried equals for {0} and got {1}".format(word, score[0][0]))

            if score[0][0] is not None and score[0][0] <= 2.0:
                self.swn_scores['s_word-'+str(self.words.index(word))] = round(score[0][0], 2)
                self.final += score[0][0]
            else:
                self.swn_scores['s_word-'+str(self.words.index(word))] = 1
                self.excluded += 1
        else:
            self.excluded += 1

        return score[0][0] if score[0][0] is not None else 1.0