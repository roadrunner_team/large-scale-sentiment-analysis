__author__ = 'maria'

import re
import traceback
from hash_tag_handler import HashTagHandler
from string import punctuation

__author__ = 'maria'
from SentimentAnalysis.TweetUtils.helpers.globals import g


class TextTagger(object):
    """
    TextTagger is responsible for applying identified tags to a cleaned text.
    The result text should be:
    {REFERENCE} {LINK}
    and the result dictionary:
    _tags = {
    ...
    "CAPITAL" : "True",
    ...
    }

    """


    _initial_values = [False, "False", "", 0]

    def __init__(self, feature_configuration=None):
        self.initial_text = ""
        self.tagged_text = ""
        self.tag_pattern = " {%s} "
        self.ht = None
        self.ht_handler = HashTagHandler()
        self.feature_configuration = feature_configuration
        self._tags = None
        self._patterns_ = None
        self._init_configuration()


    def tag_text(self, text):
        """
        replaces text_to_tag tag part with appropriate tag label
        e.g. http://... ==> {LINK}
        """
        self.ht_handler.reset()
        self.initial_text = text
        self.tagged_text = text
        self.reset()
        for key in self._patterns_:
            try:

                if key == g.TAGS.__CAPITAL__:
                    match = re.findall(self._patterns_[key], self.initial_text)
                    if len(match) > 0 and "RT" not in match:
                        self.tagged_text += self.tag_pattern % self._tag(key)
                        self._tags[g.TAGS.name[key]]= "True"

                elif key == g.TAGS.__hashtag_lexicon_sum__:
                    if self.ht is None:
                        self.ht = re.findall(self._patterns_[key], self.initial_text)
                    if type(self.ht) == list:                                 # if more than one
                            ht_key = 0
                            for each in self.ht:                                     # for each in found hashtags
                                ht_key += self.ht_handler.handle(each, type=key)     # find if it is a positive
                                                                                     # or negative or neutral
                            ht_key = float(ht_key) / float(len(self.ht)) if len(self.ht) > 0 else 0.0
                    else:                                                            # not a list
                        ht_key = self.ht_handler.calculate_from_ht_lexicon(self.ht)  # find score from NRC HT Lexicon
                    self._tags[g.TAGS.name[key]] = ht_key

                elif key == g.TAGS.__punctuation_percentage__:
                    # urls should probably be removed before this
                    match = re.findall(self._patterns_[key], self.initial_text)
                    match = "".join([x for x in match if x !=' '])
                    self._tags[g.TAGS.name[key]] = round(float(len(match))/float(len(self.initial_text)) * 100, 0)

                elif key == g.TAGS.__HT__:
                    # find all hashtags
                    if self.ht is None:
                        self.ht = re.findall(self._patterns_[key], self.initial_text)
                    if self.ht is not None and len(self.ht):                         # if found
                        ht_key = ''
                        if type(self.ht) == list:                               # if more than one
                            tmp = []
                            for each in self.ht:                                # for each in found hashtags
                                tmp.append(self.ht_handler.handle(each))        # find if it is a positive or negative or neutral
                            tmp.append(tmp[-1])                                 # double weight at the last hashtag
                            pos = tmp.count(g.TAGS.__HT_POS__)
                            neg = tmp.count(g.TAGS.__HT_NEG__)
                            neu = tmp.count(g.TAGS.__HT__)
                            if pos > neg:
                                ht_key = g.TAGS.__HT_POS__
                            elif (neg >= pos) and (neg >= neu) and neg > 0:
                                ht_key = g.TAGS.__HT_NEG__
                            else:
                                ht_key = g.TAGS.__HT__
                        else:                                                # not a list
                            ht_key = self.ht_handler.handle(self.ht)         # find if it is a positive or negative or neutral
                        self.tagged_text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), self.tagged_text, flags=re.IGNORECASE)
                        self._tags[g.TAGS.name[ht_key]] = "True"             # replace the final verdict
                                                                             # of Hashtag handler with "True"
                elif key == g.TAGS.__HT_NEG__ or key == g.TAGS.__HT_POS__:
                    pass
                else:
                    if re.findall(self._patterns_[key], self.initial_text):
                        self._tags[g.TAGS.name[key]] = "True"
                        self.tagged_text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), self.tagged_text, flags=re.IGNORECASE)
                        if key == g.TAGS.__LINK__:
                            self.initial_text = re.sub(self._patterns_[key], "", self.initial_text, flags=re.IGNORECASE)
                    if re.findall(self._patterns_[key], self.initial_text.lower()):
                        self._tags[g.TAGS.name[key]] = "True"
                        self.tagged_text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), self.tagged_text, flags=re.IGNORECASE)
                        if key == g.TAGS.__LINK__:
                            self.initial_text = re.sub(self._patterns_[key], "", self.initial_text, flags=re.IGNORECASE)
            except:
                g.logger.error(traceback.format_exc())
                g.logger.error(str(key) + "\t" + g.TAGS.name[key])
                print key, g.TAGS.name[key]
                print traceback.print_exc()

        return self.tagged_text

    def get_tags(self):
        return self._tags

    def _tag(self, tag):
        return g.TAGS.name[tag]

    def reset(self):
        for key in self._tags.keys():
            self._tags[key] = "False"

    def _init_configuration(self):
        self._tags = self.feature_configuration.get_tags()
        self._patterns_ = self.feature_configuration.get_patterns()
        # for feature_name, value in self._tags.items():
        #     exists = [a for a in self.feature_configuration if a.name == feature_name]
        #     if len(exists) == 0:
        #         del self._tags[feature_name]
        #
        #     else:
        #         self._patterns[feature_name] = exists[0].regex
        #         self._tags[feature_name] = self._init_data_type(exists[0])

    def _init_data_type(self, feature):
        if feature.value in range(0, len(self._initial_values)):
            return self._initial_values[feature.value]

        else:
            raise Exception("No suitable type of feature: {0}".format(feature.value))

