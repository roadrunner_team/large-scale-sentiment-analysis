"""Web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'LargeScaleUI.views.home', name='home'),
    url(r'^login/$', 'LargeScaleUI.views.login', name='login'),
    url(r'^register/$', 'LargeScaleUI.views.register', name='register'),
    url(r'^about/$', 'LargeScaleUI.views.about', name='about'),
    url(r'^dashboard/$', 'LargeScaleUI.views.dashboard', name='dashboard'),
    url(r'^dashboard/task_info$', 'LargeScaleUI.views.task_info', name='task_info'),
    url(r'^dashboard/data_sets$', 'LargeScaleUI.views.data_sets', name='data_sets'),
    url(r'^dashboard/results$', 'LargeScaleUI.views.results', name='results'),
    url(r'^dashboard/trial$', 'LargeScaleUI.views.trial', name='trial'),
    url(r'^dashboard/start_trial$', 'LargeScaleUI.views.start_trial', name='start_trial'),
    url(r'^dashboard/get_results', 'LargeScaleUI.views.get_results', name='get_results'),
    url(r'^dashboard/tweet_utils', 'LargeScaleUI.views.tweet_utils', name='tweet_utils'),
    url(r'^dashboard/real_time', 'LargeScaleUI.views.real_time', name='real_time'),
    url(r'^dashboard/feedback', 'LargeScaleUI.views.feedback', name='feedback'),
    url(r'^dashboard/send_endpoint', 'LargeScaleUI.views.send_endpoint', name='send_endpoint'),
    url(r'^dashboard/long_polling_endpoint', 'LargeScaleUI.views.long_polling_endpoint', name='long_polling_endpoint'),
]
