/**
 * Created by mariakaranasou on 9/27/15.
 */

$(document).ready(function(){
    //$('.btn').button();
    setUpDonut();
    var chart = c3.generate({
    data: {
                x: 'x',
        //        xFormat: '%Y%m%d', // 'xFormat' can be used as custom format of 'x'
                columns: [
                    ['x', '2015-11-01', '2015-11-02', '2015-11-03', '2015-11-04', '2015-11-05', '2015-11-06'],
        //            ['x', '20130101', '20130102', '20130103', '20130104', '20130105', '20130106'],
                    ['neutral', 71130, 51200, 55100, 89400, 61150, 91250],
                    ['negative', 91130, 91340, 71200, 31500, 50250, 90350],
                    ['positive', 54400, 71500, 42450, 66700, 26100, 71500]
                ]
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%Y-%m-%d'
                    }
                }
            }
        });

        /*setTimeout(function () {
            chart.load({
                columns: [
                    ['positive', 54400, 71500, 42450, 66700, 26100, 71500]
                ]
            });
        }, 1000);*/
   listen();
});

var btn_correct = "<button class='btn btn-success' title='Prediction is correct' onclick='feedback(this)'><i class='fa fa-check'></i></button>";
var btn_error = "<button class='btn btn-danger' title='Prediction is not correct' onclick='feedback(this)'><i class='fa fa-close'></i></button>";

function listen(){
    $.ajax({url: "../dashboard/long_polling_endpoint/",
        success: function(result){
            try{
                 var jsonRes = JSON.parse(result);
                if(jsonRes.type==="tweet"){
                    var cls = "";
                    if(jsonRes.prediction == "positive"){
                        cls = 'btn-success';
                    }else if(jsonRes.prediction == "negative"){
                        cls = "btn-danger";
                    }else{
                        cls = "btn-info"
                    }
                    //$("<li class='"+ cls +"'>"+btn_correct + btn_error +"<span> "  +result+"</span></li>").hide().prependTo('#notifications').slideDown();
                    $("<li id='"+jsonRes.id+"' class='"+ cls +"'>"+btn_correct + btn_error +"<span> "  +jsonRes.text+"</span></li>").prependTo('#notifications');
                }
                else{
                    $("<li>"+btn_correct + btn_error +"<span> " +result+"</span></li>").prependTo('#notifications');
                }
            }
            catch(ex){
                $("<li class='btn-default'>"+btn_correct + btn_error +"<span> " +result+"</span></li>").prependTo('#notifications');
            }


            listen();
    },
    error: function(xhr,status,error){
       showalert(error, "danger");
        console.log("ERROR:", xhr.responseText)
    }
    });
}

function feedback(el){
    var $el = $(el);
    if($el.hasClass('btn-success')){
        $el.closest('li').slideUp();

    }else{

    }

}

function sendMessage(){
    var msg ={"message": $("#message").val()};
    var csrftoken = getCookie('csrftoken');

    $.ajax({url: "../dashboard/send_endpoint/",
        data: msg,
        method: "POST",
        "beforeSend": function(xhr, settings) {
            $.ajaxSettings.beforeSend(xhr, settings);
        },
        success: function(result){
            console.log(result.message);
    },
    error: function(xhr,status,error){
       showalert(error, "danger");
        console.log("ERROR:", xhr.responseText)
    }
    });
}

function setUpDonut() {

    var svg = d3.select("#donut")
        .append("svg")
        .append("g")

    svg.append("g")
        .attr("class", "slices");
    svg.append("g")
        .attr("class", "labels");
    svg.append("g")
        .attr("class", "lines");

    var width = 960,
        height = 450,
        radius = Math.min(width, height) / 2;

    var pie = d3.layout.pie()
        .sort(null)
        .value(function (d) {
            return d.value;
        });

    var arc = d3.svg.arc()
        .outerRadius(radius * 0.8)
        .innerRadius(radius * 0.4);

    var outerArc = d3.svg.arc()
        .innerRadius(radius * 0.9)
        .outerRadius(radius * 0.9);

    svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var key = function (d) {
        return d.data.label;
    };

    var color = d3.scale.ordinal()
        .domain(["Lorem ipsum", "dolor sit", "amet", "consectetur", "adipisicing", "elit", "sed", "do", "eiusmod", "tempor", "incididunt"])
        .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

    function randomData() {
        var labels = color.domain();
        return labels.map(function (label) {
            return {label: label, value: Math.random()}
        });
    }

    change(randomData());

    d3.select(".randomize")
        .on("click", function () {
            change(randomData());
        });


    function change(data) {

        /* ------- PIE SLICES -------*/
        var slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(data), key);

        slice.enter()
            .insert("path")
            .style("fill", function (d) {
                return color(d.data.label);
            })
            .attr("class", "slice");

        slice
            .transition().duration(1000)
            .attrTween("d", function (d) {
                this._current = this._current || d;
                var interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    return arc(interpolate(t));
                };
            })

        slice.exit()
            .remove();

        /* ------- TEXT LABELS -------*/

        var text = svg.select(".labels").selectAll("text")
            .data(pie(data), key);

        text.enter()
            .append("text")
            .attr("dy", ".35em")
            .text(function (d) {
                return d.data.label;
            });

        function midAngle(d) {
            return d.startAngle + (d.endAngle - d.startAngle) / 2;
        }

        text.transition().duration(1000)
            .attrTween("transform", function (d) {
                this._current = this._current || d;
                var interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    var d2 = interpolate(t);
                    var pos = outerArc.centroid(d2);
                    pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
                    return "translate(" + pos + ")";
                };
            })
            .styleTween("text-anchor", function (d) {
                this._current = this._current || d;
                var interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    var d2 = interpolate(t);
                    return midAngle(d2) < Math.PI ? "start" : "end";
                };
            });

        text.exit()
            .remove();

        /* ------- SLICE TO TEXT POLYLINES -------*/

        var polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(data), key);

        polyline.enter()
            .append("polyline");

        polyline.transition().duration(1000)
            .attrTween("points", function (d) {
                this._current = this._current || d;
                var interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    var d2 = interpolate(t);
                    var pos = outerArc.centroid(d2);
                    pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                    return [arc.centroid(d2), outerArc.centroid(d2), pos];
                };
            });

        polyline.exit()
            .remove();
    };
}
