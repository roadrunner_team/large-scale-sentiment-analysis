from django.shortcuts import render

# Create your views here.

import datetime
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
# from SentimentAnalysis.FigurativeTextAnalysis.helpers.globals import g
# from SentimentAnalysis.FigurativeTextAnalysis.models.Config import DatasetConfig, ClsConfig, ProcessConfig, TrialConfig
# from SentimentAnalysis.FigurativeTextAnalysis.models.DatasetHandler import DatasetHandler
# from SentimentAnalysis.FigurativeTextAnalysis.models.Trial_ import Trial
from models import TweetTestData, TweetFinalTestData, receive_message, send_message
from django.views.decorators.csrf import ensure_csrf_cookie
from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage

import redis
CHANNEL = 'live'
redis_publisher = RedisPublisher(broadcast=True)
message = RedisMessage('Hello World')
redis_publisher.publish_message(message)

# Create your views here.

selected_features =[
                    'postags',              # * // << +
                    # "__lemma_word__"
                    # 'words',              #
                    '__swn_score__',
                    's_word',               # * // << +
                    'pos_position_'
                    '__res__',                  # * <<
                    '__multiple_chars_in_a_row__',
                    '__punctuation_percentage__',
                    '__hashtag_lexicon_sum__',
                    '__is_metaphor__',
                    '__synset_length__',
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',               # * // <<   +
                    '__HT_NEG__',               # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',           # * // << +
                    '__NEG_SMILEY__',           # * // << +
                    '__NEGATION__',             # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',         # * // << +
                    '__exclamation__',          # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                ]

figurative_features = [ 'OH_SO',
                        'DONT_YOU',
                        'AS_GROUND_AS_VEHICLE',
                        ]
morphological_features=['CAPITAL',
                        'HT',
                        'HT_POS',
                        'HT_NEG',
                        'LINK',
                        'POS_SMILEY',
                        'NEG_SMILEY',
                        'NEGATION',
                        'REFERENCE',
                        'questionmark',
                        'exclamation',
                        'fullstop',
                        'RT',
                        'LAUGH',
                        'LOVE']
text_similarity_features = [
                        'res',
                        'lin',
                        'wup',
                        'path', ]
features = ['postags',
           # 'words',
           'swn_score',
           's_word',
           'contains_'
           ]


def home(request):
    return render_to_response('index.html')


def login(request):
    return render_to_response('login.html')


def register(request):
    return render_to_response('register.html')


def about(request):
    return render_to_response('about.html')


def dashboard(request):
    return render_to_response('dashboard.html')


def task_info(request):
    return render_to_response('task_info.html')


def long_polling_endpoint(request):
    message = receive_message()
    return HttpResponse(message.encode('utf8'), content_type='text/plain: charset-utf-8')


def send_endpoint(request):
    print request.POST
    message = request.POST.get('message')
    print message
    send_message(message)
    # return long_polling_endpoint(request)
    return HttpResponse(message.encode('utf8'), content_type='text/plain: charset-utf-8')


def tweet_utils(request):
    cleaning_options = {
        "remove_non_ascii":True,
        "remove_rt" : True,
        "remove_laugh": True,
        "split_sentences": True,
        "remove_negations": True,
        "remove_urls": True,
        "remove_emoticons": True,
        "remove_reference": True,
        "remove_special_characters": True,
        "fix_space": True,
        "split_words": True,
        "convert_to_lower": True,
        "remove_multiples": True,
        # self.spellcheck = True
        "remove_stop_words": True
    }
    options = {"cleaning_options": cleaning_options}

    return render_to_response('tweet_utils.html', options)


def data_sets(request):
    test_data_test = TweetTestData.objects.filter(train=0).count()
    test_data_trial = TweetTestData.objects.filter(train=1).count()
    final_data = TweetFinalTestData.objects.count()
    final = {"DataSets": [{"key": "test set", "val": test_data_test},
                          {"key": "trial set", "val": test_data_trial},
                          {"key": "final trial data", "val": final_data},
                          {"key": "final test data", "val": test_data_test+test_data_trial}]
             }

    return render_to_response('data_sets.html', final)


@ensure_csrf_cookie
def real_time(request):
    return render_to_response('real_time.html')


@ensure_csrf_cookie
def feedback(request):
    return render_to_response('feedback.html')


@ensure_csrf_cookie
def results(request):
    # todo: duplicate code: move this in a get_options method
    classifiers = ['NBayes', 'SVM', 'DecisionTree',
                   'SVMStandalone', 'SVR', 'NuSVR',
                   'RandomForestRegressor', 'SGD', 'SVC']
    discretization = ['1.0', '0.5', '0.2']
    context = RequestContext(request)
    options = {"classifiers": classifiers,
               "morphological": morphological_features,
               "figurative": figurative_features,
               "similarity": text_similarity_features,
               "features": features,
               "discretization": discretization,
               "corpuses" : ["Test", "Final"],
               "csrf_token": context}

    q = "SELECT * FROM SentiFeed.TrialScores inner join SentiFeed.Trial on SentiFeed.TrialScores.trial_id = Trial.id " \
        "inner join SelectedFeatures on Trial.selected_features_id = SelectedFeatures.id " \
        "where SelectedFeatures.AS_GROUND_AS_VEHICLE=1 limit 10000; "



    # data = g.mysql_conn.execute_query(q)

    return render_to_response('results.html', options)


@ensure_csrf_cookie
def trial(request):
    classifiers = ['NBayes', 'SVM', 'DecisionTree',
                   'SVMStandalone', 'SVR', 'NuSVR',
                   'RandomForestRegressor', 'SGD', 'SVC']
    discretization = ['1.0', '0.5', '0.2']
    context = RequestContext(request)
    options = {"classifiers": classifiers,
               "morphological": morphological_features,
               "figurative": figurative_features,
               "similarity": text_similarity_features,
               "features": features,
               "discretization": discretization,
               "corpuses" : ["Test", "Final"],
               "csrf_token": context}

    return render_to_response('trial.html', options)

@ensure_csrf_cookie
def start_trial(request):
    global selected_features

    send_message("START TRIAL..." + str(datetime.datetime.now()))
    print datetime.datetime.now().time()
    ds_config = DatasetConfig()
    ds_config.set_defaults()

    print ds_config
    send_message("DATASET CONFIGURATION..." + str(ds_config))
    ds = DatasetHandler(ds_config)
    ds.get_data_set()
    ds.get_feature_dicts_and_scores()
    ds.get_test_and_trial_set()
    send_message("GATHERED DATASET...")
    cls_config = ClsConfig(cls_type=g.CLASSIFIER_TYPE.SVMStandalone, vec_type=g.VECTORIZER.Dict,
                           max_vote=False,
                           select_percent=60,
                           use_idf=False,
                           use_hashing=False)
    process_config = ProcessConfig(cls_config, False, selected_features, ds)
    trial_config = TrialConfig(process_config, selected_features, "Tweet_Total")
    send_message("CONFIGURED TRIAL...")
    trial = Trial(trial_config, 0)
    send_message("START TRIAL...")
    trial.start()
    send_message("TRIAL FINISHED...")
    send_message("GETTING STATS...")
    trial.get_stats()
    send_message("SAVING RESULTS...")
    trial.save_results()
    # cls = request.POST.get('classifier')
    # discretization = request.POST.get('discretization')
    # corpus = request.POST.get('corpus')
    # selected_features = request.POST.getlist(u'selected_features[]')

    test_set = trial.tweet_processor.test_set
    test_initial_scores = trial.y_test
    predicted_scores = trial.predictions
    sklearn_precision_score = 0
    prediction_per_initial_agree = []
    prediction_per_initial_disagree = []

    for i in range(0, len(test_initial_scores)):

        temp = predicted_scores[i]

        # if type(predicted_scores[i]) is not float:
        #     print "before", predicted_scores[i]
        #     temp = round(float(trial.tweet_processor.discrete_labels[trial.tweet_processor.predictions[i]][0] +
        #                            trial.tweet_processor.discrete_labels[trial.tweet_processor.predictions[i]][1])/2.0)

        if test_initial_scores[i] > 0:
            if temp > 0:
                prediction_per_initial_agree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "success"])
            else:
                prediction_per_initial_disagree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "success"])
        elif test_initial_scores[i] == 0:
            if temp == 0:
                prediction_per_initial_agree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "info"])
            else:
                prediction_per_initial_disagree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "info"])
        elif test_initial_scores[i] < 0:

            if temp < 0:
                prediction_per_initial_agree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "danger"])
            else:
                prediction_per_initial_disagree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "danger"])

    context = RequestContext(request)

    result = {
                "CosineResult": trial.cosine_similarity,
                "csrf_token": context,
                "Results": {
                  "agree": prediction_per_initial_agree,
                  "disagree": prediction_per_initial_disagree
                },
                "sklearn_precision_score": sklearn_precision_score
              }

    print "end:", datetime.datetime.now().time()
    return render_to_response('start_trial.html', result)

def get_results(request):
    cls = request.POST.get('classifier')
    discretization = request.POST.get('discretization')
    corpus = request.POST.get('corpus')
    selected_features = request.POST.getlist(u'selected_features[]')
    cls_type = transform_cls_to_enum_values(cls)
    dscr_type = transform_discretization_to_enum_values(discretization)

    return HttpResponse('')


def foobar(request):
    # and somewhere else
    redis_publisher.publish_message(message)


def transform_cls_to_enum_values(cls):
    cls_types = [g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.NBayes],
                 g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.SVM],
                 g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.DecisionTree],
                 g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.SVMStandalone],
                 g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.SVR],
                 g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.NuSVR],
                 g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.RandomForestRegressor],
                 g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.SGD],
                 g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.SVC]]

    for each in cls_types:
        if each == cls:
            return cls_types.index(each)


def transform_discretization_to_enum_values(dscr):

    if dscr == "1.0":
        return g.DISCRETIZATION.ONE
    elif dscr == "0.5":
        return g.DISCRETIZATION.HALF
    elif dscr == "0.2":
        return g.DISCRETIZATION.ZERO_POINT_TWO
    else:  # default is no descretization
        return g.DISCRETIZATION.ONE