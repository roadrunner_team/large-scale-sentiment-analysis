from django.db import models
from tornado.ioloop import PeriodicCallback
import psutil
import redis
CHANNEL = 'live'


def send_message(message):
    client = redis.StrictRedis()
    message = message.encode('utf8')
    return client.publish(CHANNEL, message)


def receive_message():
    client = redis.StrictRedis()
    pubsub = client.pubsub()
    pubsub.subscribe(CHANNEL)
    message = ""
    for event in pubsub.listen():
        if event['type'] == 'message':
            message = event['data'].decode('utf8')
            break
    pubsub.unsubscribe()

    return message


class CommonResponse(object):
    def __init__(self):
        self.success = False
        self.message = ""
        self.data = None


class TweetTestData(models.Model):
    id = models.BigIntegerField(primary_key=True)
    text = models.CharField(max_length=150, blank=True)
    clean_text = models.CharField(max_length=250, blank=True)
    tagged_text = models.CharField(max_length=250, blank=True)
    pos_tagged_text = models.CharField(max_length=500, blank=True)
    tags = models.CharField(max_length=1000, blank=True)
    initial_score = models.FloatField(blank=True, null=True)
    sentences = models.CharField(max_length=250, blank=True)
    words = models.CharField(max_length=500, blank=True)
    links = models.CharField(max_length=250, blank=True)
    corrected_words = models.CharField(max_length=150, blank=True)
    smileys_per_sentence = models.CharField(max_length=150, blank=True)
    uppercase_words_per_sentence = models.CharField(max_length=150, blank=True)
    reference = models.CharField(max_length=150, blank=True)
    hash_list = models.CharField(max_length=150, blank=True)
    non_word_chars_removed = models.CharField(max_length=250, blank=True)
    negations = models.CharField(max_length=150, blank=True)
    swn_score_dict = models.CharField(max_length=1000, blank=True)
    swn_score = models.FloatField(blank=True, null=True)
    feature_dict = models.TextField(blank=True)
    score = models.FloatField(blank=True, null=True)
    train = models.TextField(blank=True)  # This field type is a guess.
    round_score0 = models.FloatField(blank=True, null=True)
    round_score1 = models.FloatField(blank=True, null=True)
    round_score2 = models.FloatField(blank=True, null=True)
    pnn = models.CharField(max_length=45, blank=True)
    modified = models.CharField(max_length=45, blank=True)
    polarity = models.CharField(max_length=45, blank=True)
    words_to_swn_score_dict = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'TweetTestData'


class TweetFinalTestData(models.Model):
    id = models.BigIntegerField(primary_key=True)
    text = models.CharField(max_length=150, blank=True)
    clean_text = models.CharField(max_length=250, blank=True)
    tagged_text = models.CharField(max_length=250, blank=True)
    pos_tagged_text = models.CharField(max_length=500, blank=True)
    tags = models.CharField(max_length=1000, blank=True)
    initial_score = models.FloatField(blank=True, null=True)
    sentences = models.CharField(max_length=250, blank=True)
    words = models.CharField(max_length=500, blank=True)
    links = models.CharField(max_length=250, blank=True)
    corrected_words = models.CharField(max_length=150, blank=True)
    smileys_per_sentence = models.CharField(max_length=150, blank=True)
    uppercase_words_per_sentence = models.CharField(max_length=150, blank=True)
    reference = models.CharField(max_length=150, blank=True)
    hash_list = models.CharField(max_length=150, blank=True)
    non_word_chars_removed = models.CharField(max_length=250, blank=True)
    negations = models.CharField(max_length=150, blank=True)
    swn_score_dict = models.CharField(max_length=1000, blank=True)
    swn_score = models.FloatField(blank=True, null=True)
    feature_dict = models.TextField(blank=True)
    score = models.FloatField(blank=True, null=True)
    train = models.TextField(blank=True)  # This field type is a guess.
    round_score0 = models.FloatField(blank=True, null=True)
    round_score1 = models.FloatField(blank=True, null=True)
    round_score2 = models.FloatField(blank=True, null=True)
    pnn = models.CharField(max_length=45, blank=True)
    modified = models.CharField(max_length=45, blank=True)
    polarity = models.CharField(max_length=45, blank=True)
    words_to_swn_score_dict = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'TweetFinalTestData'

pcb = None

# def broadcast_sys_info():
#     global pcb
#
#     if pcb is None:
#         pcb = PeriodicCallback(broadcast_sys_info, 500)
#         pcb.start()
#
#     cpu = psutil.cpu_percent()
#     net = psutil.net_io_counters()
#     bytes_sent = '{0:.2f} kb'.format(net.bytes_recv / 1024)
#     bytes_rcvd = '{0:.2f} kb'.format(net.bytes_sent / 1024)
#
#     publish_data('sysinfo', {
#         'cpu': cpu,
#         'kb_received': bytes_sent,
#         'kb_sent': bytes_rcvd,
#     })