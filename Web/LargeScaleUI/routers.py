# from swampdragon import route_handler
# from swampdragon.route_handler import ModelPubRouter
# from .models import Notification
# from .serializers import NotificationSerializer
# from swampdragon.route_handler import BaseRouter
# from models import broadcast_sys_info
#
# __author__ = 'mariakaranasou'
#
#
# class NotificationRouter(ModelPubRouter):
#     valid_verbs = ['subscribe']
#     route_name = 'notifications'
#     model = Notification
#     serializer_class = NotificationSerializer
#
#
# class SysInfoRouter(BaseRouter):
#     route_name = 'sys'
#
#     def get_subscription_channels(self, **kwargs):
#         broadcast_sys_info()
#         return ['sysinfo']
#
#
# route_handler.register(SysInfoRouter)
# route_handler.register(NotificationRouter)